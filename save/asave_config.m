function current = asave_config(new)
% asave_config returns (or sets) the matlab-global asave configuration
% 
% current = asave_config() returns the current configuration
% current = asave_config(new) sets a new configuration (and returns it, e.g. current==new)
% 
% See asave_config_* for details on configuration values.
% In general, asave_config_* should be used to set/get configuration values.

persistent config;

if isempty(config)
	% first time, set default values
	config.retries = 3;
	config.gracePause = 8;
end;

if exist('new','var')==1
	config = new;
end;

current = config;
