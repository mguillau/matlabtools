function data = load_struct(filename)
% load_struct loads a struct from a .mat file
%
% filename
%	filename or cell array of arguments for sprintf

if iscell(filename)
	filename = sprintf(filename{:});
end;

data = load(filename,'-mat');
