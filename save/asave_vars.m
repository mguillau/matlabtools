function asave_vars(filename,varargin)
% asave_vars saves named values to a .mat file as an atomic operation (either completely or not at all, no curruption)
% See asave_struct and save_vars for details.

asave_struct(filename,nameValueToStruct(varargin));
