function current = csave_config(new)

persistent config;

if isempty(config)
	config.minDelay = 10; % in minutes
end;

if exist('new','var')==1
	config = new;
end;

current = config;
