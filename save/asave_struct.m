function asave_struct(filename,data)
% asave_struct saves a struct as an atomic operation (either completely or not at all, no curruption)
% First, the data is written to a temporary file and then checked for errors.
% On success, it is moved to the target location.
% On failure, a number of delayed retries are performed (see config gracePause retries).
% In effect, the target file will allways be valid, either with the new data, or untouched with the old data.
% filename
%	filename or cell array of sprintf arguments

if iscell(filename), filename = sprintf(filename{:}); end;

config = asave_config();

% temporary file
% using prefix . to make it hidden
% using prefix and postfix to original filename to prevent simple filesearches to pick up on temporary files
tempfilename = sprintf('.temp_%s_%s',file_getName(filename),file_getName(tempname()));
tempfilename = file_convert(filename,[],tempfilename,[]);

fails = 0;
while true
	try
		save_struct(tempfilename,data);
		data = load(tempfilename);
		[success message] = movefile(tempfilename,filename,'f'); % TODO just how atomic is the matlab movefile?
		if success
			break; % done
		else
			error(message);
		end;
	catch e
	        fprintf('saving of %s => %s failed\n',tempfilename,filename);
		system(sprintf('rm %s 2>/dev/null',tempfilename)); % TODO doesn't handle fails
		fails = fails+1;
		if fails==config.retries
			rethrow(e); % give up, go up (the exception handling chain)
		end;
		pause(config.gracePause+rand()*config.gracePause);
	end;
end;

