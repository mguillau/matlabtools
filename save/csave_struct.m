function csave_struct(filename,data)

config = csave_config();

f = dir(filename);
if isempty(f) || (now()-f.datenum)*24*60>=config.minDelay
	asave_struct(filename,data);
end;
