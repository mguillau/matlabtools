function n = asave_config_retries(n)
% asave_config_retries sets/gets the numbers of retries for saving a file
% 
% n = asave_config_gracePause() returns the current setting
% n = asave_config_gracePause(n) sets a new value

config = asave_config();

if exist('n','var')==1
	config.retries = n;
	asave_config(config);
else
	n = config.retries;
end;
