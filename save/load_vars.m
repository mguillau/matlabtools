function varargout = load_vars(filename,varargin)
% load_vars loads named variables from a .mat file
% 
% [ value1 ... ] = load_vars(filename,name1,...)
%
% filename
%	filename or cell array of arguments for sprintf


assert(nargout()<=length(varargin));
if nargout()<length(varargin)
	warning('There are more name variables (%d) then return values (%d).',length(varargin),nargout()); %#ok<WNTAG>
end;

data = load_struct(filename);

varargout = cell(nargout(),1);
for i = 1:nargout()
	varargout{i} = data.(varargin{i});
end;
