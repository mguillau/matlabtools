function save_vars(filename,varargin)
% save_vars saves named values to a .mat file
% 
% See save_struct for details on saving.
% See nameValueToStruct(...) for details on varargin.
%
% filename
%	filename or cell array of sprintf arguments

save_struct(filename,nameValueToStruct(varargin));
