function save_value(filename,value)

if iscell(filename)
	filename = sprintf(filename{:});
end;

if sizeOfVar('value','gb')>=2;
    save(filename,'value','-v7.3');
else
    save(filename,'value','-v7');
end
