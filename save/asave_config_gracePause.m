function p = asave_config_gracePause(p)
% asave_config_gracePause sets/gets the approximate grace-pause in seconds after an unsuccessful save
% The effective pause is randomized uniformly in the intervall [p 2*p]
% 
% p = asave_config_gracePause() returns the current setting
% p = asave_config_gracePause(p) sets a new value

config = asave_config();

if exist('p','var')==1
	config.gracePause = p;
	asave_config(config);
else
	p = config.gracePause;
end;
