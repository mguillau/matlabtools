function value = load_value(filename)

if iscell(filename)
	filename = sprintf(filename{:});
end;

load(filename,'value');
