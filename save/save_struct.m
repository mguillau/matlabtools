function save_struct(filename,data) %#ok<INUSD>
% save_struct saves the fields of a struct to a .mat file
% folder will be created, if not existent
%
% filename
%	filename or cell array of sprintf arguments

if par_state_isWorker() && ~has_caller('asave_struct',0) % use safe wrapper but avoid infinite loop
	asave_struct(filename,data);
	return;
end;

if iscell(filename)
	filename = sprintf(filename{:});
end;

file_mkdir(file_getFolder(filename));

if sizeOfVar('data','gb')>=2;
    save(filename,'-struct','data','-v7.3');
else
    save(filename,'-struct','data','-v7');
end

