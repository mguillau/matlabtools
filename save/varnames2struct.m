function str = varnames2struct(varargin)
% VARNAMES2STRUCT Transform a list of variables names into a struct
%
% str = varnames2struct(varname1,varname2,...,varnameN) returns a struct str such that str.(varnameI) contains the value of varnameI is the caller workspace.
% str = varnames2struct({varname1,varname2,...,varnameN}) does the same.

if length(varargin)==1 && iscell(varargin{1}),
    varargin=varargin{1};
end

assert( all(cellfun(@ischar,varargin)) );

str = struct();
for i=1:length(varargin),
    str.(varargin{i}) = evalin('caller',varargin{i});
end
