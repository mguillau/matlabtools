function normalize_ip(varargin)
% NORMALIZE_IP Normalize in place.
%
% NORMALIZE_IP(A,dim)
%
% Modifies the matrix A in place, normalizing (sum=1) along dimension d
% if d is omitted or d<=0, then the entire matrix is normalized to sum to 1.
% if d is larger than the number of dimensions of A, then d is set to the
% last dimension of A.

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile_nooutput;
