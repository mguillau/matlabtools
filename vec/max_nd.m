function [x,varargout] = max_nd(X)
% MAX_ND Multidimensional maximum
%
% x = MAX_ND(X) finds maximum value of X  (this is like x=MAX(X(:)) )
% [x,i] = MAX_ND(X) also returns the index of x in X: x=X(i)
% [x,i,j,k,...] = MAX_ND(X) returns the subscript values of x in X:
% x = X(i,j,k,l,...)
%
% See also: MAX, MIN_ND, SORT_ND

[x,v]=max(X(:));
n_argout = max(1,nargout);
varargout = cell(1,n_argout);
if n_argout==2,
    varargout{1} = v;
else
    [varargout{:}]=ind2sub(size(X),v);
end
