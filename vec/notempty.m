function Y = notempty(X)
Y = ~isempty(X);
end