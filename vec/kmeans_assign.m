function idx = kmeans_assign(centroids,data)

if true,
    D = 2*data*centroids';
    D = bsxfun(@minus,sum(data.^2,2),D);
    D = bsxfun(@plus,sum(centroids.^2,2)',D);
    [~,idx] = min(D,[],2);
else
    [K d] = size(centroids);
    n = size(data,1);
    assert(size(data,2)==d);
    
    idx = zeros(n,1);
    parfor i = 1:n
        [~,idx(i)] = min(sum(bsxfun(@minus,data(i,:),centroids).^2,2));
    end; clear i;
end

