/*
 * =============================================================
 * sparseSwitch.c
 * Considering a sparse matrix with one element per column, this
 * allows to efficiently modify the row index of the elements
 * mex -largeArrayDims sparseSwitch.c
 * =============================================================
 */


#include <math.h> /* Needed for the ceil() prototype. */
#include "mex.h"

/* If you are using a compiler that equates NaN to be zero, you 
 * must compile this example using the flag  -DNAN_EQUALS_ZERO.
 * For example:
 *
 *     mex -DNAN_EQUALS_ZERO fulltosparse.c
 *
 * This will correctly define the IsNonZero macro for your C
 * compiler.
 */

#if defined(NAN_EQUALS_ZERO)
#define IsNonZero(d) ((d) != 0.0 || mxIsNaN(d))
#else
#define IsNonZero(d) ((d) != 0.0)
#endif

void mexFunction(
        int nlhs,       mxArray *plhs[],
        int nrhs, const mxArray *prhs[]
        )
{
    
    
  /* Declare variables. */
  int j,k,m,n,p;
  double *sr, *newlabels, *idx;
  mwIndex *irs, *jcs;

  /* Check for proper number of input and output arguments. */    
  if (nrhs < 2 || nrhs > 3) {
    mexErrMsgTxt("Two or three input argument required.");
  } 

  /* Check data type of input argument. */
  if (!(mxIsSparse(prhs[0])) || mxIsComplex(prhs[0])) {
    mexErrMsgTxt("The first argument must be a real sparse matrix.");
  } 

  if (!mxIsDouble(prhs[1]) || mxIsSparse(prhs[1])) {
    mexErrMsgTxt("The second argument must be a full double vector.");
  }

  if (nrhs==3 &&(!mxIsDouble(prhs[2]) || mxIsSparse(prhs[2]))) {
    mexErrMsgTxt("The third argument must be a full double vector.");
  }
  
  newlabels = mxGetPr(prhs[1]); /* pointer to new labels */

  m  = mxGetN(prhs[0]);
  n  = mxGetNumberOfElements(prhs[1]);
  if (nrhs==3) {
     p = mxGetNumberOfElements(prhs[2]);
     idx = mxGetPr(prhs[2]);
     if (n!=p || m<n) {
        mexErrMsgTxt("Invalid arguments");
     }
  } else if (m != n) {
      mexPrintf("Both arguments should have the same number of elements (%d vs. %d)",m,n);
      mexErrMsgTxt("Error");
  }
  
  /* Make sure the data is not shared by matlab */
  mxUnshareArray((const mxArray *) prhs[0], true);

  sr  = mxGetPr(prhs[0]); /* Pointer to ones (the data) */
  irs = mxGetIr(prhs[0]); /* Pointer to rows, same length as sr, values are 0-(rows-1) indicate rows*/
  jcs = mxGetJc(prhs[0]); /* Pointer to first non zeros element (in sr and irs) for each column */
  
  for (j=0;j<n;j++) {
      if (nrhs==3) {
         k = (int) idx[j]-1;
         if (k<0 || k>=m) {
             mexPrintf("Invalid row (%d vs. %d)",k+1,m);
             mexErrMsgTxt("Error");
         }
      } else {
         k = j;
      }
      irs[k] = (int) newlabels[j]-1;
  }
  
}
