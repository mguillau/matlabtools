function result = bitadd(a,b)
% BITADD Perform addition with C-style overflow
%
% c=BITADD(a,b) adds a and b with C-style overflow
%
% Compare the following:
%  a = intmax('uint32')  % =4294967295
%  b = uint32(10)
%  a+b          returns  4294967295
%  bitadd(a,b)  returns  9
%

carry = bitand(a,b);
result = bitxor(a,b);

while carry~=0,
    shiftedcarry = bitshift(carry,1);
    carry = bitand(result,shiftedcarry);
    result = bitxor(result,shiftedcarry);
end

