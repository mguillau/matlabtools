function [M, z] = normalize2(A, dim)
% NORMALIZE2 Make the entries of a (multidimensional) array sum to 1
% [M, c] = normalize2(A)
% c is the normalizing constant
%
% [M, c] = normalize2(A, dim)
% If dim is specified, we normalize the specified dimension only,
% otherwise we normalize the whole array.

if ~exist('dim','var'),
  z = sum(A(:));
  % Set any zeros to one before dividing
  % This is valid, since c=0 => all i. A(i)=0 => the answer should be 0/1=0
  s = z + (z==0);
  M = A / s;
else
   z = sum(A,dim);
   s = z + (z==0);
   M = bsxfun(@rdivide,A,s);
end
