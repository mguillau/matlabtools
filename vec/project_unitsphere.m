function x = project_unitsphere(y)
% PROJECT_UNITSPHERE Project vector to L2 unit sphere
%
% Project a vector on the unit sphere (of same dimension)

x = y(:);
x = x./norm(x);
