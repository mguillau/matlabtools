function [x fv e]  = project_distrib(y)
% PROJECT_DISTRIB Project vector to positive L1 unit surface
%
% x = project_distrib(y) projects a vector y onto the set of multinomial distributions
% (positive values that sum to 1, i.e, the intersection of the positive cone and L1 unit sphere).
% This implementation solves a quadratic program.
%
% [x fv e ] = project_distrib(y) also returns the value fv of the objective of the quadprog, and e is the exit code of the quadprog
%
% See also QUADPROG

y   = y(:);
N   = length(y);
H   = 2*eye(N);
f   = -2*y;

Aeq   = ones(1,N); % equality constraint
beq   = 1;         % equality constraint

A = [];
b = [];

lb  = zeros(N,1);
ub  = ones(N,1); % []

options = optimset('LargeScale','off','Display','off');

[x fv e] = quadprog(H,f,A,b,Aeq,beq,lb,ub,[],options);

fv = fv + y'*y;

x = max(0,x);
x = x/sum(x);
