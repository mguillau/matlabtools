function [Z,I] = merge_sortedrows(X,Y,k)
% MERGE_SORTEDROWS Merge two matrices of sorted rows into a single matrix of sorted rows, keeping only the smallest values
%
% Z = merge_sortedrows(X,Y,k) merges each row of X and Y (sorted), to keep only the k smallest elements, and keep them sorted.
% [Z,I] = merge_sortedrows(X,Y,k) also returns the indices of X (positive) and Y (negative) that were kept.
%
% Example:
%
% If X =
%     4     6     7    10    17
%     1     4     7    13    14
% and Y =
%     5     9     9    11    11
%     1     4     8     8    15
% Then merge_sortedrows(X,Y,7) returns Z =
%     4     5     6     7     9     9    10
%     1     1     4     4     7     8     8
% and I =
%     1    -1     3     5    -3    -5     7
%    -2     2    -4     4     6    -6    -8
%

kx = size(X,2);
ky = size(Y,2);
default k kx+ky
assert(size(X,1)==size(Y,1));
n = size(X,1);
k = min(k,kx+ky); % can't keep more than kx+ky elements!

Z = zeros(n,k);
I = zeros(n,k);
Ix = (1:n)';
Iy = Ix;

X(:,end+1)=Inf; % trivialize the problem of exhausting a vector
Y(:,end+1)=Inf;

for i=1:k, % we treat each all the rows in one go, so we iterate over columns
    xc = X(Ix);
    yc = Y(Iy);
    ix = xc<yc; % -> take values from x
    iy = ~ix;   % -> take values from y

%     Z(:,i) = min(xc,yc);
    Z(ix,i) = xc(ix);
    Z(iy,i) = yc(iy);
    
    I(ix,i) = Ix(ix);  % keep track of indices from X (+)
    I(iy,i) = -Iy(iy); % and Y (-)
    
    Ix(ix) = Ix(ix)+n; % shift indices to the next column (X)
    Iy(iy) = Iy(iy)+n; % (Y)
end

% I = sign(I) .* (bsxfun(@minus,abs(I),(1:n)')/n+1);
