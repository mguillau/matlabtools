function [x,i]=bottom(X,k)
% BOTTOM return the k smallest elements of a vector
% Y=BOTTOM(X,k) returns the k smallest elements of X
% [Y,I]=BOTTOM(X,k) also returns the indices such that X(I) = BOTTOM(X,k)
% If 0<k<1, then is considered as a fraction as round(k*length(X)) is used.

X=X(:);
if round(k)~=k,
    k = round(k*length(X));
end

if nargout>1,
    [x,i]=sort(X,'ascend'); % n.log(n)
    x=x(1:k);
    i=i(1:k);
else
    x=nth_element(X,k); % n
    x=sort(x(1:k),'ascend'); % k.log(k)
end
