function Y = ranks(X)
[~,o] = sort(X);
Y = invperm(o);

