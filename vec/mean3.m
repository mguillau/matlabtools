function y = mean3(x,dim)
%MEAN3   Average or mean value.
%   For vectors, MEAN3(X) is the mean value of the elements in X. For
%   matrices, MEAN3(X) is a row vector containing the mean value of
%   each column.  For N-D arrays, MEAN3(X) is the mean value of the
%   elements along the first non-singleton dimension of X.
%
%   Contrary to the Matlab version of mean, this version ignores NaN values
%
%   MEAN3(X,DIM) takes the mean along the dimension DIM of X. 
%
%   Example: If X = [1 2 3; 3 3 6; 4 6 8; 4 7 7];
%
%   then mean3(X,1) is [3.0000 4.5000 6.0000] and 
%   mean3(X,2) is [2.0000 4.0000 6.0000 6.0000].'
%
%   Class support for input X:
%      float: double, single
%
%   See also MEAN, MEDIAN, STD, MIN, MAX, VAR, COV, MODE.

default dim find(size(x)~=1,1,'first');
% Determine which dimension SUM will use

if isempty(dim), dim = 1; end

nans = sum(isnan(x),dim);
x(isnan(x))=0;
nans(nans==size(x,dim))=NaN;

y = bsxfun(@rdivide,sum(x,dim),size(x,dim)-nans);

end
