#include "matrix.h"
#include "mex.h"
#include <math.h>

/* TODO: multi-thread version!! */

/* extern "C" bool mxUnshareArray(mxArray *array_ptr, bool noDeepCopy); */

#define DEFINE_NORMALIZE_FUN(type) \
void inplaceNormalize_##type(type * data,size_t K,size_t offset,size_t pad) { \
    size_t i = offset;         \
    size_t k = 0;              \
    type acc = 0;              \
    for (k=0;k<K;k++) {        \
        acc += data[i];        \
        i   += pad;            \
    }                          \
    if (acc>0) {               \
        i = offset;            \
        for (k=0;k<K;k++) {    \
            data[i] /= acc;    \
            i       += pad;    \
        }                      \
    }                          \
}

#define APPLYNORM(type)                     \
DEFINE_NORMALIZE_FUN(type);                 \
type* data = (type *) mxGetPr(prhs[0]);     \
if (d==-1)                                  \
    inplaceNormalize_##type(data,(size_t) N,(size_t) 0,(size_t) 1); \
else {                                      \
    size_t jp = na*dims[d];                 \
    for (i=0;i<nb;i++) {                    \
        for (j=0;j<na;j++) {                \
            inplaceNormalize_##type(data,dims[d],j*offset,na); \
        }                                   \
        data = data+jp;                     \
    }                                       \
}



void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    if (nrhs < 1 || nrhs > 2)
        mexErrMsgTxt("1 or 2 inputs required.");
    if ( !mxIsDouble(prhs[0]) && !mxIsSingle(prhs[0]) )
        mexErrMsgTxt("First input must be SINGLE or DOUBLE");

    /* make sure the data is not shared by MATLAB */
    mxUnshareArray((const mxArray *) prhs[0], true);

    /* read the input dimensions */
    mwSize D = mxGetNumberOfDimensions(prhs[0]);
    const mwSize *dims = mxGetDimensions((const mxArray *) prhs[0]);
    
    /* make sure d is valid */
    mwSize d = -1;
    if (nrhs==2) {
        d = (mwSize) mxGetScalar(prhs[1])-1;
        d = d>D?D:d;
        d = d<-1?-1:d;
    }
    
    mwSize N = mxGetNumberOfElements(prhs[0]);
    mwSize i = 0;
    mwSize j = 0;
    mwSize na = 1;
    mwSize nb = 1;
    mwSize offset = 1;
    for (j=0;j<d;j++)
        na *= dims[j];
    for (j=d+1;j<D;j++)
        nb *= dims[j];
    offset = d==0?dims[0]:1;

    if (mxIsDouble(prhs[0])) {
        APPLYNORM(double);
    } else {
        APPLYNORM(float);
    }
}
