function varargout = isconstant(varargin)
% ISCONSTANT checks if a vector contains all equal values.
%
% b = ISCONSTANT(X) returns true iff X is empty or all elements of X are equal up to machine (double) precision.

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile;
