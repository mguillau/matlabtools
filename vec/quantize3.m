function B=quantize3(A, boundaries)
% QUANTIZE3 Compute a 3D joint histogram with specified boundaries
%
% B=quantize3(A, boundaries)
% A is a I x J x 3  array
% boundaries is a 3 x 1 cell array

if size(A,3)~=3 || length(boundaries)~=3
    error('data and/or boundaries do not fit, need 3D data');
end
D=3;

% first make sure we have well implemented boundaries
for d=1:D
    boundaries{d}(end)=inf;
end

imsize = size(A(:,:,1));

B=zeros([imsize 3],'uint16');
for d=1:D,
    bb=boundaries{d};
    Bt=zeros(imsize,'uint16');
    At=A(:,:,d);
    for t=length(bb):-1:1,
        ii = At<bb(t);
        Bt(ii) = t;
    end
    B(:,:,d)=Bt;
end
