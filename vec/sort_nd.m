function [x,varargout] = sort_nd(X)
% SORT_ND Multidimensional sorting
%
% Y = SORT_ND(X) sorts all the elements of X in ascending order. This is like Y=sort(X(:)).
%
% [Y,I] = MAX_ND(X) also returns the indices of Y in X: Y=X(I).
%
% [Y,I1,I2,I3,...,Id] = SORT_ND(X) sorts all the elements of X in ascending order, but also returns the subscripts of each element:
% for all k, Y(k) = X(I1(k),I2(k),...,Id(k)), where d is the number of dimensions of X.
%

[x,v]=sort(X(:));
n_argout = max(1,nargout);
varargout = cell(1,n_argout);
if n_argout==2,
    varargout{1} = v;
else
    [varargout{:}]=ind2sub(size(X),v);
end
