function B = array_shift(A,value,shiftsize)
% ARRAY_SHIFT Shift array and fill with value.
%
% B = ARRAY_SHIFT(A,value,SHIFTSIZE) shifts the values in the array A
%   by SHIFTSIZE elements. SHIFTSIZE is a vector of integer scalars where
%   the N-th element specifies the shift amount for the N-th dimension of
%   array A. If an element in SHIFTSIZE is positive, the values of A are
%   shifted down (or to the right). If it is negative, the values of A
%   are shifted up (or to the left).
% 
%   Examples:
%      A = [ 1 2 3;4 5 6; 7 8 9];
%      B = array_shift(A,0,1) % shifts first dimension values down by 1 and set the first row to 0.
%      B =     0     0     0
%              1     2     3
%              4     5     6
%      B = array_shift(A,9,[1 -1]) % shifts first dimension values down by 1
%                               % and second dimension left by 1.
%                               % the first row and the last column are
%                               % filled with 9
%      B =     9     9     9
%              2     3     9
%              5     6     9
%   See also CIRCSHIFT, SHIFTDIM.


% Error out if there are not exactly two input arguments
if nargin < 2
    error(message('MATLAB:circshift:NoInputs'))
end

% Parse the inputs to reveal the variables necessary for the calculations
try
    [p, sizeA, numDimsA] = ParseInputs(A,shiftsize);
catch ME
    rethrow(ME)
end

% Calculate the indices that will convert the input matrix to the desired output
% Initialize the cell array of indices
idx  = cell(1, numDimsA); % circshift indices
idx2 = cell(1, numDimsA); % filling indices

% Loop through each dimension of the input matrix to calculate shifted indices
for k = 1:numDimsA
    m       = sizeA(k);
    u       = (0:m-1)-p(k);
    idx{k}  = mod(u, m)+1;
    idx2{k} = (u<0)|(u>=m);
end

% Perform the actual conversion by indexing into the input matrix
B = A(idx{:});
for k = 1:numDimsA
    p = [ k 1:k-1 k+1:numDimsA ];
    B = permute(B,p);
    B(idx2{k},:) = value;
    B = ipermute(B,p);
end


%%%
%%% Parse inputs
%%%
function [p, sizeA, numDimsA] = ParseInputs(a,p)

% default values
sizeA    = size(a);
numDimsA = ndims(a);

% Make sure that SHIFTSIZE input is a finite, real integer vector
sh        = p(:);
isFinite  = all(isfinite(sh));
nonSparse = all(~issparse(sh));
isInteger = all(isa(sh,'double') & (imag(sh)==0) & (sh==round(sh)));
isVector  = isvector(p);

% Error out if ParseInputs discovers an improper SHIFTSIZE input
if ~(isFinite && isInteger && isVector && nonSparse)
    error(message('MATLAB:circshift:InvalidShiftType'));
end

% Make sure the shift vector has the same length as numDimsA.
% The missing shift values are assumed to be 0. The extra
% shift values are ignored when the shift vector is longer
% than numDimsA.
if (numel(p) < numDimsA)
    p(numDimsA) = 0;
end

