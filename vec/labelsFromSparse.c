/*
 * =============================================================
 * labelsFromSparse.c
 * Get a label vector from a sparse assignment matrix.
 * There is at most one nonzero value per column, given by the input vector.
 * mex -largeArrayDims labelsFromSparse.c
 * =============================================================
 */


#include "mex.h"

void mexFunction(
        int nlhs,       mxArray *plhs[],
        int nrhs, const mxArray *prhs[]
        )
{
    
    
  /* Declare variables. */
  int j,k,m,p;
  double *sr, *labels, *idx;
  mwIndex *irs, *jcs;
  mwSize n,c;

  /* Check for proper number of input and output arguments. */    
  if (nrhs != 1) {
    mexErrMsgTxt("One input argument required.");
  } 

  /* Check data type of input argument. */
  if (!mxIsDouble(prhs[0]) || !mxIsSparse(prhs[0])) {
    mexErrMsgTxt("The first argument must be a sparse vector.");
  }
  
  n  = mxGetN(prhs[0]); /* number of data points = number of columns */
  c  = mxGetM(prhs[0]); /* largest label = number of rows */
  irs = mxGetIr(prhs[0]); /* directly access the assignments */
  
  plhs[0] = mxCreateDoubleMatrix(1,n,mxREAL);
  labels = mxGetPr(plhs[0]);

  for (j=0;j<n;j++) {
      labels[j] = (double) irs[j]+1;
  }
  
}
