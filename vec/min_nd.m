function [x,varargout] = min_nd(X)
% MIN_ND Multidimensional minimum
%
% x = MIN_ND(X) find minimum value of X  (this is like x=MIN(X(:)) )
% [x,i] = MIN_ND(X) also returns the index of x in X: x=X(i)
% [x,i,j,k,...] = MIN_ND(X) returns the subscript values of x in X:
% x = X(i,j,k,l,...)
%
% See also: MIN, MAX_ND, SORT_ND

[x,v]=min(X(:));
n_argout = max(1,nargout);
varargout = cell(1,n_argout);
if n_argout==2,
    varargout{1} = v;
else
    [varargout{:}]=ind2sub(size(X),v);
end
