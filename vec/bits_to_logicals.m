function out = bits_to_logicals(in,dimension,outsize)
% Transform a binary representation (one bit per boolean) into logical array.
% (Not particularly efficient. Used to validate logicals_to_bits.m)
%
% arguments:
%   in = 2d array of uintXX (uint8, uint16, uint32, uint64)
%   dimension = 1, 2 (default)
%     1: uintXX stretches along dimension 1 (encodes a part of a column)
%     2: uintXX stretches along dimension 2 (encodes a part of a row)
%   outsize = real length along expandend dimension
%     if the real length is not size(uintXX)*elements
%     default is size(uintXX)*elements
%
% output:
%   out = 2d array of logicals

arg_assert in numeric
arg_assert dimension numeric default 2
arg_assert outsize numeric default []

c = class(in);
switch c
	case 'uint8', bits = 8;
	case 'uint16', bits = 16;
	case 'uint32', bits = 32;
	case 'uint64', bits = 64;
	otherwise, assert(false,c);
end;

if dimension==1
	transposed = true;
	in = in';
elseif dimension==2
	transposed = false;
else
	assert(false);
end;

[m_in n_in] = size(in);

if isempty(outsize)
	outsize = n_in*bits;
else
	assert(0<=n_in*bits-outsize && n_in*bits-outsize<bits);
end;

out = false(m_in,n_in*bits);

% version bits by all (much faster)
for b = 1:bits
% 	fprintf('%d/%d\n',b,bits);
	out(:,b:bits:end) = bitget(in,repmat(b,m_in,n_in));
end;

% % version: row by column by bits
% for m = 1:m_in
% 	for n = 1:n_in
% 		for b = 1:bits
% 			out(m,(n-1)*bits+1:n*bits) = bitget(in(m,n),1:bits)==1;
% 		end;
% 	end;
% end;

out = out(:,1:outsize);

if transposed
	out = out';
end;
