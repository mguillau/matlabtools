function x = project_positive(x)
% PROJECT_POSITIVE Project vector to positive cone

x(x<0)=0;
