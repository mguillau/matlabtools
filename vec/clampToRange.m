function x = clampToRange(a,x,b)

x = min(max(a,x),b);
