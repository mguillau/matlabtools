function varargout = labelsFromSparse(varargin)
% LABELSFROMSPARSE Create the label vector from a sparse assignment matrix
%

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile;
