function  p = softmax(e,d)
% SOFTMAX Compute the softmax activation function
%
% P = SOFTMAX(X,d) returns the softmax activations for X along dimension d (default d=1)
%
% Softmax is defined by: p = exp(e)/sum(exp(e))
%
% Original code by J.J. Verbeek, 2006, modified by Matthieu Guillaumin, 2013.

default d 1;
e = bsxfun(@minus,e,max(e,[],d)); % subtract maximum to have one energy at zero and rest below, so normalization does not give problems
I = e>-15;

p = zeros(size(e));
p(I) = exp(e(I));
p = normalize2( p+eps, d);
