function d=intdepfun(x)

if x==0,
    d={};
else
    d={ round((x-1)/2), x-1 };
end