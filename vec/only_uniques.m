function Y = only_uniques(X)
% ONLY_UNIQUES Keep only elements that occur exactly once in X
%
% Y = ONLY_UNIQUES(X) keeps only elements that occur exactly once in X.
% In other words, it removes elements occurring more than once in X.
%

[Y,ia1]=unique(X,'first');
[~,ia2]=unique(X,'last');

Y=Y(ia1==ia2);
