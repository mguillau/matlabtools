function updateSparse(varargin)
% UPDATESPARSE Update a sparse assignment matrix

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile_nooutput;
