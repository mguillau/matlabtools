function v = ind2vec(index, n)
% IND2VEC Convert a list of indices to a logical matrix
%
% v = ind2vec(index, n)
%
% if n is omitted then v will be sized by the max entry of index
%

default n max(index);
v = sparseFromLabels(index,n);
