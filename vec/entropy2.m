function h=entropy2(P)
% ENTROPY2 Compute the entropy of discrete distributions with natural logarithm
%
% h = entropy2( P )
%
% P : each row is a discrete distribution, need not be normalized. 
%

P = normalize2(P,2);
h = -sum(P.*log(P+eps),2);

 
