function [i a] = minmax(X,varargin)
% MINMAX Compute min and max
%
% [m,M]=minmax(X,...) is the same as m=min(X,...);M=max(X,...)
%

i = min(X,varargin{:});
a = max(X,varargin{:});

