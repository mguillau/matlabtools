function  p = smoothmax(e,d)
% SMOOTHMAX Smooth approximation of maximum.
%
% Y = SMOOTHMAX(X,d) computes the smooth approximation of maximum of X along dimension d.
%
% The smooth max is defined by: y = sum(x.exp(x))/sum(exp(x))
% This is the expected value of x under the softmax probabilities.
%
% See also SOFTMAX

default d 1;

f = bsxfun(@minus,e,max(e,[],d)); % subtract maximum to have one energy at zero and rest below, so normalization does not give problems
I = f>-15;

p = zeros(size(f))+eps;
p(I) = exp(f(I));

S = sum(p,d);
T = sum(p.*e,d);
p = T./S;
