#include <sys/types.h>
#include <unistd.h>
#include "mex.h"
#include "matrix.h"

int isallequal(double *data, double value, int len) {
    while (len)
    {
        if (data[--len] != value)
            return 0;
    }
    return 1;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    if(nrhs!=1)
        mexErrMsgTxt("One input required.");
    else if(nlhs > 1)
        mexErrMsgTxt("Too many output arguments.");
    
    if (mxGetNumberOfElements(prhs[0])<=1) {
        plhs[0] = mxCreateLogicalScalar(1);
    } else if(!mxIsDouble(prhs[0])) {
        mexErrMsgTxt("Non-empty input must be a double array.");
    } else {
        double * data = mxGetPr(prhs[0]);
        plhs[0] = mxCreateLogicalScalar(isallequal(data+1, data[0], mxGetNumberOfElements(prhs[0])-1));
    }
}
