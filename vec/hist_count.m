function [ Y, Z ] = hist_count( X, d, Z )
% HIST_COUNT Create a histogram of along dimension
%
% Y = HIST_COUNT(X) counts the occurrences of values in matrix X.
% Y = HIST_COUNT(X,d) works along dimension d of X and the d-th dimension of Y corresponds to the histogram.
% Y = HIST_COUNT(X,d,Z) uses bins Z (defaults to "min(X(:)):max(X(:))")
%
% [Y,Z] = HIST_COUNT(X,...) also returns the elements Z such that Y(i) == sum( X==Z(i) ). Useful when Z is not speficied in input.
%
% NB: HIST_COUNT(X) is similar to HIST_COUNT(X(:),1)

if nargin<2,
    X=X(:);
    d=1;
end
if nargin<3,
    m = full(min(X(:)));
    M = full(max(X(:)));
    Z = m:M;
end
p = [ d setdiff(1:ndims(X),d) ];
X = permute( X, p );
D = size(X);
D(1) = length(Z);
Y = zeros(D);
for k=1:length(Z),
    if Z(k)==0 && issparse(X),
        Y(k,:) = size(X,1)*prod(D(2:end))-sum(X~=0,1);
    else
        Y(k,:) = reshape(sum(X==Z(k),1),[],1);
    end
end
Y = ipermute( Y, p );
