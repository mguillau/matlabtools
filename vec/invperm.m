function q = invperm(p)
% INVPERM computes the inverse permutation
%
% Q=INVPERM(P) computes the inverse permutation of P: Q(P)=P(Q)=1:length(P)=1:length(Q)

n=length(p);
q=zeros(size(p));
q(p)=1:n;
