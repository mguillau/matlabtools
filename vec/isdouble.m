function varargout = isdouble(varargin)
% ISDOUBLE Check if a matrix has double-precision
%
% b = ISDOUBLE(X) returns true iff X has double precision.

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile;
