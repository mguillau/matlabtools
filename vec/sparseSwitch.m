function varargout = sparseSwitch(varargin)
% SPARSESWITCH Modify/update a sparse assignment matrix in place
%
% SPARSESWITCH(Y,X) updates a KxN sparse assignment matrix Y in place using a new label vector X.
% This is done extremely fast by manipulating the sparse matrix in c. Actually, the sparse format in matlab requires simplying copying X.
%
% SPARSESWITCH(Y,X,S) updates only the subset defined by S:  Z(c,S(i)) = X(S(i)) for elements in S
%
% See also SPARSEFROMLABELS

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile_nooutput;
