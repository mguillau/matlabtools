function lim = stretchlim2(values,tol)
% LOW_HIGH = STRETCHLIM(VALUES,TOL) returns the range such that 1-2*tol of values are preserved.
% Can be used by ADJUST_SIGMOID to adjust values to [0-1], or CLAMPTORANGE. The idea is to remove outliers at the extremeties.
%
% TOL = [LOW_FRACT HIGH_FRACT] specifies the fraction of the values to
% ignore at low and high ends.
%
% If TOL is a scalar, TOL = LOW_FRACT, and HIGH_FRACT = 1 - LOW_FRACT,
% which ignores equal fractions at low and high values.
%
% If you omit the argument, TOL defaults to 0.01, ignoring 2% of the
% values.
%
% If TOL = 0, LOW_HIGH = [min(VALUES(:)); max(VALUES(:))].

default tol [0.01 0.99];

if numel(tol)==1,
    tol = [ tol 1-tol ];
end

kmin = max(1,round(tol(1)*numel(values))+1);
v1 = nth_element(values,kmin);
lim(1) = v1(kmin);

kmax = min(length(values),round(tol(2)*numel(values)));
v2 = nth_element(values,kmax);
lim(2) = v2(kmax);
