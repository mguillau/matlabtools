function M = powerset(n)
% POWERSET Create a logical matrix whose columns represent all the subsets
% of n elements
%
% powerset(n) returns a n-by-(2.^n) logical matrix 
%

assert(n<=32);

M = bits_to_logicals(uint32((1:2^n)-1),1,n);
