function x = unfind(y,n)
% UNFIND Create a boolean vector from indices
%
% X = UNFIND(Y,n)  creates a boolean vector X of length n such that find(X)=Y
% by default, n=max(Y(:))

default n max(y(:))
x = false(1,n);
x(y) = true;
