function N = l2norm(X,dim)
% L2NORM Compute L2 norm along a dimension
%
% N = L2NORM(X,d) compute L2 norms of elements in X along dimension d
% If not specified, d defaults to 1.
%

default dim 1;

Y = sqrt( sum(X.^2,dim) );
