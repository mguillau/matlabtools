function [PCA,Y] = pca3(X,varargin)
% pca2 - fast PCA analysis as a struct 
%
%input
%  X - (n x d) data
%  k - desired number of components, min of dimensions and data nr by default
%returns
%  PCA.mean - (1 x d) data mean
%  PCA.proj - (d x k) eigenmatrix
%  PCA.diag - (1 x d) eigenvalues
%  Y - (n x k) projected data 
%

if nargout==2,
    [PCA.mean,PCA.proj,PCA.diag,Y] = pca2(X,varargin{:});
else
    [PCA.mean,PCA.proj,PCA.diag] = pca2(X,varargin{:});
end


