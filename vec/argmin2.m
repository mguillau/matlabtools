function idx = argmin2(x,d)
% ARGMIN2  Index of minimum element.
%
% ARGMIN2(X) returns the index I such that X(I) == MIN(X(:)) of the smallest element in x.
% ARGMIN2(X,d) operate on dimension d. It returns [~,I]=MIN(X,[],d).

if ~exist('d','var'),
    x=x(:);
    [~, idx]=min(x);
else
    [~, idx]=min(x,[],d);
end
