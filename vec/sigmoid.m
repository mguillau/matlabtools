function S = sigmoid(X);
% SIGMOID Compute the sigmoid of the elements of X
%
% y=sigmoid(x) when x is scalar returns y=1/(1+exp(-x))
% Y=sigmoid(Y) does the same for each element of X
%

S = 1./(1+exp(-X));
