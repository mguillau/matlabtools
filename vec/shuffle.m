function [Y I] = shuffle(X)
% SHUFFLE Randomly permute vector elements
% 
% Y=SHUFFLE(X) returns a vector with the same elements as X but in random order.
% [Y,I]=SHUFFLE(X) also returns the indices such that Y=X(I)

I=randperm(length(X));
Y=X(I);
