function m = simplexmesh(d,n)
% SIMPLEXMESH Uniformely sample a mesh on the d-simplex
%
% M = SIMPLEXMESH(d,n) uniformly samples points on the d-simplex (d+1 values that sum to 1).
% n is the number of points per edge on the d-simplex
%
% The returned mesh M contains one point per row. The number of points grows
% quadratically with n and exponentially with d (curse of dimensionality).
% 
% Ex: simplexmesh(2,3) returns 6 points on the 2-simplex (ie vectors of length 3 that sum to 1)
%  [ 1.0000         0         0
%    0.5000    0.5000         0
%         0    1.0000         0
%    0.5000         0    0.5000
%         0    0.5000    0.5000
%         0         0    1.0000 ]
%
% 


if d==0,
    m = 1;
elseif d==1,
    linspa = linspace(0,1,n);
    m = [1-linspa(:) linspa(:)];
else
    % Recursive version: very simple code but exponential time complexity.
    % Was used before the serial version was written and fully tested.
%     if false,
%         md = linspace(0,1,n); %lin(n); %  linspace of the additional dim
%         m = zeros(0,d+1);
%         for i=1:length(md),
%             mn = simplexmesh(d-1,n-i+1);
%             m = [ m; mn.*(1-md(i)) md(i)*ones(size(mn,1),1) ];
%         end
%     else
    % Serialized version (dynamic programming): avoid exponential time complexity and keep memory footprint low
        meshes = cell(1,n);
        linspa = cell(1,n);
        for i=1:n,
            linspa{i} = linspace(0,1,i); % linspaces of [0-1] for 1..n
            meshes{i} = [1-linspa{i}(:) linspa{i}(:)]; % meshes for d=1 for 1..n
        end
        for j=2:d,
            ms  = cellfun(@(x) size(x,1),meshes);
            cms = cumsum(ms);
            if j==d, % for the last step, don't need to compute meshes for 1..n-1
                range = n;
            else
                range = n:-1:1;
            end
            for k=range,
                md = linspa{k};
                currentMesh = zeros(cms(k),j+1);
                for i=1:k,
                    mn = meshes{i}; % = simplexmesh(j-1,k-i+1)
                    mi = md(k-i+1);
                    u = cms(k)-cms(i)+1;
                    currentMesh(u:u+ms(i)-1,:) = [ (1-mi).*mn mi*ones(size(mn,1),1) ];
                end
                meshes{k} = currentMesh; % = simplexmesh(j,k)
            end
        end
        m = meshes{n};
%     end
end

