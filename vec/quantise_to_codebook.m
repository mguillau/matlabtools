function Q = quantise_to_codecook(vectors,codebook)
% QUANTISE_TO_CODEBOOK Assign vectors to codebook entries based on nearest neighbour for the Euclidean distance
%
% Q = QUANTISE_TO_CODEBOOK(X,DICT) returns the assigment label vector Q (length N) for vectors in X (DxN matrix) using codebook DICT (DxK matrix).
% Values in Q range from 1 to K, and is compressed to the smallest uint class (uint8 if K<255, uint16 if K<65535, etc...)

norms = -.5*sum(codebook.^2,1);
DIP   = bsxfun(@plus,vectors*codebook,norms);
[~,Q] = max(DIP,[],2);

nbins = size(codebook,2);
if nbins < intmax('uint8'),
    Q=uint8(Q);
elseif nbins < intmax('uint16');
    Q=uint16(Q);
elseif nbins < intmax('uint32');
    Q=uint32(Q);
else
    Q=uint64(Q);
end
