function idxsets = splitRange(range,minTasksPerJob,maxJobs,loads)
% split the indices in range to get balanced jobs according to load

default loads ones(1,numel(range));

range=range(:);
loads=loads(:);

totalidx    = numel(range);
totalload   = sum(loads); % total load
balanced    = ceil(totalload/maxJobs); % number of loads per job
real_ntasks = max(minTasksPerJob,balanced); % we want a minimum load for each job
real_njobs  = ceil(totalload/real_ntasks); % that's the final number of jobs

idxsets = cell(real_njobs,1);

b=1;
cs = [ 0; cumsum(loads); Inf ];
for i=1:real_njobs,
    e = min(max(find(cs>=real_ntasks+cs(b),1,'first')-1,b),totalidx);
    idxsets{i} = range(b:e);
    b = e+1;
end

bal = cellfun( @(X) sum(loads(X)), idxsets);
ii = bal == 0;
idxsets(ii)=[];
bal(ii) = [];

fprintf('Load balancing report:');
fprintf(' %d',bal);
fprintf('\n');
