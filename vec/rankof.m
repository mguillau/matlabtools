function j = rankof(X,i,subset)
%j = rankof(X,i)
% returns the rank of X(i) in X
%j = rankof(X,i,subset)
% returns the rank of X(i) within in subset of indices of X. returns -1 if
% i is not in the subset.
%
% this function is ok for one call, but if you plan to make many calls, you should replace
% >> j=rankof(X,i)
% with:
% >> R=ranks(X); j=R(i);

if nargin==3,
    [b,loc] = ismember(i,subset);
    if ~b
        j = -1;
    else
        j = rankof(X,loc);
    end
    return
end

if length(i)==1,
    j = nnz(X<X(i))+1;
else
    j = sum(bsxfun(@lt,vec(X),rowvec(X(i))))+1;
end

%[~,o] = sort(X);
%[~,j] = ismember(i,o);

