function e = entropy_mult(p)

assert(all(isfinite(p)));
assert(any(p>0));

p = p./sum(p); % could use assert, but basically the same performance impact
p = p(p>0); % 0 probability does not need to be encoded
e = -sum(p.*log2(p));
