function [D,I] = knn(X,k,Y)
% [D,I] = knn(X,k,Y)  compute k-NN of X within Y
% [D,I] = knn(X,k)    compute k-NN within X (excluding oneself)
%
% X is n-by-d, Y is m-by-d, k is scalar
% D is the n-by-k squared L2 distance matrix (not sorted!)
% I is the n-by-k index matrix (values between 1 and m)
%
% memory: O(nk+m)
% time: O(ndm+nm)

sym=~exist('Y','var');
if sym,
    Y=X;
end

n=size(X,1);
D=zeros(n,k);
if nargout==2,
    I=zeros(n,k);
end

for i=1:n,
    di = distance_L2_full_mex(X(i,:),Y); % ok since parallelized over Y
    if sym,
        di(i) = Inf;
    end
    if nargout==2,
        [D(i,:) I(i,:)] = bottom(di,k);
    else
        D(i,:) = bottom(di,k);
    end
end

