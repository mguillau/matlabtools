function idx = argmax2(x,d)
% ARGMAX2  Index of maximum element.
%
% ARGMAX2(X) returns the index I such that X(I) == MAX(X(:)) of the largest element in x.
% ARGMAX2(X,d) operate on dimension d. It returns [~,I]=MAX(X,[],d).

if ~exist('d','var'),
    x=x(:);
    [~, idx]=max(x);
else
    [~, idx]=max(x,[],d);
end
