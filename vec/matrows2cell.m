function c = matrows2cell(m)

c = mat2cell(m,ones(size(m,1),1),size(m,2));
