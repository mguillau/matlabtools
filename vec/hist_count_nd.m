function [ Y, Z ] = hist_count_nd( X )
%HIST_COUNT_ND  Compute a n-d histogram of rows of X
%
% For a matrix X with n rows and d columns, Y = HIST_COUNT_ND(X) creates a histogram of d-uplets in X.
% 
% That is, Y(i,j,k,...) contains the number of rows in X that are equal to [i j k ...]
%
% [Y,Z] = hist_count_nd(X) also returns the ranges for each dimension.

d = size(X,2);
r = max(X,[],1)-min(X,[],1)+1;
Y = zeros(r);

Z = zeros(2,d);
Z(1,:) = min(X,[],1);
Z(2,:) = max(X,[],1);

U = bsxfun(@minus,X,Z(1,:))+1;
s = size(Y);
for i=1:size(U,1),
    x=num2cell(U(i,:));
    j=sub2ind(s,x{:});
    Y(j) = Y(j)+1;
end
