function varargout = sparseFromLabels(varargin)
% SPARSEFROMLABELS Create a sparse assignment matrix from a label vector
%
% Z = SPARSEFROMLABELS(X,K) creates a KxN sparse assignment matrix from a label vector of length N with values smaller than K.
% Each row of Z is a cluster, each column is a data point. Z(:,i) is the 1-of-K coding of the assignment. That is, Z(c,i)=(X(i)==c).
% There is at most one nonzero value per column, given by the input vector.
% This is done extremely fast by creating the sparse matrix in c. Actually, the sparse format in matlab requires simplying copying X.
%
% Z = SPARSEFROMLABELS(X,K,T) uses T(i) instead of 1 in Z:  Z(c,i) = T(i) if X(i)==c
%
% NB: you can revert this function with find, because with [I,J]=find(Z), then I==X and J==(1:N)'
%     or simply using X = labelsFromSparse(Z);

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile;
