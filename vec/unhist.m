function y = unhist(x)
% UNHIST Create a sorted vector following a histogram of value counts
%
% Y=UNHIST(X) creates a sorted vector Y such that hist_count(Y)=X
% Y starts with ones.
%

n = sum(x);
k = length(x);
y = zeros(1,n);

u = 0;
for i=1:k,
    v = u+x(i);
    y(u+1:v) = i;
    u = v;
end
