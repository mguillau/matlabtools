function [M,V,D,Y] = pca2(X,k)
% pca2 - fast PCA analysis 
%
%input
%  X - (n x d) data
%  k - desired number of components, min of dimensions and data nr by default
%returns
%  M - (1 x d) data mean
%  V - (d x k) eigenmatrix
%  D - (1 x d) eigenvalues
%  Y - (n x k) projected data 

default k min(n-1,d);

[n,d] = size(X);

if nargin==1
  k = min(n-1,d);
end

M = mean(X);
X = X - repmat(M,n,1);

if n > d

  options.disp=0;
  [V,D]=eigs(X'*X,[],k,'lm',options);
  [D,I]=sort(-diag(D));
  D = -D/n;
  V = V(:,I);

else 	 % use inner products matrix
 
  options.disp=0;
  K = X*X';
  [U,eigvalsK,W] = svds(K,k,'L',options); 
  L = sqrt(diag(eigvalsK))';
  V = X' * (W ./ repmat(L,size(W,1),1));

  D = L.^2 / n;

end

if nargout>3,
  Y = bsxfun(@minux,X,M)*V;
end
