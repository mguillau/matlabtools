function m2t_print(f,h,config)
% M2T_PRINT Save a figure as a tikz picture for inclusion in LaTeX. Wraps over MATLAB2TIKZ.
%
% m2t_print(filename) saves the current figure in file filename.
% m2t_print(fid) saves the current figure in the opened file given by the fid as output by fopen.
% m2t_print(f,h) uses the figure handle h instead of the current one.  mat2tikz(f) is equivalent to mat2tikz(f,gcf)
% m2t_print(f,h,config) 
%
% See also MATLAB2TIKZ, M2T_CONFIG

default h get(0,'CurrentFigure');
default config struct();
config = configFromDefaults(m2t_config(),config);
if isnumeric(f),
    config.filehandle = f;
else
    config.filename = f;
end

options = struct2arglist(config);

matlab2tikz(options{:});

