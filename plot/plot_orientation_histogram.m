function plot_orientation_histogram(hist)

Nbins = numel(hist);
step = 2*pi/Nbins;
theta = -pi:step:pi;
rho   = [hist(end) hist];

h=polar(theta,rho);
set(0,'Showhiddenhandles','on') % to show hidden handles
extrastuff = setdiff(get(gca,'children'),h);
delete(findobj(extrastuff,'-property','Interpreter'));
%delete(extrastuff) % to remove all, or you can use findobj 
