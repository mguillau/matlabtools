function h = scatterN(X,varargin)
% SCATTERN Scatter plot of a N-by-D matrix
% 
% Simple wrapper over scatter (D=2) and scatter3 (D=3).
%

assert(ndims(X)==2);

if size(X,2)==2,
   h=scatter(X(:,1),X(:,2),varargin{:});
elseif size(X,2)==3.
   h=scatter3(X(:,1),X(:,2),X(:,3),varargin{:});
else
   error('Only 2 or 3 columns supported');
end

