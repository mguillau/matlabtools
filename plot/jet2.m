function J = jet2(m)
%JET2    Variant of JET
%   JET2(M) is an M-by-3 matrix containing a colormap.
%   M defaults to 64. Contrary to MATLAB's JET, if no figure exists,
%   this code will not create one.
%
%   See also JET, HSV, HOT, PINK, FLAG, COLORMAP, RGBPLOT.

default m 64;

n = ceil(m/4);
u = [(1:1:n)/n ones(1,n-1) (n:-1:1)/n]';
g = ceil(n/2) - (mod(m,4)==1) + (1:length(u))';
r = g + n;
b = g - n;
g(g>m) = [];
r(r>m) = [];
b(b<1) = [];
J = zeros(m,3);
J(r,1) = u(1:length(r));
J(g,2) = u(1:length(g));
J(b,3) = u(end-length(b)+1:end);
