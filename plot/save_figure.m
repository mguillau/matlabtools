function save_figure_png(file,h)
ext='png';
f = [tempname() '.' ext];
print( h, ['-d' ext], f ); % save figure to disk
I = imread(f); % reload to crop
I = imautocrop(I);
imwrite(I,file); % save the cropped version
