function print_table(titl,rowtitles,coltitles,values,mode,fid)
% PRINT_TABLE Pretty-print a table in LaTeX or text
%
% print_table(titl,rowtitles,coltitles,values,mode,fid)
%  Pretty-prints a 2-D cell array of strings for convenient reading or for
%  latex.
%
%  titl = string for the upper-left most cell
%  rowtitles = cell of R strings for the R row headers
%  coltitles = cell of C strings for the C column headers
%  values = RxC cell array of strings to print
%  mode = { 'latex', other } (default=other)
%  fid = file descriptor to print in (default=stdout)
%
%  titl, rowtitles and coltitles can provide some formatting information
%  for drawing horizontal and/or vertical lines in the table.
%  this works as follows:
%  - if titl or coltitles have '<', '>' or '^' as first character, the
%  corresponding column in left-aligned, right-aligned or centered (resp.),
%  ant the character is stripped.
%  - *then*, if titl or coltitles have '|' as first (resp. last) character,
%  a vertical line is drawn on its left (resp. right). Those characters are
%  stripped from the corresponding strings
%  - *then*, if titl or rowtitles have '_' as first (resp. last) character,
%  an horizontal line is drawn before (resp. after). Those characters are
%  stripped from the corresponding strings
%  note: the order only matters for titl. ex: 'My Title|_' will only have a
%  horizontal line after the first row, while 'My Title_|' will have a
%  vertical line after the first column *and* a horizontal line after the
%  first row.
%
%  examples:
%  -  print_table('<|title_|',{ 'row1', 'row2', '_row3' },{ '^|col1', '>|col2'}, { '1', '2'; '300000', '4'; '5', '60000000000000000' })
%  outputs:
% | title      ||    col1    |              col2 
% +------------++------------+-------------------
% | row1       ||      1     |                 2 
% | row2       ||   300000   |                 4 
% +------------++------------+-------------------
% | row3       ||      5     | 60000000000000000 
%
%  - print_table('<title_|',{ '_row1', '_row2', '_row3' },{ 'col1', 'col2'}, { '1', '2'; '300000', '4'; '5', '60000000000000000' },'latex')
% outputs:
% \begin{tabular}{l|rr}
%  title      &       col1 &              col2  \\
% \hline
% \hline
%  row1       &          1 &                 2  \\
% \hline
%  row2       &     300000 &                 4  \\
% \hline
%  row3       &          5 & 60000000000000000  \\
% \end{tabular}

colsepfmt = '|';
rowsepfmt = '_';
colalign = '><^';

default fid 1; % stdout by default
default mode 'txt';

if strcmp(mode,'txt'),
    format.colsep = {' ','|'};
    format.rowsep = {'-','+'};
    format.newl   = '\n';
    format.rowsepnewl = '\n';
    format.begintable = '\n';
    format.endtable   = '\n';
    format.mincolwidth = 10;
else
    if strcmp(mode,'latexc'),
        format.latextabsep = {'','\\@\\{\\}|\\@\\{\\}'};
        format.latexcolalign = 'rlc';
        format.colsep = {' ','&'};
        format.rowsep = {'',''};
        format.begintable = '\\begin{tabular}{';
        format.newl   = ' \\\\\n';
        format.rowsepnewl = '\\hline\n';
        format.endtable   = '\\end{tabular}\n';
        format.mincolwidth = 10;        
    else
        format.latextabsep = {'','|'};
        format.latexcolalign = 'rlc';
        format.colsep = {' ','&'};
        format.rowsep = {'',''};
        format.begintable = '\\begin{tabular}{';
        format.newl   = ' \\\\\n';
        format.rowsepnewl = '\\hline\n';
        format.endtable   = '\\end{tabular}\n';
        format.mincolwidth = 10;        
    end
end
format.mode=mode;
format.fid=fid;

C=length(coltitles);
R=length(rowtitles);

% process titles to get formatting of columns
format.L = cell(1,length(coltitles)); % column width
sep.left = false(1,1+length(coltitles));
sep.right = false(1,1+length(coltitles));
format.align = ones(1,1+length(coltitles));
[format.align(1),titl]=parsetitle2(titl,colalign);
[sep.left(1),sep.right(1),titl]=parsetitle(titl,colsepfmt);
for c=1:C,
    [format.align(c+1),coltitles{c}]=parsetitle2(coltitles{c},colalign);
    [sep.left(c+1),sep.right(c+1),coltitles{c}]=parsetitle(coltitles{c},colsepfmt);
    format.L{c} = max([ format.mincolwidth; cellfun(@length,[ coltitles(c); values(:,c) ])]); % column width
end

% and of rows
sep.top = false(1,1+length(rowtitles));
sep.bottom = false(1,1+length(rowtitles));
[sep.top(1),sep.bottom(1),titl]=parsetitle(titl,rowsepfmt);
for r=1:R,
    [sep.top(r+1),sep.bottom(r+1),rowtitles{r}]=parsetitle(rowtitles{r},rowsepfmt);
end
format.sep = sep;
format.K1 = max([ format.mincolwidth cellfun(@length,[ {titl} rowtitles ])]); % width of row header

% print table
fprintf(fid,format.begintable);
if strcmp(mode,'latex'),
    print_latexformat(format);
    fprintf(fid,'}\n');
end
coltitles = arrayfun(@(c1) nsprintf('%s',format.L{c1},coltitles{c1},format.align(c1+1)),1:C,'UniformOutput',false);
print_tableline(1,titl,coltitles,format);
for r=1:R,
    print_tableline(r+1,rowtitles{r},values(r,:),format);   
end
fprintf(fid,format.endtable);
    
end


function [left,right,str]=parsetitle(str,chr)
    left = ( ~isempty(str) && str(1)==chr );
    right = ( length(str)>1 && str(end)==chr );
    if left, str(1)=[]; end
    if right, str(end)=[]; end
end

function [align,str]=parsetitle2(str,chrset)
    align = ( ~isempty(str) && any(str(1)==chrset) );
    if align, align=find(chrset==str(1)); str(1)=[]; else align=1; end
end

function print_latexformat(format)
print_tablecell(1,1,format.latexcolalign(format.align(1)),'latextabsep',format,false);
for c1=1:length(format.L),
    print_tablecell(c1+1,1,format.latexcolalign(format.align(c1+1)),'latextabsep',format,false);
end
end

function print_tableline(linenum,titl,values,format)
if format.sep.top(linenum),
    print_rowsep(format);
end
print_tablecell(1,format.K1,titl,'colsep',format,false);
for c1=1:length(values),
    print_tablecell(c1+1,format.L{c1},values{c1},'colsep',format,false);
end
fprintf(format.fid,format.newl);
if format.sep.bottom(linenum),
    print_rowsep(format);
end
end

function print_rowsep(format)
print_tablecell(1,format.K1,format.rowsep{1},'rowsep',format,true);
for c1=1:length(format.L),
    print_tablecell(c1+1,format.L{c1},format.rowsep{1},'rowsep',format,true);
end
fprintf(format.fid,format.rowsepnewl);
end

function print_tablecell(c,n,s,sep,format,repeat)
ltx = strcmp(format.mode,'latex');
forcecolsep  = ltx && strcmp(sep,'colsep') && c>1; % if true, put separation only for second column on.
celllinemode = ~ltx || strcmp(sep,'latextabsep');  % if true, print separation for each cell individually.
if forcecolsep || (format.sep.left(c) && celllinemode),
    fprintf(format.fid,format.(sep){2});
end
if repeat,
    if isempty(s),
        tmp = '';
    else
        tmp = repmat(s,1,ceil(n/length(s)));
        tmp = tmp(1:n);
    end
    fprintf(format.fid,[ format.(sep){1} tmp format.(sep){1} ]);
else
    fprintf(format.fid,[ format.(sep){1} nsprintf('%s',n,s,format.align(c)) format.(sep){1} ]);
end
if format.sep.right(c) && celllinemode,
    fprintf(format.fid,format.(sep){2});
end
end
