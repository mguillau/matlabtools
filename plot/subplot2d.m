function h=subplot2d(m,n,u,v)
% Specify subplot with gridsize (m,n) and location (u,v).
% Location u and v can be length(.)=2

if ~exist('n','var')
	
	% assuming input m = [ m n u v ; ... ], nested boxes
	h=nested(m);
	
else

	if isscalar(u) && isscalar(v)
		h=subplot(m,n,(u-1)*n+v);
	elseif length(u)==2 && length(v)==2
		h=subplot(m,n,[ (u(1)-1)*n+v(1) (u(2)-1)*n+v(2) ]);
	else
		assert(false);
	end;

end;


function h=nested(boxes)

bounds = [ 0 0 1 1 ];
n = size(boxes,1);

for i = 1:n %#ok<FORPF>
	bounds = getBounds(bounds,boxes(i,:));
end;

h=subplot('Position',bounds);


function bounds = getBounds(border,box)

w = border(3)/box(2);
h = border(4)/box(1);

bounds = [ border(1)+w*(box(4)-1) border(2)+border(4)-h*box(3) w h ];
