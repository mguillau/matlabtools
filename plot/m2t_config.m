function current = m2t_config(new)

persistent c;
persistent d;

if isempty(d), % initialize with default settings: check the help of matlab2tikz
%	d.colormap = colormap;
	d.strict = false;
	d.showInfo = false;
	d.showWarning = true;
	d.imagesAsPng = true;
	d.relativePngPath = [];
	d.height = '\\tikzpictureheight';
	d.width = '\\tikzpicturewidth';
	d.extraCode = [];
	d.extraAxisOptions = [];
	d.extraTikzpictureOptions = [];
	d.encoding = '';
	d.floatFormat = '%.15g';
	d.maxChunkLength = 4000;
	d.parseStrings = true;
	d.parseStringsAsMath = false;
	d.showHiddenStrings = false;
	d.interpretTickLabelsAsTex = false;
	d.tikzFileComment = '';
	d.automaticLabels = false;
	d.standalone = false;
	d.checkForUpdates = false;
end;

if exist('new','var'), % Replace existing config
    c = new;
end;

if ~isempty(getenv('MATLABTOOLS')) && isempty(c), % Matlabtools is used but the config got lost (eg, clear functions)
    % Set c to the default values
    feval(mfilename,d);
    % Reload the config
    warning('Reloading matlabtools config');
    run(file_makepath(getenv('MATLABTOOLS'),'setup.m'));
    current = feval(mfilename);
    return
end

if exist('new','var'), % Replace existing config
    c = new;
end;

current = c;
