function S = mvfield(S,oldname,newname)
% Rename a field, keeping all other fields intact.

f = fieldnames(S);
ii = cellfun(@(x) ~isempty(x),regexp(f,oldname));
assert(nnz(ii)==1)

oldfield = f{find(ii)};
data = {S.(oldfield)};
S = rmfield(S,oldfield);
n = numel(S);
for i=1:n,
  S(i).(newname) = data{i};
end

