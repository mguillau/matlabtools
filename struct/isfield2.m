function bool = isfield2(X,varargin)
% Checks the existence of nested fields
% Ex: isfield2(X, 'a', 'b', 'c') checks the existence of X.a.b.c

bool = true;
if ~isempty(varargin),
    fld1 = varargin{1};
    bool = isfield(X,fld1) && isfield2(X(1).(fld1),varargin{2:end});
end

end
