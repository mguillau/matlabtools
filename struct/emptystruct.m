function s = emptystruct(varargin)
% Returns an empty struct with the specified fields

args = cell(1,length(varargin)*2);
args(1:2:end) = varargin;
args(2:2:end) = repmat({{}},1,length(varargin));
s = struct(args{:});
