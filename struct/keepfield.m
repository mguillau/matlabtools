function struct_out = keepfield(struct_in,fld)
% Strip down a struct so as to retain only the specified fields

if ischar(fld),
    fld = {fld};
end

struct_out = struct();
for i=1:length(struct_in);
    for f=1:length(fld),
        struct_out(i).(fld{f}) = struct_in(i).(fld{f});
    end
end
