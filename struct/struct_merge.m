function out = struct_merge(varargin)
% Merge structs that are consistent on existing fields

if nargin<1,
  error('at least one input required');
end

out = varargin{1};

for i=2:nargin, % loop over arguments
   astr = varargin{i};
   afld = fieldnames(astr);
   for o=1:length(afld), % loop over fields
       if isfield(out,afld{o}), % fields collapse
           % check equality (especially for imID) or merge
           for k=1:length(out),
               if isstruct(out(k).(afld{o})),
                   out(k).(afld{o}) = struct_merge(out(k).(afld{o}),astr(k).(afld{o}));
               else
                   iseq = deep_eq(out(k).(afld{o}), astr(k).(afld{o}));
                   if ~iseq,
                       disp(out)
                       disp(out(k).(afld{o}));
                       disp(astr)
                       disp(astr(k).(afld{o}));
                       assert(false);
                   end
                   %assert( all(reshape(eq( out(k).(afld{o}), astr(k).(afld{o}) ),[],1)) );
               end
           end
       else % fields do not collapse
           for k=1:length(out),
               out(k).(afld{o}) = astr(k).(afld{o});
           end
       end
   end
end

end
