function out = struct_merge_default(varargin)
% Merge structs keeping only the last version of each field

if nargin<1,
  error('at least one input required');
end

out = varargin{1};
validate(out);

for i=2:nargin, % loop over arguments
   astr = varargin{i};
   validate(astr);
   afld = fieldnames(astr);
   for o=1:length(afld), % loop over new fields
       nfld = afld{o};
       if isfield(out,nfld) && isstruct(out.(nfld)) && isstruct(astr.(nfld)), % fields collapse
           out.(nfld) = struct_merge_default(out.(nfld),astr.(nfld));
       else % fields do not collapse
           out.(nfld) = astr.(nfld);
       end
   end
end

end

function validate(str)
if ~isstruct(str),
    error('inputs must be structs')
end
if length(str)>1,
    error('struct arrays not implemented yet')
end
end
