function struct_copy(to,from,varargin)
% call with command syntax
% to = target struct
% from = source struct
% varargin = list of members to be copied

n = length(varargin);

for i = 1:n
	stmt = sprintf('%s.%s = %s.%s;',to,varargin{i},from,varargin{i});
	evalin('caller',stmt);
end;
