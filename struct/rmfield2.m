function str = rmfield2(str,varargin)
% RMFIELD2 Remove several fields without error if a field does not exist
assert(isstruct(str));

if ischar(varargin{1}),
    flds = varargin;
else
    flds = varargin{1};
end
assert(iscell(flds));

for f=1:length(flds),
    fld=flds{f};
    if ischar(fld) && isfield(str,fld),
        str=rmfield(str,fld);
    end
end
