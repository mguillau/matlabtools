function s = nameValueToStruct(varargin)
% nameValueToStruct builds a struct from name-value pairs
% 
% s = nameValueToStruct(name,value,...) varargin list of name-value pairs
% s = nameValueToStruct({name value ... }) 1d cell array of name-value pairs
% s = nameValueToStruct({name value ; ... }) 2d cell array of name-value pairs, one pair per line
% 
% Works with no name-value pairs and returns a struct with no fields.
% There is no support for returning arrays of structs.

if length(varargin)==1
	% input given as either 1d or 2d cell array
	varargin = varargin{1};
	if ndims(varargin)==1
		% input given as 1d cell array: name, value, ...
	elseif ndims(varargin)==2
		% input given as 2d cell array: { name value ; ... }
		varargin = varargin';
		varargin = { varargin{:} }; %#ok<CCAT>
	else
		error('For a single argument, a 1d or 2d cell array is needed.');
	end;
end;

assert(mod(length(varargin),2)==0,'Even number of arguments needed for name-value pairs.');

s = struct();

for i = 1:2:length(varargin)
	s.(varargin{i}) = varargin{i+1};
end;
clear i;
