function [Z,Y] = structfun2(fun,X)
% STRUCTFUN2 Apply a function to each field of a struct
%
% Z = structfun2(fun,X)
% Applies fun to each field of X, returns a struct with the same fields
% The function fun can take one or two arguments
%   - @(x) : the data,
%   - @(f,x): the field name f, the data x.
%
% [Z,Y] = structfun2(fun,X) also returns Y, the cell array of field values.
% Y = struct2cell(Z), but saves the function call.

fld = fieldnames(X);

try
   Y = cellfun2(@(f) feval(fun,f,X.(f)),fld);
catch e
    try
        Y = cellfun2(@(f) feval(fun,X.(f)),fld);
    catch e2,
        rethrow(e);
    end
end

for f=1:length(fld),
   Z.(fld{f}) = Y{f};
end

