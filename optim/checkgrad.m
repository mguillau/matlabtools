function d = checkgrad(f, X, f_opts, e, d, varargin);
% CHECKGRAD Check the derivative of a function
%
% checkgrad checks the derivatives in a function, by comparing them to finite
% differences approximations. The partial derivatives and the approximation
% are printed and the norm of the diffrence divided by the norm of the sum is
% returned as an indication of accuracy.
%
% usage: checkgrad(fun, X, {P1, P2, ...}, e, d)
%
% where X is the argument and e is the small perturbation used for the finite
% differences. d is the set of dimensions to explore, give [] for exploring all dimensions
% and the P1, P2, ... are optional additional parameters which
% get passed to f. The function fun (provided as string or function handle) should be of the type 
%
% [fX, dfX] = f(X, P1, P2, ...)
%
% where fX is the function value and dfX is a vector of partial derivatives.
%
% e and d are optional, the defaults are e=1e-6, d=[];
%
% Carl Edward Rasmussen, 2001-08-01.
% Modified by Matthieu Guillaumin

default e 1e-6;
default d [];

[y dy] = feval(f,X,f_opts{:});                         % get the partial derivatives dy

dh = zeros(length(X),1) ;
for j = 1:length(X)
  progress_bar(length(X),j);
  if ismember(j,d) || isempty(d),
     dx = zeros(length(X),1);
     dx(j) = dx(j) + e;                               % perturb a single dimension
     y2 = feval(f,X+dx,f_opts{:});
     dx = -dx ;
     y1 = feval(f,X+dx,f_opts{:});
     dh(j) = (y2 - y1)/(2*e);
  end
end

if ~isempty(d),
  dy=dy(d);
  dh=dh(d);
end

disp([dy(:) dh(:)]')                                          % print the two vectors
d = norm(dh-dy)/norm(dh+dy);       % return norm of diff divided by norm of sum
