function [f, g] = fg_logisticdiscriminant(w,data,labels,ss,weighted)
% FG_LOGISTICDISCRIMINANT Negative log-likelihood and gradient for logistic discriminant
%
% [f, g] = logdis_f_g(v,data,labels,ss,weighted)
% calculate the negative log-likelihood (and its gradient) of the labels under logistic model for data
% a weighted log-likelihood is used: the log-likelihood of each outcome is weighted by the corresponding entry in labels
%
% v     (DC x 1) : concatenation of the columns of the parameter matrix (D x C)
% data   (N x d) : each row represents a D dimensional data point
% labels (N x C) : supervision, maximize sum_n log sum_c labels(n,c) p(c|data_n) (default)
% or if weighted=1: maximize sum_n sum_c labels(n,c) log p(c|data_n)
% 
% in case of partial labeling set 'labels' to 1 for each possible label, so the objective is to output one of the possible labels 

default ss 0;
default weighted 0;

[N,D]   = size(data);
C       = size(labels,2);

p       = -full(data * reshape(w,D,C));
p       = softmax(p,2);

if weighted==1
    f       = full(labels(:)'*log(eps+p(:)));    
    labels  = p.*(sum(labels,2)*ones(1,C)) - labels;
else
    labels  = p.*labels;
    f       = full(sum(log(sum(labels,2))));
    labels  = p - normalize2(labels,2);   
end

g       = data'*labels;
g       = full(g(:));
    
if ss>0; % use gaussian prior N(w; 0, ss) on weights, ie. MAP estimation
    f = f - w'*w/(2*ss) - length(w)*log(ss)/2;
    g = g - w/ss; 
end

f = -f/N; % we minimize the -negative- log-likelihood
g = -g/N;

