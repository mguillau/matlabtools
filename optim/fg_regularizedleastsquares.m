function [f,G] = fg_regularizedleastsquares(w,X,T,lambda)
% FG_REGULARIZEDLEASTSQUARES Loss and gradient for regularized least squares
%
% w is d-by-1, X is N-by-d, T is N-by-1, lambda is a scalar
%
% >> w_min=minimize(randn(size(X,2),1),@fg_regularizedleastsquares,{X,T,lambda});
% >> w_ext=inv(lambda*eye(size(X,2))+X*X')*X*T';
% >> norm(w_min-w_ext)/norm(w_min+w_ext)

default lambda 0;
assert(length(w)==size(X,2));

f = sum((T-X*w).^2) + lambda*w'*w;
f = f/2;
G = -X'*(T-X*w) + lambda*w;

