function [err,centroids,assign,dis] = mg_kmeans(varargin)
% [err,centroids,assign,dis] = mg_kmeans(varargin);
%
% Mandatory inputs:
% data(d, n)        vectors to cluster
% k                 number of centroids
% Optional inputs:
% niter             number of iterations (default=20)
% redo              perform clustering this many times and keep clusters with smallest quantization error (default=1)
%
% Outputs:
% err               converged error function
% centroids(d, k)   output centroids
% assign(n)         index of assigned centroid in 0..k-1
% dis(n)            squared distance to assigned centroid of each input vector

[err,centroids,assign,dis] = mg_kmeans_mex(varargin{:});
