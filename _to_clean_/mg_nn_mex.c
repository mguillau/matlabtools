/* Compile with
 *  mex -I../extern/yael_v119/yael -L../extern/yael_v119/yael -lyael mg_nn_mex.c 
 */

#include <sys/types.h>
#include <unistd.h>
#include "mex.h"
#include "matrix.h"

#include "../extern/yael_v119/yael/nn.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    int maxthreads = 8;
    
    float *data,*centroids;
    int k,n,n2,d,d2,*nn;
    
    if(nrhs<2)
        mexErrMsgTxt("Two inputs required.");
    else if(nlhs > 1)
        mexErrMsgTxt("Too many output arguments.");
    else if(!mxIsSingle(prhs[0]))
        mexErrMsgTxt("Input must be a single-precision array.");

    data = (float*) mxGetData(prhs[0]);
    n = mxGetN(prhs[0]); /* number of cols */
    d = mxGetM(prhs[0]); /* number of rows */
    centroids = (float *) mxGetData(prhs[1]);
    n2 = mxGetN(prhs[1]); /* number of cols */
    d2 = mxGetM(prhs[0]); /* number of rows */
    
    if (d!=d2)
        mexErrMsgTxt("Both inputs must have the same dim.");

    /* write output */
    plhs[0] = mxCreateNumericMatrix(1, n, mxUINT16_CLASS, mxREAL);
    nn = (int *) mxGetData(plhs[0]);
    
    /* nn_thread (int n, int nb, int d, 
                const float *b, const float *v,
                int *assign,    
                int n_thread,
                void (*peek_fun) (void *arg,double frac),
                void *peek_arg); */
    nn_thread( n, n2, d, centroids, data, nn, maxthreads, NULL, NULL );

    for (k=0;k<n;k++) /* 1-based indexing in matlab */
        nn[k]++;
    
}
