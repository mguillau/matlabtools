function varargout = getGridSearchGrid(varargin)
% varargin = {axis1,axis2,...},{axis1,axis2,...},...
% grids will be joined
% TODO it's not super-efficent, but should be enough, it's only a grid after all

% build all grids
out = cell(length(varargin),nargout());
for i = 1:length(varargin)
	[out{i,:}] = build(varargin{i}{:});
end; clear i;

% join grids
varargout = cell(1,nargout());
for i = 1:nargout()
	varargout{i} = cat(1,out{:,i});
end; clear i;

% find unique combinations
varargout = findUnique(varargout);


function varargout = build(varargin)

varargout = cell(1,nargout());
assert(nargout()==length(varargin));

if length(varargin)==1
	
	varargout{1} = varargin{1}(:);
	
else
	
	part = cell(1,length(varargin)-1);
	[part{:}] = build(varargin{2:end});
	
	for i = 1:length(part)
		varargout{1+i} = repmat(part{i},length(varargin{1}),1);
	end; clear i;
	
	varargout{1} = repmat(varargin{1}(:)',length(part{1}),1);
	varargout{1} = varargout{1}(:);
	
end;


function uni = findUnique(all)

k = length(all);
n = length(all{1});

keep = true(n,1);
for i = 1:n
	keep(i) = notYetAppeared(i,all);
end; clear i;

uni = cell(1,k);
for i = 1:k
	uni{i} = all{i}(keep);
end; clear k:


function a = notYetAppeared(i,all)

a = true;
for j = 1:i-1
	if allEqual(i,j,all)
		a = false;
		break;
	end;
end; clear j;


function a = allEqual(i,j,all)

a = true;
for k = 1:length(all)
	if ~isequal(all{k}(i),all{k}(j))
		a = false;
		break;
	end;
end;  clear k;
