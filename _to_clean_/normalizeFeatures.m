function [data nmean nstd] = normalizeFeatures(data,nmean,nstd)

n = size(data,1);

if ~exist('nmean','var')
	nmean = mean(data,1);
end;

data = data - repmat(nmean,n,1);

if ~exist('nstd','var')
	nstd = std(data,[],1);
	nstd(nstd==0) = 1;
end;

assert(all(nstd(:)>0));
data = data ./ repmat(nstd,n,1);
