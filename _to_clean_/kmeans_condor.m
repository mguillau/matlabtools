function centroids = kmeans_condor(file,data,K)

iterations = inf;
delta = eps;

data = data'; % vgg works in transposed space

if file_exist(file)
	state = load_struct(file);
	assert(all(all(state.data==data)),'kmeans data seems to have changed');
	assert(state.K==K);
else
	state.data = data;
	state.K = K;
	state.centroids = [];
	state.iterations = 0;
	state.converged = false;
end;

clear data K;

if ~state.converged

	assert(size(state.data,2)>=state.K,'less data points then clusters');

	if isempty(state.centroids)
		perm = randperm(size(state.data,2));
		state.centroids = state.data(:,perm(1:state.K));
		clear perm;
	end;

	pg_begin('kmeans','iteration %d: sse=%g (*%f)');
	sse0 = inf;
	while state.iterations<iterations

		[state.centroids sse] = vgg_kmiter(state.data,state.centroids);    
		state.iterations = state.iterations+1;

		pg_update(state.iterations,sse,sse/sse0);

		if sse0-sse < delta
			state.converged=true;
			break;
		end

		sse0=sse;
		
		csave_struct(file,state);

	end;
	pg_end('%d iterations',state.iterations);
	
	save_struct(file,state);

end;

centroids = state.centroids';
