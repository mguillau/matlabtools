/* Compile with
 *  mex -I../extern/yael_v119/yael -L../extern/yael_v119/yael -lyael mg_kmeans_mex.c 
 */

#include <sys/types.h>
#include <unistd.h>
#include "mex.h"
#include "matrix.h"

#include "../extern/yael_v119/yael/kmeans.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    float *data,*centroids,*dis,*err;
    int k,d,n,niter,flags,redo,*assign,*nassign;
    long seed=0;
    
    if(nrhs<2)
        mexErrMsgTxt("Two inputs required.");
    else if(nlhs > 4)
        mexErrMsgTxt("Too many output arguments.");
    else if(!mxIsSingle(prhs[0]))
        mexErrMsgTxt("Input must be a single-precision array.");

    data = (float*) mxGetData(prhs[0]);
    n = mxGetN(prhs[0]); /* number of cols */
    d = mxGetM(prhs[0]); /* number of rows */
    k = (int) *mxGetPr(prhs[1]); /* number of centroids */
    
    mexPrintf("%d points (%d dims) clustered into %d.\n",n,d,k);
    
    if (nrhs<3) /* number of iterations */
        niter = 20;
    else
        niter = (int) *mxGetPr(prhs[2]);
    
    if (nrhs<4) /* number of redos */
        redo = 1;
    else
        redo = (int) *mxGetPr(prhs[3]);   
    
    flags = 8 | KMEANS_INIT_RANDOM; /* use 8 threads and random init */

    /* write output */
    plhs[0] = mxCreateNumericMatrix(1, 1, mxSINGLE_CLASS, mxREAL);
    err = (float*) mxGetData(plhs[0]);
    
    plhs[1] = mxCreateNumericMatrix(d, k, mxSINGLE_CLASS, mxREAL);
    centroids = (float*) mxGetData(plhs[1]);
    
    if (nlhs>2) {
        plhs[2] = mxCreateNumericMatrix(n, 1, mxINT32_CLASS, mxREAL);
        assign = (int*) mxGetData(plhs[2]);
    } else
        assign = NULL;
    
    if (nlhs>3) {
        plhs[3] = mxCreateNumericMatrix(n, 1, mxSINGLE_CLASS, mxREAL);
        dis = (float*) mxGetData(plhs[3]);
    } else
        dis = NULL;
    
    nassign = NULL;
    
    *err = kmeans( d, n, k, niter, data, flags, seed, redo, centroids, dis, assign, nassign);
    
    if (assign) { /* 1-based indexing in matlab */
        while (n)
            assign[--n]++;
    }
    
}
