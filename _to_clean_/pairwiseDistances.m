function [distances scale] = pairwiseDistances(features,distance,normalize)
% features are row vectors, or a cell array with such elements {from to}
% distance is a function handle to distance_*
% if normalize, then distances are rescaled, so that the maximum is at 1, not gaussian-like normalized
% the minimum at 0 is not enforced, but should be because distance(a,a) = 0 normally
% (not the features are being normalized!)
% scale returns the normalization

if iscell(features)
	from = features{1};
	to = features{2};
else
	from = features;
	to = features;
end;

n = size(from,1);
m = size(to,1);
distances = zeros(n,m);

parfor i = 1:n
	% TODO fuer distanz auf sich selber, matrix symmetrisch, nur halb soviel arbeit
	distances(i,:) = distance(from(i,:),to)';
end; clear i;

if normalize
	m = max(distances(:));
	assert(m>0,'max distance is %g',m);
	scale = 1.0/m;
	distances = distances * scale;
else
	scale = 1;
end;
