function data = normalizeRowSums(data,unit,zeroerror)
% Normalizes rows of data to unit.
% Rows with zeros sum ever error or default to uniform.
%
% data
%	Works for sparse, but not optimal speed.
% unit (optional, default 1)
% zeroerror (optional, default true)
%	true, stop on zero sum for a row
%	false, uniform distribution for zero sum rows

if ~exist('unit','var'), unit = 1; end;
if ~exist('zeroerror','var'), zeroerror = true; end;

sums = full(sum(data,2));
% Even if data was sparse, sums will likely not be sparse.
% Final normalization is faster with non-sparse sums.

if zeroerror
	assert(all(sums>0),'normalizeRowSums contains <=zero sums');
else
	if any(sums==0)
		% Slow for sparse, but there should not be many cases
% 		if sum(sums==0)>0.2*length(sums), warning('normalizeRowSums:sums','%d 0s out of %d',sum(sums==0),length(sums)); end;
		data(sums==0,:) = 1/size(data,2);
		sums(sums==0) = 1;
	end;
end;

data = data ./ repmat(sums,1,size(data,2)) * unit;
