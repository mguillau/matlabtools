function r = str_endswith(str,with)

if length(str)<length(with)
	r = false;
else
	l = length(with);
	r = strcmp(str(end-l:end),with);
end;
