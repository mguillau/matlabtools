function r = str_startswith(str,with)

if length(str)<length(with)
	r = false;
else
	l = length(with);
	r = strcmp(str(1:l),with);
end;
