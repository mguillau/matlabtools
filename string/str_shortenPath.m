function sp = str_shortenPath(lp,max)

arg_assert lp char
arg_assert max numeric default 40

n = length(lp);

if n>max
	a = floor((max-3)*1/3);
	b = floor((max-3)*2/3);
	sp = [ str_head(lp,a) '...' str_tail(lp,b) ];
else
	sp = lp;
end;
