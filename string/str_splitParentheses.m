function strings = str_splitParentheses(str,chr,varargin)
% strings = str_splitParentheses(str,chr)
%    splits the string str at character chr, ensuring that all parentheses
%    are correctly closed in each substring.
%
% strings = str_splitParentheses(str,chr,...,'option',value,...) to specify options.
% 
% Options:
% - 'depth' (int) specifies the maximum depth of split, starting from 0.
%     eg: str_splitParentheses('<<a|b>|<c|d>>','|','depth',1)
%     returns { '<<a|b>', '<c|d>>' }
% - 'left', 'right' (string) specifies the opening and closing parentheses. by
%     default: left='{([<'; right = '})]>';  the one-to-one match between
%     opening and closing parenthesis is important!
%     ex: str_splitParentheses('(a,(b,c)),((d,e),(f))',',')
%     returns { '(a,(b,c))', '((d,e),(f))' };
% - 'both' (string) specifies parentheses which have identical opening and
%     closing characters. default: '"'
% - 'clean' (bool) removes the parentheses of the splits

read_varargin;
defaultstr left '{([<';
defaultstr right '})]>';
defaultstr both '"''''';
default depth 0;
default clean 0;

assert(isempty(intersect(left,right)));
assert(isempty(intersect(left,both)));
assert(isempty(intersect(right,both)));

stack = []; % keeping track of types of opened parenthesis
pos   = []; % and their position (for debugging info)
i=1; % starting pos of word
j=1; % ending pos of word
w=1; % number of words
for k=1:length(str),
    iop = find(str(k)==left); % find type of opening parenthesis
    icl = find(str(k)==right); % or closing
    ibo = -find(str(k)==both); % or both. use minus to differentiate between op/cl and special.
    opening = ~isempty(iop) || ( ~isempty(ibo) && ( isempty(stack) || ibo~=stack(end)) );
    closing = ~isempty(icl) || ( ~isempty(ibo) && (~isempty(stack) && ibo==stack(end)) );
    if opening, % if opening
%         fprintf('opening %s at char %d\n',str(k),k);
        stack(end+1) = [iop ibo]; % pile onto stack. one of these guys is empty
        pos(end+1)   = k;   % and keep position
        if clean && length(stack)<=depth, % ignore opening parenthesis character for extracted string
            i=i+1;
        end
        j=j+1;
    elseif closing, % if closing
%         fprintf('closing %s at char %d\n',str(k),k);
        if [icl ibo]==stack(end), % check consistency
            if ~clean || length(stack)>depth, % ignore closing parenthesis character for extracted string
                j=j+1;
            end
            stack = stack(1:end-1); % pop stack
            pos   = pos(1:end-1);
        else % report debuging info
            error('Invalid string: "%s" at char %d not matching "%s" at char %d',right(icl),k,left(stack(end)),pos(end));
        end
    else
        if length(stack)<=depth && str(k)==chr, % extract string when splitting character is found
            strings{w} = str(i:j-1);
            i=k+1;
            j=k+1;
            w=w+1;
        else
            j=j+1;
        end
    end
end
if isempty(stack), % check for empty stack
    strings{w} = str(i:j-1); % extract the last string
else
    disp(left(stack));
    error('Invalid string: %d parentheses not properly closed',length(stack));
end

