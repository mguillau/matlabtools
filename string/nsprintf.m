function [s fmt] = nsprintf(t,n,o,a)

default a 1;
if a<1 || a>3,
    a=1;
end

if t(1)=='%',
    u=t(1:end-1);
    t=t(end);
else
    u='%';
end
switch a,
    case 1, % left-align
        fmt = [ u sprintf('%d',n) t ];
        s = sprintf(fmt,o); % string with full leading space
    case 2, % right-align
        fmt = [ u t ];
        s = sprintf(fmt,o); % string with no leading space
        s = [ s repmat(' ',1,max(n-length(s),0)) ];
    case 3, % center
        fmt = [ u t ];
        s = sprintf(fmt,o); % string with no leading space
        r = (n-length(s))/2;
        s = [ repmat(' ',1,max(ceil(r),0)) s repmat(' ',1,max(floor(r),0)) ];
end

