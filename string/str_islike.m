function ok = str_islike(s,p)
% Does s adhere to the regexp p (in total)?

[a,b] = regexp(s,p);

if length(a)==1
	ok = a==1 && b==length(s);
else
	ok = false;
end;
