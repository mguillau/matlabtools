#include <sys/types.h>
#include <unistd.h>
#include "mex.h"
#include "matrix.h"

unsigned long strhash(unsigned char *str) {
    unsigned long hash = 5381;
    int c;
    while (c=*str++) /* will stop when the NULL char is found */
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    return hash;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    
unsigned long *pr, strlen;
unsigned char *str;

/* read input string */
if(nrhs!=1)
    mexErrMsgTxt("One input required.");
else if(nlhs > 1)
    mexErrMsgTxt("Too many output arguments.");
else if(!mxIsChar(prhs[0]))
    mexErrMsgTxt("Input must be a string.");
str = (unsigned char *) mxArrayToString(prhs[0]);

/* write output int */
plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
pr = mxGetData(plhs[0]);
*pr = strhash(str);

}
