function s = str_fromcount(c,format)
% Transforms a count (integer) into a human readable string.
% format = a (default) | h | e
%   a = using ' delimiters for 10^3 steps
%   h = using k,m,g, and 10^x after that
%   'e' = using 10^x notation

arg_assert c numeric
arg_assert format char default '''a'''

assert(rem(c,1)==0); % integer value

switch format
	case 'a'
		s = sprintf('%d',c');
		assert(str_islike(s,'[0-9]*'));
		s = insertApostrophs(s);
	case 'h'
		if c>1e9
			s = sprintf('%.1fg',c/1e9);
		elseif c>1e6
			s = sprintf('%.1fm',c/1e6);
		elseif c>1e3
			s = sprintf('%.1fk',c/1e3);
		else
			s = sprintf('%d',c);
		end;
	case 'e'
		s = sprintf('%.1e',c);
	otherwise
		assert(false,'uknown format %s',format);
end;


function s = insertApostrophs(s)

if length(s)>3
	s = [ insertApostrophs(s(1:end-3)) '''' s(end-2:end) ];
end;
