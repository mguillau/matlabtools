function s = str_frombytes(s)
% Human readable memory size string.

if s<1e3
	s = sprintf('%db',s);
elseif s<1e6
	s = sprintf('%.1fkb',s/1e3);
elseif s<1e9
	s = sprintf('%.1fmb',s/1e6);
else
	s = sprintf('%.1fgb',s/1e9);
end;
