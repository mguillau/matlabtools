function bool = str_match(str, pattern)
% return true iff str matches pattern
% if str is a string cell array, bool is a logical vector

if ischar(str),
    str = {str};
end

bool = cellfun(@(x) ~isempty(x),strfind(str,pattern));

