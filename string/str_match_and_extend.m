function Y = str_match_and_extend(X,pattern,replacements)

Y=X;

idx = find(str_match(Y,pattern));
for i=1:length(idx),
    for r=1:length(replacements),
        Y{end+1} = regexprep(Y{idx(i)},pattern,replacements{r});
    end
end
Y(idx)=[];
