function fprintfn(format)
% Python-like named fprintf.
% Copy of sprintf but with fprintf at the end.
% Nested call won't work because of evalin('caller',...)

[~,~,~,~,variables] = regexp(format,'%\[([^\]]*)\]');
format_raw = regexprep(format,'%\[([^\]]*)\]','%');

n = length(variables);
values = cell(1,n);
for i = 1:n
	values{i} = evalin('caller',variables{i}{1});
end;

fprintf(format_raw,values{:});
