function varargout = str_findchar(varargin)
% idx = str_findchar(string,start_index,character)
% 
% Find locations of the character 'character' in the string 'string' after start_index (1-based as in matlab).
% Returns an empty list if character is not found.

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile;
