function str = data2str(s)
% Create a string from matlab data. The resulting string is parsable with str2data().

if isempty(s),
    str = '';
end

if ischar(s),
    str = ['"' s '"'];
    return;
end

if isnumeric(s) && numel(s)==1,
    if double(int64(s))==s,
        str = sprintf('%d',s);
    else
        str = sprintf('%f',s);
    end
    return;
end

if iscell(s),
    str = [ '{' str_join(cellfun2(@data2str,s),',') '}' ];
    return;
end

if isnumeric(s),
    str = [ '[' str_join(arrayfun2(@data2str,s),',') ']' ];
    return;
end

if isstruct(s) && numel(s)==1,
    F = fieldnames(s);
    V = cellfun2(@(x) s.(x),F);
    str = [ '(' str_join(cellfun2(@(k,v) [k '=' data2str(v)],F,V),',') ')' ];
    return;
end

error('not implemented yet');

