function s = str2data(str)
% Parse a string into a matlab data. Cf data2str().

str = regexprep(str,'\s*',''); % remove whitespace

if isempty(str), % empty array
    s=[];
    return;
end

switch str(1),
    case '(', % a struct
        assert(str(end)==')');
        F = str_splitParentheses(str(2:end-1),',','depth',0);
        for i=1:length(F),
            V = str_splitParentheses(F{i},'=','depth',0);
            s.(V{1}) = str2data(V{2});
        end

    case '[', % an array
        assert(str(end)==']');
        F = str_splitParentheses(str(2:end-1),',','depth',0);
        s = cellfun(@str2data,F);

    case '{', % a cell array
        assert(str(end)=='}');
        F = str_splitParentheses(str(2:end-1),',','depth',0);
        s = cellfun2(@str2data,F);

    case '"', % a string
        assert(str(end)=='"');
        s = str(2:end-1);

    otherwise,
        s=str2double(str); % try to parse a number
        if isnan(s) && ~strcmp(str,'NaN'), % it failed, assume it's a string
            s = str;
        end
end

