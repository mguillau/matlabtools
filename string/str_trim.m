function string = str_trim(string,characters)
% STR_TRIM remove the tailing sequence of provided characters from the input string

defaultstr characters ' ';

idx = ismember(string,characters);
vidx = find(~idx,1,'last'); % last position of character to keep
idx(1:vidx) = false;
string(idx) = '';
