function S = str_split(text,pattern)
% S = str_split(text,pattern)
%
% Splits a string 'text' into substrings according to a pattern 'pattern'
% and returns the cell of substrings.
% 1. If pattern is a string, then text is split at each occurence of
% pattern using regexp. The occurrences of pattern are removed.
% 2. If pattern is a number N, text is split in blocks of size N.
% 3. If pattern is a sequence of numbers, text is split folowing the
% sequence of block sizes. If text is longer than pattern, a last block of
% variable length will be outputted.

if ischar(pattern),
    S = regexp(text,pattern,'split');
elseif isnumeric(pattern) && numel(pattern)==1 && pattern>0 && isfinite(pattern),
    nc = ceil(length(text)/pattern);
    S = cell(1,nc);
    for i=1:nc-1,
        S{i} = text(pattern*(i-1)+1:pattern*i);
    end
    S{end} = text((nc-1)*pattern+1:end);
elseif isnumeric(pattern) && all(pattern>0) && all(isfinite(pattern)),
    cs = [0 cumsum(pattern)];
    nc = find(cs>=length(text),1,'first')-1;
    if isempty(nc),
        nc = length(pattern)+1;
    end
    S = cell(1,nc);
    for i=2:nc,
        S{i-1} = text(cs(i-1)+1:cs(i));
    end
    S{nc} = text(cs(nc)+1:end);
else
    error('Invalid input');
end
