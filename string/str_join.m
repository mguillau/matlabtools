function str = str_join(strings,sep)
% str = str_join(strings,sep) does the opposite of strings = regexp(str,sep,'split')
if isempty(strings),
    str='';
    return;
end
n = length(sep)+1;
strings = cellfun2(@(X) [ sep X ],strings);
str = [strings{:}];
if length(str)>=length(sep),
    str = str(n:end);
end
