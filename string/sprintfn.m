function string = sprintfn(format)
% Python-like named sprintf.

[~,~,~,~,variables] = regexp(format,'%\[([^\]]*)\]');
format_raw = regexprep(format,'%\[([^\]]*)\]','%');

n = length(variables);
values = cell(1,n);
for i = 1:n
	values{i} = evalin('caller',variables{i}{1});
end;

string = sprintf(format_raw,values{:});
