function varargout = str_hash(varargin)
% hash = str_hash(string)
% 
% Compute a uint32 hash for an arbitrary string.

ext_source = '.c';
ext_mex = mexext;
funcName = mfilename;
fileName = mfilename('fullpath');
sourceName = [ fileName ext_source ];
mexName = [ fileName '.' ext_mex ];
fprintf('compiling %s\n',mexName);
mex(sourceName,'-output',mexName);
varargout=cell(nargout,1);
[varargout{:}] = feval(funcName,varargin{:});
