function str2 = str_texprotect(str)
% ensure that the string has no unprotected '_'.
% use for figure titles/caption/legend/axis to avoid TeX-like
% interpretation

str2 = regexprep(str,'_','\\_');
