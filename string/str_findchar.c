#include <sys/types.h>
#include "mex.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
mwSize idx, len;
mxChar *str, chr;

if (nrhs != 3)
    mexErrMsgTxt("Must provide three inputs");

len  = mxGetNumberOfElements(prhs[0]) + 1;
str  = mxGetChars(prhs[0]);
        
idx  = ((mwSize) mxGetScalar(prhs[1])) - 1;
chr  = mxGetChars(prhs[2])[0];

while (idx<len && (str[idx++]!=chr)) {}

if (idx<len) {
    plhs[0] = mxCreateDoubleScalar((double) idx);
} else {
    plhs[0] = mxCreateNumericMatrix(1, 0, mxDOUBLE_CLASS, mxREAL);
}

}
