function M=json2mat(J)
%JSON2MAT converts a javascript data object (JSON) into a Matlab structure
%         using a recursive approach. J can also be a file name.
%         When reading a file, lines starting with the '#' character are ignored.
%
%Example: lala=json2mat('{lele:2,lili:4,lolo:[1,2,{lulu:5,bubu:[[1,2],[3,4],[5,6]]}]}')
%         notice lala.lolo{3}.bubu is read as a 2D matrix.
%
% Jonas Almeida, March 2010
% Matthieu Guillaumin, September 2013

if exist(J,'file')==2 % if J is a filename
    fid=fopen(J,'rt');
    J='';
    while ~feof(fid) % load while removing newlines and commented lines
        li=fgetl(fid);
        if length(li)>0 && li(1)~='#',
            J=[J,li];
        end
    end
    fclose(fid);
    M=json2mat(J);
else
    J=regexprep(J,'^\s*',''); % remove leading whitespace
    J=regexprep(J,'\s*$',''); % remove trailing whitespace
    if isempty(J)
        M='';
    elseif J(1)=='{' %extract structures
        assert(J(end)=='}');
        JJ=J(2:end-1);
        if ~isempty(JJ)
            JJ=str_splitParentheses(JJ,','); % separate fields
            JJ=cellcat(2,cellfun2(@(x) str_splitParentheses(x,':'),JJ)); % separate fieldnames from values and flatten
            JJ=cellfun2(@json2mat,JJ); % recursive call to json2mat
            % enclose cells in a extra cell to prevent the creation of a struct array
            ic=cellfun(@iscell,JJ);
            JJ(ic)=cellfun2(@(x) {x},JJ(ic));
            M=struct(JJ{:}); % build the struct: let matlab decide whether it's valid input or not.
        else
            M=struct();
        end
    elseif J(1)=='[' %extract cells
        assert(J(end)==']');
        JJ=J(2:end-1);
        if ~isempty(JJ)
            JJ=str_splitParentheses(JJ,','); % separate fields
            M=cellfun2(@json2mat,JJ); % recursive call to json2mat
        else
            M={};
        end
        try
            assert(all(arrayfun(@isstruct,M))); % merge only structs
            M=cell2mat(M); % try converting to array: will succeed if all elements are consistent.
        end
    elseif J(1)=='"' %literal string
        assert(J(end)=='"');
        JJ=J(2:end-1); %regexp(J,'\"(.*)\"','tokens');
        if ~isempty(JJ)
            M=JJ;
        else
            M='';
        end
    else %numeric value
        j = str2double(J);
        if ~isnan(j) || strcmp(J,'NaN'), %it is a number
            M=j; % is numeric value
        else
            j = str2num(J); %#ok<ST2NM>
            if isempty(j),
                M=J; % not a number after all
            else
                M=j;
            end
        end
    end
end


