function struct_out=str2struct(str_in,varargin)
assert(ischar(str_in));

read_varargin;
defaultstr strsyntax.struct_left '(';
defaultstr strsyntax.struct_right ')';
defaultstr strsyntax.struct_sep ',';
defaultstr strsyntax.struct_field '=';
defaultstr strsyntax.cell_left '{';
defaultstr strsyntax.cell_right '}';
defaultstr strsyntax.cell_sep ',';
defaultstr strsyntax.array_left '[';
defaultstr strsyntax.array_right ']';
defaultstr strsyntax.array_linesep ';';
defaultstr strsyntax.array_sep ',';
defaultstr strsyntax.hash '#';

syntaxchar = unique(cellcat(2,struct2cell(strsyntax)'));

str_in=regexprep(str_in,'^\s*',''); % remove leading whitespace
str_in=regexprep(str_in,'\s*$',''); % remove trailing whitespace
has_hash = ~isempty(str_in) && str_in(end)==strsyntax.hash;
if has_hash, % remove trailing hash
    i=length(str_in)-1; while str_in(i)~=strsyntax.hash, assert(ismember(str_in(i),'1234567890abcdef')); i=i-1; end;
    hash = str_in(i+1:end-1);
    str_in = str_in(1:i-1);
end
if isempty(str_in)
    struct_out=[];
elseif str_in(1)==strsyntax.struct_left %extract structures
    assert(str_in(end)==strsyntax.struct_right);
    str_in=str_in(2:end-1);
    if ~isempty(str_in)
        str_in=str_splitParentheses(str_in,strsyntax.struct_sep); % separate fields
        str_in=cellfun2(@(x) str_splitParentheses(x,strsyntax.struct_field),str_in); % separate fieldnames from values
        assert(all(vec(bsxfun(@eq,cellsizes(str_in),[1 2]))),'bad formatting of struct'); % there should be exactly two elements to define each field
        str_in=cellcat(2,str_in); % flatten
        str_in=cellfun2(@str2struct,str_in); % recursive call for values. fields should remain unchanged
        % enclose cells in a extra cell to prevent the creation of a struct array
        ic=cellfun(@iscell,str_in);
        str_in(ic)=cellfun2(@(x) {x},str_in(ic));
        struct_out=struct(str_in{:}); % build the struct: let matlab decide whether it's valid input or not.
    else
        struct_out=struct();
    end
elseif str_in(1)==strsyntax.cell_left %extract cells
    assert(str_in(end)==strsyntax.cell_right);
    str_in=str_in(2:end-1);
    if ~isempty(str_in)
        str_in=str_splitParentheses(str_in,strsyntax.cell_sep); % separate fields
        struct_out=cellfun2(@str2struct,str_in); % recursive call
        try
            assert(all(arrayfun(@isstruct,struct_out))); % merge only structs
            struct_out=cell2mat(struct_out); % try converting to array: will succeed if all elements are consistent.
        end
    else
        struct_out={};
    end
else % try numeric value, or default to string
    j = str2double(str_in);
    if ~isnan(j) || strcmp(str_in,'NaN'), %it is a number
        struct_out=j; % is numeric value
    else
        %j = str2num(str_in); %#ok<ST2NM>
        %if isempty(j),
        %    assert(str_in(1)~=strsyntax.hash,'can''t parse hashed data');
        %    %assert(~any(ismember(str_in,syntaxchar)),sprintf('syntax characters "%s" are forbidden in strings: "%s"',syntaxchar,str_in)); % forbid strsyntax characters
            struct_out=str_in; % not a number after all
        %else
        %    struct_out=j;
        %end
    end
end

if has_hash,
    h=DataHash(struct_out);
    h=h(1:length(hash));
    if ~strcmp(h,hash),
        warning('hash has changed');
    end
end

