function J=mat2json(M,varargin)
%JSON2MAT converts a Matlab structure into a javscript data object (JSON).
%         M can also be a file name. In the spirit of fast prototyping 
%         this function takes a very loose approach to data types and 
%         dimensionality - neither is explicitly retained.
%
%Example: mat2json(json2mat('{lala:2,lele:4,lili:[1,2,{bubu:5}]}')) 
%
% Jonas Almeida, March 2010.
% Matthieu Guillaumin, September 2013

read_varargin;
default prettyprinting 0;
default thisindent 0;
default indent 2;
default structvalueindent 1;

% add indentation for recursive calls
opts = get_params();
opts.thisindent = opts.thisindent + opts.indent;

if prettyprinting>0,
    nl=sprintf('\n');
    t=repmat(' ',1,thisindent);
else
    nl='';
    t='';
end
ns=length(nl);

if isstruct(M) && numel(M)>1, % handle struct arrays by duplicating field information
    M=mat2cell(M,ones(1,size(M,1)),ones(1,size(M,2)));
end

switch class(M)
    case 'struct'
        f = fieldnames(M);
        k = max(cellfun(@length,f));
        Jrec = cellfun2(@(x) [t,'"',x,'":',mat2json(M.(x),opts,'thisindent',k-length(x)+structvalueindent)],f);
        J = [t,'{',nl,str_join(Jrec,[',',nl]),nl,t,'}'];
        
    case 'cell'
        Jrec = cellfun2(@(x) mat2json(x,opts),M);
        J = [t,'[',nl,str_join(Jrec,[',',nl]),nl,t,']'];
        
    otherwise
        if isa(M, 'function_handle'),
            M=func2str(M);
        end
        if isnumeric(M) || islogical(M) % notice looseness in not converting single numbers into arrays
            if ndims(M)>2
                M=rowvec(M);
                warning('n-dim arrays not supported: reshaped to horizontal vector');
            end
            if numel(M)==1
                J=[t,mynum2str(M)];
            else
                Jrec=arrayfun2(@mynum2str,M);
                s=size(M);
                if (length(s)==2)&(s(1)<2) % horizontal or null vector
                    J=[t,'[',str_join(Jrec,','),']'];
                else %2D matrix
                    J=[t,'[',nl];
                    for i=1:s(1)
                        Ji = str_join(Jrec(i,:),',');
                        J=[J,t,Ji,',',nl];
                    end
                    J=[J,t,']'];
                end
            end
        else
            J=[t,'"',M,'"']; % otherwise it is treated as a string
        end
end
end

function s = mynum2str(n)
if islogical(n), s = bool2str(n); else s = num2str(n); end
end

function s = bool2str(b)
if b, s='true'; else s='false'; end
end
