function str_out=struct2str(struct_in,varargin)

read_varargin;
default hash_level Inf; % below this recursion level, replace string with DataHash
default hash_length Inf;
default hash_append false;
defaultstr strsyntax.struct_left '(';
defaultstr strsyntax.struct_right ')';
defaultstr strsyntax.struct_sep ',';
defaultstr strsyntax.struct_field '=';
defaultstr strsyntax.cell_left '{';
defaultstr strsyntax.cell_right '}';
defaultstr strsyntax.cell_sep ',';
defaultstr strsyntax.array_left '[';
defaultstr strsyntax.array_right ']';
defaultstr strsyntax.array_linesep ';';
defaultstr strsyntax.array_sep ',';
defaultstr strsyntax.hash '#';

syntaxchar = unique(cellcat(2,struct2cell(strsyntax)'));

% options for recursive calls
opts = get_params();
opts.hash_append = false;
if ~isinf(opts.hash_level),
    opts.hash_level = opts.hash_level-1;
end

if hash_level==0,
    if ( isnumeric(struct_in) || islogical(struct_in) ) && numel( struct_in )==1,
        % for simple outputs, do not hash
        str_out = mynum2str(struct_in);
    elseif ischar( struct_in ),
        str_out = struct_in;
    else
        str_out = DataHash(struct_in);
        if length(str_out)>hash_length,
            str_out = str_out(1:hash_length);
        end
        str_out = [strsyntax.hash str_out]; % indicate data hashing
    end
    return;
end

if isstruct(struct_in) && numel(struct_in)>1, % handle struct arrays by duplicating field information
    struct_in=mat2cell(struct_in,ones(1,size(struct_in,1)),ones(1,size(struct_in,2)));
end

switch class(struct_in)
    case 'struct'
        [~,str_out] = structfun2(@(f,x) [f,strsyntax.struct_field,struct2str(x,opts)],struct_in);
        str_out = [strsyntax.struct_left,str_join(str_out,strsyntax.struct_sep),strsyntax.struct_right];
        
    case 'cell'
        str_out = cellfun2(@(x) struct2str(x,opts),struct_in);
        str_out = [strsyntax.cell_left,str_join(str_out,strsyntax.cell_sep),strsyntax.cell_right];
        
    otherwise
        if isa(struct_in, 'function_handle'),
            struct_in=func2str(struct_in);
        end
        if isnumeric(struct_in) || islogical(struct_in) % notice looseness in not converting single numbers into arrays
            if ~ismatrix(struct_in)
                struct_in=rowvec(struct_in);
                warning('n-dim arrays not supported: reshaped to horizontal vector');
            end
            if numel(struct_in)==1
                str_out=mynum2str(struct_in);
            else
                str_out=arrayfun2(@mynum2str,struct_in);
                s=size(str_out);
                if (length(s)==2)&&(s(1)<2) % horizontal or null vector
                    str_out=[strsyntax.array_left,str_join(str_out,strsyntax.array_sep),strsyntax.array_right];
                else %2D matrix
                    J = cell(1,s(1));
                    for i=1:s(1)
                        J{i} = str_join(str_out(i,:),strsyntax.array_sep);
                    end
                    str_out=[strsyntax.array_left,str_join(J,strsyntax.array_linesep),strsyntax.array_right];
                end
            end
        else % struct_in is a string
            assert(ischar(struct_in));
%             assert(~any(ismember(struct_in,syntaxchar)),sprintf('syntax characters "%s" are forbidden in strings: "%s"',syntaxchar,struct_in)); % forbid strsyntax characters
            str_out=struct_in; % otherwise it is treated as a string
        end
end

if hash_append,
    hash = DataHash(struct_in);
    if ~isinf(hash_length) && length(hash)>hash_length,
        hash = hash(1:hash_length);
    end
    str_out = [ str_out strsyntax.hash hash strsyntax.hash ];
end
end

function s = mynum2str(n)
if islogical(n), s = bool2str(n); else s = num2str(n); end
end

function s = bool2str(b)
if b, s='true'; else s='false'; end
end
