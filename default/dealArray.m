function varargout = dealArray(data)

warning('dealArray:slow','turns out, dealArray is very slow, although readable');

n = nargout();
assert(n==numel(data));

varargout = cell(1,n);

for i = 1:n
	varargout{i} = data(i);
end;
