function G = create_argument_grid(varargin)

G = {};
    
if ~isempty(varargin),

    arg1 = varargin{1};
    G1 = create_argument_grid(varargin{2:end});
    if isempty(G1),
        G = arg1;
    else
        for v=1:length(arg1),
            G = [ G cellfun2(@(x) [arg1(v) x],G1) ];
        end
    end
    
end

end
