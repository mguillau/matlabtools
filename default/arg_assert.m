function arg_assert(variable,type,varargin)
% arguments: variable type [default value]
% type = all the arguments valid for isa(x,?)

if isempty(varargin)
	default = false;
else
	assert(strcmp(varargin{1},'default'));
	default = true;
	defaultValue = varargin{2};
end;

ex = evalin('caller',sprintf('exist(''%s'',''var'')',variable)) == 1;
if default && ~ex
	evalin('caller',sprintf('%s = %s;',variable,defaultValue));
else
	assert(ex);
end;

assert(evalin('caller',sprintf('isa(%s,''%s'')',variable,type)));
