function params = readParams(default,changes)

assert(mod(length(changes),2)==0);

params = default;

for i = 1:2:length(changes)
	params.(changes{i}) = changes{i+1};
end;
clear i;
