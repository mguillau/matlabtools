function c = configFromDefaults(d,o)

c = d;
f = fieldnames(o);
for i = 1:length(f)
	c.(f{i}) = o.(f{i});
end;
