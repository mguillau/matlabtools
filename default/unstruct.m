function unstruct(variable)
% Saves the fields of a (named) structure as variables in the caller
% environment. Useful for loading mat files or struct options.

if ischar(variable), % variable is a string that refers to a variable name
    
    assert(evalin('caller',sprintf('isstruct(%s)',variable))); % variable is a struct in caller space
    
    f = evalin('caller',sprintf('fieldnames(%s);',variable)); % read fields of variable

		if any(strcmp(variable,f))
			% if one of the fields in the struct has the same name as the variable
			% assign it last so that the original doesn't get overwritten
			f{strcmp(variable,f)} = [];
			f{end+1} = variable;
		end;

    for i=1:length(f),
        evalin('caller',sprintf('%s = %s.%s;',f{i},variable,f{i})); % save values in variables named with fields
    end
        
elseif isstruct(variable),
    
    f = fieldnames(variable);
    for i=1:length(f),
        assignin('caller',f{i},variable.(f{i})); % save values in variables named with fields
    end
    
else
    
    error('Invalid input for unstruct');
    
end
