function uncell(variable)
% Saves the content of a (named) cell array as variables in the caller
% environment. Useful for loading mat files and reading varargin. Cf
% unstruct.m and read_varargin.m.

if ischar(variable), % variable is a string that refers to a variable name
    
    assert(evalin('caller',sprintf('iscell(%s)',variable))); % variable is a cell array in caller space
    n = evalin('caller',sprintf('length(%s)',variable));
    assert(mod(n,2)==0);
    n = n/2;
    
    for i=1:n,
        varname = evalin('caller',sprintf('%s{%d}',variable,2*i-1));
        evalin('caller',sprintf('%s = %s{%d};',varname,variable,2*i)); % save values in variables named with fields
    end
    
elseif iscell(variable),

    n = length(variable);
    assert(mod(n,2)==0);
    n = n/2;
    for i=1:n,
        if isvarname(variable{2*i-1}),
            assignin('caller',variable{2*i-1},variable{2*i});
        else % in case of valid assignments like struct arrays, try using a temporary variable
            [~,t]=fileparts(tempname);
            assignin('caller',t,variable{2*i});
            evalin('caller',sprintf('%s = %s; clear %s',variable{2*i-1},t,t));
        end
        %fprintf('set %s from command-line\n',variable{2*i-1});
    end

else
   
    error('Invalid input for unstruct');
    
end
