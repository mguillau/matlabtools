function arg_default(variable,value)
% Example: arg_default x 12
% The value is being evaluated in the callers workspace.
% Therefore, a string as a default value needs to be put as
%   arg_default x '''test'''

ex = evalin('caller',sprintf('exist(''%s'',''var'')',variable)) == 1;
if ~ex
	evalin('caller',sprintf('%s = %s;',variable,value));
end;
