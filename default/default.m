function default(variable,value)
% Set a variable (or a structure field) to <value> if not already defined

if isvarname(variable), % the input is a simple variable name
    ex = evalin('caller',sprintf('exist(''%s'',''var'')',variable)) == 1;
else
    fields = str_split(variable,'\.');
    if all(cellfun(@isvarname,fields)), % the input is a valid struct field
        ex = evalin('caller',sprintf('exist(''%s'',''var'')',fields{1})) == 1;
        K = 1;
        while ex && K<length(fields),
            curr = str_join(fields(1:K),'.');
            if ~evalin('caller',sprintf('isstruct(%s)',curr)),
                error('%s is not a struct, can''t set a default field',curr)
            end
            ex = evalin('caller',sprintf('isfield(%s,''%s'')',curr,fields{K+1}));
            K = K + 1;
        end
    else
        error('invalid variable string: %s',variable);
    end
end

if ~ex
	evalin('caller',sprintf('%s = %s;',variable,value));
end;

% register variables with default values
if evalin('caller','exist(''registered__default__variables'',''var'')') == 1;
    evalin('caller',sprintf('registered__default__variables.%s = %d;',variable,~ex));
end

