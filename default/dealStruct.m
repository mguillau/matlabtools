function dealStruct(data)
% deal struct into callers workspace, data is the name, not the value
% use command syntax

fn = evalin('caller',sprintfn('fieldnames(%[data]s)'));

for i = 1:length(fn)
	evalin('caller',sprintf('%s = %s.%s;',fn{i},data,fn{i}));
end;
