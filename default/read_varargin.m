function read_varargin()
% Read the varargin of the caller function as saves variables

% Prepare registration of variables with default value
evalin('caller','registered__default__variables=struct();');

if evalin('caller','length(varargin)==0;');
    return;
end

if evalin('caller','isstruct(varargin{1})'),
    evalin('caller','unstruct(varargin{1});');
    evalin('caller','uncell(varargin(2:end));');
else
    evalin('caller','uncell(varargin);');
end

