function [params,info] = get_params()
% Get values of parameters
assert(evalin('caller','exist(''registered__default__variables'',''var'')==1'));
isdefault = evalin('caller','registered__default__variables');
varnames = fieldnames(isdefault);
params = evalin('caller','varnames2struct(fieldnames(registered__default__variables))');
if nargout>1,
    info = struct();
    for i=1:length(varnames),
        name = varnames{i};
        info.(name).name = name;
        info.(name).value = params.(name);
        info.(name).isdefault = isdefault.(name);
    end
end

