function L = log_gauss_iso(X,M,R)
%em_gauss - compute log-likelihoods for all points and all components
%
%L = log_gauss(X,M,R)
%  X - (d x n) matrix of input data
%  M - (d x k) matrix of components means
%  R - (1 x k) matrix of component isotropic variances. 
%returns 
%  L - (k x n) log-likelihoods of points x_n belonging to component k

% Jakob Verbeek, 2006

[d,n]   = size(X);
k       = size(M,2);

Mah     = sqdist(M,X) ./ repmat( R', 1, n) ; % Mahanalobis distances

L       = Mah + repmat( d * ( log(2*pi) + log(R') ) , 1, n);

L       = -L/2;