function D = KL_multinomial(p,q);
%
% compute KL divergences between distributions in the cols of p and the cols of q
%
%
%

D = -repmat(entropy(p'),1,size(q,2));

D = D - p'*log(q+eps);