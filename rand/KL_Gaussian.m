function KL = KL_Gaussian(m1,C1, m2,C2)
%
% D = KL_Gaussian(m1,C1, m2,C2)
%
% JJV, jan 2007, INRIA
%

if (isvector(C1) && isvector(C2))
    
    C2  = C2.^-1;

    tmp = C1.*C2;

    KL = -sum(log(tmp)) + sum(tmp) + C2'* ((m1-m2).^2);
    
else
    
    C2  = inv(C2);

    tmp = C1*C2;

    KL = -log(det(tmp)) + trace(tmp) + (m1-m2)'*C2*(m1-m2);

end

KL = KL/2;