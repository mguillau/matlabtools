function x = randMultinomialLog(lw)

assert(all(isfinite(lw)));
assert(all(isreal(lw)));
assert(~isempty(lw));
x = randMultinomial(softmax(lw(:)));
