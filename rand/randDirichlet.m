function x = randDirichlet(alpha)

x = gamrnd(alpha,1); % TODO langsamer?
x = x ./ sum(x);
