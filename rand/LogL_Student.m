function log_l = student(X, mu, lambda, alpha )
%
% log_l = student(X, mu, lambda, alpha )
%
% X (D x N)     : data vectors as columns
% 
% log_l (1 x N) : data log-likelihoods
%
% computes the log-likelihood of data points in X under student distribution
% 

[D,N] = size(X);

K  = gammaln( (alpha+D)/2 ) - gammaln( alpha/2 ) - (D/2) * log( alpha*pi ) + log(det(lambda)) /2 ;

Xc = X-repmat(mu,1,N);

Q  = sum( (lambda*Xc).*Xc, 1);

log_l = K - ( (alpha+D)/2 ) * log( 1 + Q/alpha );
