function [X,L] = mixgeN(N,K,d,c,e)
%mixgen - Sample from Gaussian mixture 
%
%[X,T] = mixgen(N,m,K,d,c,e) 
%  N - size of data set 
%  K - Number of components
%  d - dimension
%  c - separation degree
%  e - maximum eccentricity
%returns
%  X - data set (d x N)
%  L - data log-likelihoods
%
% Nikos Vlassis & Jakob Verbeek, 2006.
% for definitions see (Dasgupta, 1999)

% sample mixing weights from uniform dirichlet distribution
% --> Normalizing samples from multi-dimensional uniform distribution biases weights to uniform
W = dirichlet_sample(ones(K,1)); 

% sample mixture components for data poiNts
Z = sample_discrete(W,N,1);

% create c-separated Gaussian clusters of maximum eccentricity e
ok = 0; trials = 0;
while ~ok;
  trials = trials +1;
  M = randn(d,K)*sqrt(K)*sqrt(c)*trials;
  Trace = zeros(K,1);
  for k = 1:K
    U = rand(d); U = (U*U')^(-1/2) * U;
    L = diag( rand(d,1)*(e-1)+1 ).^2;
    covs(:,:,k) = U*L*U';    
    maxL(k) = max(diag(L));
  end

  ok = 1;  % check degree of separation
  for i = 1:K;  for j = i+1:K;
      if norm(M(:,i)-M(:,j)) < c * sqrt( d * max(maxL(i), maxL(j)) );     ok= 0;  end;
  end;  end;
  
end

X = zeros(d,N); % sample data
for k=1:K;
   I = find(Z==k);
   X(:,I) = randnorm( length(I), M(:,k), [], covs(:,:,k) );    
end

L = log_gauss(X,M,covs) + repmat( log(W+eps), 1, N);       % joint log-likelihood
Q = normalize( exp( L - repmat( max(L,[],1), K, 1) ), 1);   % posterior probabilities
L = sum(Q.*( L - log(Q+eps) ),1);                           % minimum free-energy = mixture log-liKelihood
