function I = mut_inf_bin(data);
%
% I = mut_inf_bin(data);
%
% data (C x N) 	: N joint observations of the C binary variables
%
% I (C x C) 	: the matrix with mutual information between the variables 

[C,N] = size(data);

X  = sum(data,2);
XY = data*data';

pX = X/N; pX = [pX 1-pX];

for i=1:C;
for j=1:C;
pXY = zeros(2);
pXY(2,2) = XY(i,j);
pXY(2,1) = X(i) - XY(i,j);
pXY(1,2) = X(j) - XY(i,j);
pXY(1,1) = N - sum(pXY(:));

I(i,j) = entropy(pX(i,:)) + entropy(pX(j,:)) -entropy(pXY(:)');

end;
end;