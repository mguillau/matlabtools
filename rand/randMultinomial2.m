function x = randMultinomial2(w,n)
% randMultinomial2 draws from a multinomial distribution (need not be normalized)
% Returns a random vector of size (n,1).
% n defaults to 1.
%
% much faster than randMultinomial but uses more memory (explicit n*k logical matrix)

default n 1

assert(all(isfinite(w)),'non-finite elements');

cdf = cumsum(w(:)');
assert(cdf(end)>0);
rnd  = cdf(end)*rand(n,1);
% TODO: cdf is sorted so we could use binary search to speed things up. check STL lower_bound
x = sum(bsxfun(@lt,cdf,rnd),2)+1;
