function x = randMultinomial(w,n)
% randMultinomial draws from a multinomial distribution (need not be normalized)
% Returns a random vector of size (n,1).
% n defaults to 1.
% n can be 0, but not negative.

default n 1

assert(all(isfinite(w)),'non-finite elements');

cdf = cumsum(w);
assert(cdf(end)>0);

x = zeros(n,1);
for i = 1:n
	x(i) = sum(cdf < rand()*cdf(end)) + 1;
end;
