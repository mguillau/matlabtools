function H = entropy_dirichlet(P);

k  = size(P,1);

PP = sum(P,1);

H =   - gammaln( PP ) + sum( gammaln( P ), 1 );

H = H - sum( ( P - 1 ) .*  digamma( P ) , 1)  + (PP-k).*digamma( PP );
