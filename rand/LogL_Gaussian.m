function L = log_gauss(X,M,R)
%em_gauss - compute log-likelihoods for all points and all components
%
%L = log_gauss(X,M,R)
%  X - (d x n) matrix of input data
%  M - (d x k) matrix of components means
%  R - (d x d x k) matrix of component covariances. 
%  R - (d x k)     matrix of component variances per dimension, giving the diagonal covariance matrices
%returns 
%  L - (k x n) log-likelihoods of points x_n belonging to component k

% Jakob Verbeek, 2007

[d,n]   = size(X);
k       = size(M,2);
diag    = size(R,3)==1;

L       = zeros(k,n) + (-d/2)*log(2*pi);

for j = 1:k 

  Xc  = X - repmat(M(:,j),1,n);

  if diag;   Mah = Xc ./ repmat( R(:,j), 1, n);
  else;      Mah = inv(R(:,:,j)) * Xc;              end      
  L(j,:) = L(j,:) - sum(Xc.*Mah,1) / 2;

  if diag;  L(j,:) =  L(j,:) - sum(log(R(:,j)))   / 2; 
  else;     L(j,:) =  L(j,:) - log(det(R(:,:,j))) / 2; end
      
end
