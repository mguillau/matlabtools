function x = randUnitSphere(n,d)
% RANDUNITSPHERE Uniformly sample points on the L2 unit sphere
%
% x = randUnitSphere(n,d) uniformly samples n points on the L2 d-sphere
% x is a n-x-d matrix.

x = randn(n,d);
norm = sqrt(sum(x.^2,2));
x = bsxfun(@rdivide,x,norm);

