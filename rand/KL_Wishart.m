function KL = KL_Wishart(nu1,S1, nu2,S2)

D = size(S1,1);

log_Z1 = (nu1*D/2) * log(2) +  (D*(D-1)/4) * log(pi)  + (nu1/2) * log(det(S1)) + sum( gammaln( (nu1+1-[1:D])/2));
log_Z2 = (nu2*D/2) * log(2) +  (D*(D-1)/4) * log(pi)  + (nu2/2) * log(det(S2)) + sum( gammaln( (nu2+1-[1:D])/2));

E_logDetW = D*log(2) + log(det(S1)) + sum( psi( (nu1+1-[1:D])/2));

KL = log_Z2 - log_Z1 + (1/2)*(nu1-nu2)*E_logDetW + (1/2)*nu1*trace(inv(S2)*S1-eye(D));

