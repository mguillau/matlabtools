function [ ROCauc, ROCeer, ROCeerthr, ROCpts ] = ROC(score, labels, draw, c )
% ROC Receiver operating characteristic
%
% [auc, eer, pts , i] = ROC(score, labels,c,draw);
%
% pts=curve points, i=threshold at eer

if nargin==2; draw=0; end
if nargin==3; c='b-'; end

[so,si]=sort(-score);
tp=cumsum(labels(si)>0)'/sum(labels>0);
fp=cumsum(labels(si)<1)'/sum(labels<1);
%rc=cumsum(labels(si)>0)';
%rc=rc./(1:length(rc))';

[uo,ui]=unique(so);
tp=[0;tp(ui);1];
fp=[0;fp(ui);1];
%rc=[0;rc(ui);1];

% compute lower envelope and area under curve
di=[true ; tp(2:end-1)~=tp(1:end-2) ; true];
x=fp(di);
%x2=rc(di);
y=tp(di);

ROCauc=(x(2:end)-x(1:end-1))'*y(1:end-1);
%PRauc =(x2(2:end)-x2(1:end-1))'*y(1:end-1);

% plot lower envelope
xp  = [0 ; reshape([x x]',[],1) ; 1 ; 1];
%x2p = [0 ; reshape([x2 x2]',[],1) ; 1 ; 1];
yp  = [0 ; 0 ; reshape([y y]',[],1) ; 1];

% compute the Equal Error Rate
%i = min(find( fp > (1-tp) ));if fp(i)==fp(i-1);  eer = 1-fp(i);else;               eer =   tp(i);   end
i = find( xp > (1-yp) , 1 );
if xp(i)==xp(i-1);  ROCeer = 1-xp(i);else ROCeer =   yp(i);   end
ROCeerthr = -uo(ceil((i-1)/2)-1);

%i = find( x2p > yp , 1 );
%if xp(i)==xp(i-1);  PReer = 1-xp(i);else PReer =   yp(i);   end
%PReerthr = -uo(ceil((i-1)/2)-1);

if draw
    if draw>1,
        semilogx(xp,yp,c,[0 1], [1 0],'r--');
    else
        plot(xp,yp,c,[0 1], [1 0],'r--');
    end
    grid;
    axis([0 1 0 1]);
    xlabel 'false positive rate'
    ylabel 'true positive rate'
    title(sprintf('ROC:  AUC = %.1f, EER = %.1f',ROCauc*100,ROCeer*100));
    drawnow;
end

ROCpts = [ xp yp ];
%PRpts  = [ x2p yp ];
