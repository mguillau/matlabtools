function C=confusion_matrix_binary(x,y)
% CONFUSION_MATRIX_BINARY Confusion matrix of a binary prediction problem, or compare binary vectors
%
% C = CONFUSION_MATRIX_BINARY(X,Y) outputs a 2x2 matrix C with the following statistics:
%  C(1,1) contains the number of trues in X&Y
%  C(1,2) contains the number of trues in X&~Y
%  C(2,2) contains the number of trues in ~X&~Y
%  C(2,1) contains the number of trues in ~X&Y

x=logical(x(:));
y=logical(y(:));

c(1,1) = nnz(x & y);
c(1,2) = nnz(x & ~y);
c(2,1) = nnz(~x & y);
c(2,2) = nnz(~x & ~y);
