function ap = AP(score,anno)
% AP Average Precision
%
% compute average precision for each column in score and anno
% 

[N,C] = size(score);

for c = 1 : C
	[tmp ii] = sort(score(:,c),'descend'); % sort the scores in descending order
	anno(:,c) = anno(ii,c);                % re-arrange the annotation bits correspondingly
end
	
prec = cumsum(anno,1) ./ repmat( (1:N)', 1,C); % compute precision at each position for each concept

ap   = sum( prec .* anno ,1) ./ sum(anno,1);   % average precisions over relevant entries

ap( isnan(ap) ) = 0; % set AP to zero if there are no relevant entries
