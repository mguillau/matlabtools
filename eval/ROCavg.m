function [auc, eer, pts] = ROCavg(score, labels, draw, c)
% ROCavg
% 
% [auc, eer] = ROC(score, labels,c,draw);
%
%

if nargin==2; draw=0; end
if nargin==3; c='b-'; end

[so,si]=sort(-score);
tp=cumsum(labels(si)>0)'/sum(labels>0);
fp=cumsum(labels(si)<1)'/sum(labels<1);
[uo,ui]=unique(so);
tp=[0;tp(ui);1];
fp=[0;fp(ui);1];

% compute lower envelope and area under curve
di=[true ; tp(2:end-1)~=tp(1:end-2) ; true];
x=fp(di);
y=tp(di);
auc=(x(2:end)-x(1:end-1))'*y(1:end-1);

% plot lower envelope
xp=[0 ; reshape([x x]',[],1) ; 1 ; 1];
yp=[0 ; 0 ; reshape([y y]',[],1) ; 1];

% compute the Equal Error Rate
%i = min(find( fp > (1-tp) ));if fp(i)==fp(i-1);  eer = 1-fp(i);else;               eer =   tp(i);   end
i = find( xp > (1-yp) , 1 );
if xp(i)==xp(i-1);  eer = 1-xp(i);else               eer =   yp(i);   end

if draw
    plot(xp,yp,c,[0 1], [1 0],'r--');
    grid;
    axis([0 1 0 1]);
    xlabel 'false positive rate'
    ylabel 'true positive rate'
    title(sprintf('AUC = %.2f, EER = %.2f',auc*100,eer*100));
    drawnow;
end

if nargout>2, 
   pts = [ xp yp ];
end
