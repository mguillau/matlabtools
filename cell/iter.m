function A = iter(fun,varargin)
% Accumulate the result of a binary function
%
% Ex: iter(@plus,v{:});          % computes the sum a cell array of numbers
%     iter(@bitor,binMasks{:});  % compute the union of several binary masks

if nargin==3,
    A = feval(fun,varargin{:});
else
    L = varargin{end};
    varargin(end)=[];
    B = iter(fun,varargin{:});
    A = feval(fun,B,L);
end
