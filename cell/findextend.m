function Y = findextend(X,findfun,repfuns)
% FINDEXTEND Find and replace elements of a cell array
%
% X is an array or cell-array
% findfun return  a boolean for an element of X: true where repfun should be used
% repfun  is a list of functions for elements to replace matched elements
Y=X;
ic = iscell(X);

idx = find(applyfun(findfun,X));
for i=1:length(idx),
    if ic,
        x = X{idx(i)};
        replacements = applyfun(@(f) f(x),repfuns);
        Y(end+1:end+length(replacements)) = {replacements{:}};
    else
        x = X(idx(i));
        replacements = applyfun(@(f) f(x),repfuns);
        Y(end+1:end+length(replacements)) = [replacements{:}];
    end
end
Y(idx)=[];

