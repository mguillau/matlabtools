function X = cellfun2(fun,varargin)
% Applies the function fun to all elements of cells, and returns a cell
% array of the results
X = cellfun(fun,varargin{:},'UniformOutput',false);
