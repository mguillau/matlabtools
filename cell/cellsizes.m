function varargout = cellsizes(varargin)
% CELLSIZES Read the sizes of matrices inside a cell array
%

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile;
