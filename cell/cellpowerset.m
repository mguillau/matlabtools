function X = cellpowerset(A,nonempty)
% CELLPOWERSET Computes the powerset of a cell array
%
% X = cellpowerset(A,nonempty)
%  Outputs the powerset of cell A. 'nonempty' specifies if the empty set should be included or not.

default nonempty false

X = {};

if isempty(A),
    if ~nonempty,
        X = {{}};
    end
    return
end

a = A{1};
A(1) = [];
Xa = cellpowerset(A,false);
X = [ X Xa cellfun2(@(x) [ a x ],Xa) ];

if nonempty,
    X(1) = [];
end
