function varargout = cellsizes(varargin)
% CELLSIZES Efficiently check if elements of a cell array are empty
%

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile;
