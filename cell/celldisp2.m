function celldisp2(c)
% Compactly displays the content of a cell array

for i=1:length(c),
    disp(c{i});
end
