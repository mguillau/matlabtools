function X = applyfun(fun,varargin)
% Apply a function to each element of an array or cell array.
% If the outputs are consistent, then there are in an array, otherwise a
% cell array.

if iscell(varargin{1}),
    try
        X = cellfun(fun,varargin{:});
    catch e
        if strcmp(e.identifier, 'MATLAB:arrayfun:NotAScalarOutput' ),
            X = cellfun2(fun,varargin{:});
        else
            rethrow(e);
        end
    end
else
    try
        X = arrayfun(fun,varargin{:});
    catch e
        if strcmp(e.identifier, 'MATLAB:arrayfun:NotAScalarOutput' ),
            X = arrayfun2(fun,varargin{:});
        else
            rethrow(e);
        end
    end
end
