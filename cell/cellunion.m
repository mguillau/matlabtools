function x = cellunion(c)
% Takes the union of all cell elements

x={};
for i=1:length(c),
    x=union(x,c{i});
end
