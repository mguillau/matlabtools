function bool = deep_eq(X,Y)
% Check the "deep" equality of X and Y. Useful for cell arrays and structs (The ordering of fields of structs is ignored).
% Works recursively, so it can be slow on some inputs.

if isempty(X) && isempty(Y),
    bool = true;
    return
end
if ischar(X) && ischar(Y),
    bool = strcmp(X,Y);
    return
end
try
    if isnumeric(X) && isnumeric(Y),
        X(isnan(X))=0;
        Y(isnan(Y))=0;
    end
    bool = all(reshape(eq(X,Y),[],1));
catch e
    if strcmp(e.identifier,'MATLAB:UndefinedFunction'),
        if iscell(X) && iscell(Y),
            bool = all(reshape(cellfun(@(x,y) deep_eq(x,y),X,Y),[],1));
        elseif isstruct(X) && isstruct(Y),
            afld = fieldnames(X);
            blfd = fieldnames(Y);
            if ~isempty(setxor(afld,bfld),
                bool = false;
                return;
            else
                bool = true;
                for o=1:length(afld),
                    for k=1:length(X),
                        bool = bool && deep_eq(X(k).(afld{o}),Y(k).(afld{o}));
                        if ~bool,
                           return;
                        end
                    end
                end
            end
        else
            bool=false;
        end 
    else
        rethrow(e);
    end
end

end
