
#include <mex.h>
#include <matrix.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* Check number and type of inputs/outputs */
    if (nrhs < 1 || nrhs > 1)
        mexErrMsgTxt("Only one argument.");
    if (nlhs > 1)
        mexErrMsgTxt("Only one output.");
    if (!mxIsCell(prhs[0]))
        mexErrMsgTxt("The input must be a cell array.");

        /* Read input cell and create output matrix */
    int n_cell = mxGetNumberOfElements(prhs[0]);
    int i;
    plhs[0] = mxCreateLogicalMatrix(n_cell,1);
    mxLogical * isempty = mxGetLogicals(plhs[0]);

    for (i = 0; i < n_cell; i++) {
        /* Look at cell array size and fill output */
        const mxArray *cellcontent = mxGetCell(prhs[0], i);
        if (!cellcontent || mxIsEmpty(cellcontent)) {
            isempty[i] = 1;
        }
    }
    return;
}

