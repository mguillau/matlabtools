function r = file_hasName(file,name)

r = strcmp(file_getName(file),name);
