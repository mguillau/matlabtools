function name = system_hostname()
% (short) hostname

[status,result] = system('hostname');
assert(status==0,result);

name = result(1:end-1); % remove the carriage return at the end

% TODO in matlab, if you type while it's processing, the keystrokes get dumped into the next call of system
% that's why there are stray enters, and other things :/ any fix?
if name(1)==sprintf('\n') % sometimes, the output contains a leading carriage return
	name = name(2:end);
end;