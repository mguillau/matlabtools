function lock = file_slock(filename,varargin)
% Locks the file filename, if possible.
% Returns a struct on succes.
% Returns [] on failure.
% The struct is built so that on garbage collection it will release the lock.
% Note: Functions are built so that unlock shouldn't be called twice,
%   (This might be a source for bugs, if lock ids are reused.)
% There is no file_sunlock, just clear lock;
% Also, creates the file, if it doesn't exist. But not parent folders.
% Varargin can contain sprintf arguments.

if ~isempty(varargin), filename = sprintf(filename,varargin{:}); end;

if ~file_exist(filename)
	file_touch(filename);
end;

lock.id = file_lock(filename);
if lock.id==-1
	lock = [];
else
	lock.clean = onCleanup(@() file_unlock(lock.id));
% 	lock.clean = onCleanup(@() fprintf('cleaning %d\n',lock.id));
end;
