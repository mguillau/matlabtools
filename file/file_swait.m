function lock = file_swait(file,message)
% Wait for file to be locked.
% Using save lock methods with onCleanUp-lock-removal.
% In a local (non-condor) session, waits forever, but outputs information after a certain time limit.
% In a condor session, will wait longer and then put on hold an retry after some time.
% File can be a cell array of sprintf arguments.
% Message can be a cell array of sprintf arguments.
% Message will be used if lock can't be acquired after some time.
% Use 'clear lock' to lose the lock.
% (Also see file_slock)
% This function will only return with a lock, or loop forever!

% settings
message_t = 0; % 20; % time to wait silently, warning message after that
condor_t = 90; % time to wait before putting job on hold
dt = 1; % retry every second

% parameters
if iscell(file)
	file = sprintf(file{:});
end;
if ~var_exists('message') || isempty(message)
	message = sprintf('waiting for lock on %s',file);
elseif iscell(message)
	message = sprintf(message{:});
end;

% message = sprintf('HACK locking %s',file); % TODO hack

% wait

if isempty(getenv('CONDOR_JOBFOLDER')) % TODO more uniform access to this information
	max_t = inf;
else
	max_t = condor_t;
end;

lock = [];

while isempty(lock)

	tStart = now();
	message_shown = false;
	
	lock = file_slock(file);
	while isempty(lock) && (now()-tStart)<(max_t/60/60/24)
		if (now()-tStart)>(message_t/60/60/24)
			if message_shown
% 				pg_update();
				pg.update();
			else
				message_shown = true;
				pg = pg_start(message);
% 				pg_begin(message,'');
			end;
		end;
		pause(dt+rand());
		lock = file_slock(file);
	end;

	if message_shown
% 		pg_end();
		pg.done();
	end;
	
	if isempty(lock)
		if isempty(getenv('CONDOR_JOBFOLDER')) % TODO more uniform access to this information
			assert(false);
		else
			pg_message('Could not get a lock on %s, condor hold',file);
			condor_hold();
		end;
	end;
	
end;
