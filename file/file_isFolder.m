function e = file_isFolder(file)
% Checks if file exists and is a folder (instead of a file)
% Works for cell arrays of files.

if iscell(file)
	e = cellfun(@file_isfolder,file);
else
	e = exist(file,'file')==7;
end;
