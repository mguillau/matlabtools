function r = file_hasExt(file,ext)
% ext
%	extension without the dot

[~,~,oext] = fileparts(file);
r = strcmp(oext,['.' ext]);
