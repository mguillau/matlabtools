function file_rename(from,to)
% each argument can be a cell of sprintf arguments
% to is only a name, not a whole path

if iscell(from), from = sprintf(from{:}); end;
if iscell(to), to = sprintf(to{:}); end;

[status result] = system(sprintf('mv %s %s%s%s',from,file_getFolder(from),filesep,to));
assert(status==0,'file_rename error: %s',result);
