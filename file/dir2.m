function files = dir2(directory_name,pattern,fun)
% directory_name
%	directory with files to search for
% pattern
%	regexp-like expression to be applied to filenames in directory
% fun (optional)
%   apply fun to filenames
%
% examples:
%	^iteration_\d+.mat$

files = dir(directory_name);
%todo: try to do like gnu find.
%files = ls('-1',directory_name); % expands *, puts one file per line.
%files = arrayfilter(@notempty,str_split(files,sprintf('\n'))); % split lines
%dir_names = cellfun(@(x) x(end)==':',files); % find those files that are directories
if exist('pattern','var'),
    files = arrayfilter(@(X) ~isempty(regexp(X.name,pattern,'once')), files );
end

files = arrayfun2(@(X) X.name,files);

if exist('fun','var'),
    files = cellfun2(fun,files);
end

