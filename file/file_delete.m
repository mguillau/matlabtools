function file_delete(file)
% Force delete of file/directory.
% Works for directories.
% File can be a cell array of sprintf arguments.

if iscell(file)
	file = sprintf(file{:});
end;

% matlabs rmdir and delete act strangely when it's used on symlinks
% if file_isFolder(file)
% 	rmdir(file,'s');
% else
% 	delete(file);
% end;

% therefore i'm using linux system commands
system(sprintf('rm -rf %s',file));
