function file_unlock(varargin)
% lock = file_unlock(file);
%
% Lock a file using fcntl. If the lock value is -1, then the locking has failed.
% Otherwise it has succeeded, and file_unlock should be used to release the lock.

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile;
