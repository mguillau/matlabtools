/*******************************************************************************************************
 * File:    FILE_REALPATH.C
 *
 * Purpose: This is a Matlab mex file which will get the real path of a file.
 ******************************************************************************************************/

#include <limits.h> /* PATH_MAX */
#include <stdio.h>
#include <stdlib.h>
#include "mex.h"

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])

{
char *realname;
char *filename;

/* Check for proper number of arguments NOW! */
if (nrhs != 1)
   {
   mexErrMsgTxt("One input argument (filename) required.");
   }

if (nlhs > 1)
   {
   mexErrMsgTxt("Only one output argument is allowed.");
   }

if (!mxIsChar(prhs[0]))
   {
   mexErrMsgTxt("Input argument must be a character string.");
   }

if (mxGetM(prhs[0]) != 1)
   {
   mexErrMsgTxt("Input must be a row vector.");
   }

/* Allocate space for the return value */
realname = mxCalloc(PATH_MAX+1,sizeof(char));
/* Get the filename parameter. Note: filename is a valid string because mxCalloc initializes to 0 */
filename = mxCalloc(mxGetN(prhs[0])+1,sizeof(char));
mxGetString(prhs[0],filename,mxGetN(prhs[0])+1);
/* Fill realname with... real name */
char *res = realpath(filename, realname);

if (res) {
    /* Copy to output on success */
    plhs[0] = mxCreateString(realname);
} else {
    mexErrMsgTxt("Error running realpath");
}

}
