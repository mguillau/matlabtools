function str2 = file_protectName(str)
% FILE_PROTECTNAME Ensure that a filename has no file separator

str2 = regexprep(str,filesep,'-sep-');

