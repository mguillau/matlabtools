function name = file_getName(file)

if iscell(file)
	name = cellfun(@file_getName,file,'UniformOutput',false);
else
	[~,name] = fileparts(file);
end

