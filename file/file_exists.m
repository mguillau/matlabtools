function e = file_exists(file,varargin)
% file_exist checks for the existance of a file or folder.
% Works for cell arrays of files.
% Accepts sprintf arguments

if iscell(file)
	e = cellfun(@file_exist,file);
else
	if ~isempty(varargin)
		file = sprintf(file,varargin{:});
	end;
	if exist(file,'file')
		e = true;
	else
		e = false;
	end;
end;
