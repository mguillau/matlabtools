function varargout = file_lock(varargin)
% lock = file_lock(file);
%
% Lock a file using fcntl. If the lock value is -1, then the locking has failed.
% Otherwise it has succeeded, and file_unlock should be used to release the lock.

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile;
