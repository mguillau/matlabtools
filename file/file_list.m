function files = file_list(folder,pattern)
% folder
%	folder with files to search for
% pattern
%	regexp(.) like expression to be applied to filenames in folder
%	(see matlab help of regexp(.))
%	it returns all filenames that match at least once to this regexp
%	don't forget ^ for start and $ for end of filename
%
% examples:
%	^iteration_\d+.mat$

files = dir(folder);
e = regexp({files.name},pattern,'start','once');
e = cellfun(@(i) ~isempty(i),e);
files = {files(e).name}';
