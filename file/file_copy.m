function file_copy(from,to)
% copy file or directories
% using force, will work in most cases
% each argument can be a cell of sprintf arguments

if iscell(from), from = sprintf(from{:}); end;
if iscell(to), to = sprintf(to{:}); end;

[status result] = system(sprintf('cp -rf %s %s',from,to));
assert(status==0,'file_copy error: %s',result);
