function e = file_isFile(file)
% Checks if file exists and is a file (instead of a folder)
% Works for cell arrays of files.

if iscell(file)
	e = cellfun(@file_isFile,file);
else
	e = exist(file,'file')==2;
end;
