function file_slink(from,to)
% creates a symbolic link named from, pointing to to
% from,to can each be cell arrays of sprintf arguments
% symoblic and force, will overwrite from file

if iscell(from), from = sprintf(from{:}); end;
if iscell(to), to = sprintf(to{:}); end;

[status,result] = system(sprintf('ln -sf %s %s',to,from));
assert(status==0,result);
