function [lock waited] = file_wait(file,t,dt)
% t, dt and waited are in seconds

if ~exist('t','var'), t = 20; end;
if ~exist('dt','var'), dt = 1; end;

tStart = now();

lock = file_lock(file);
while lock==-1 && (now()-tStart)<(t/60/60/24)
	pause(dt+rand());
	lock = file_lock(file);
end;

waited = (now()-tStart)*24*60*60;
