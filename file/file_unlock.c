/*******************************************************************************************************
 * File:    PP_CLOSE_LOCK.c
 *
 * Purpose: This is a Matlab mex file which will close the file (and remove any locks) pointed to by the
 *          file descriptor passed as a parameter.
 *
 * Author:  Michael DeVore
 * Date:    November 19, 1999
 ******************************************************************************************************/

/*
% This file and the DistributePP software package were created and
% are maintained by Michael D. DeVore. The package originated in
% November, 1999. Permission is granted by the author for anyone
% to use or modify this software provided that:
% (1) any modified files are documented internally to clearly indicate
%     that they have been modified from the original release; and
% (2) it is recognized that neigher Michael D. DeVore nor Washington
%     University assumes any liability for the use or misuse of the software.
*/

#include "stdio.h"
#include "stdlib.h"
#include "fcntl.h"
#include "unistd.h"
#include "mex.h"
#include "matrix.h"

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])

{
double *fd_double;

/* Check for proper number of arguments */
if (nrhs != 1)
   {
   mexErrMsgTxt("One input argument (file descriptor) required.");
   }

if (nlhs > 0)
   {
   mexErrMsgTxt("No output argument is returned.");
   }

if (!mxIsDouble(prhs[0]))
   {
   mexErrMsgTxt("Input argument must be a (double) file descriptor.");
   }

if ((mxGetM(prhs[0])*mxGetN(prhs[0])) != 1)
   {
   mexErrMsgTxt("Input must be a scalar.");
   }

fd_double = mxGetPr(prhs[0]);
close((int)*fd_double);
}
