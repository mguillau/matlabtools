function success = sha1sum_check(file,hash1)

success = strcmp(hash1,sha1sum(file));