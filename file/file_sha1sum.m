function hash = sha1sum(file)

[success,hash]=system(['sha1sum ' file]);
if success==0,
    hash=regexprep(hash,'^([0-9a-f]*).*$','$1');
else
    if success==127,
        error('sha1sum error: system call failed');
    else
        error('sha1sum error: inexistant file?');
    end
end
