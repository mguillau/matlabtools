function ext = file_getExt(file)

if iscell(file)
	name = cellfun(@file_getExt,file,'UniformOutput',false);
else
	[~,~,ext] = fileparts(file);
end

