/*******************************************************************************************************
 * File:    PP_OPEN_LOCK.c
 *
 * Purpose: This is a Matlab mex file which will open and lock the file whose path is given as a parameter.
 *          The open mode is read-write and a file descriptor is returned.  This file-descriptor should be
 *          passed to PP_CLOSE_LOCK to close the file.  If the file could not be opened or locked, a value
 *          of -1 is returned and a call to PP_CLOSE_LOCK is not needed.
 *
 * Author:  Michael DeVore
 * Date:    November 19, 1999
 ******************************************************************************************************/

/*
% This file and the DistributePP software package were created and
% are maintained by Michael D. DeVore. The package originated in
% November, 1999. Permission is granted by the author for anyone
% to use or modify this software provided that:
% (1) any modified files are documented internally to clearly indicate
%     that they have been modified from the original release; and
% (2) it is recognized that neigher Michael D. DeVore nor Washington
%     University assumes any liability for the use or misuse of the software.
*/

#include "stdio.h"
#include "stdlib.h"
#include "fcntl.h"
#include "unistd.h"
#include "mex.h"
#include "matrix.h"

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])

{
int fd;
double *fd_double;
char *filename;

/* Check for proper number of arguments NOW! */
if (nrhs != 1)
   {
   mexErrMsgTxt("One input argument (filename) required.");
   }

if (nlhs > 1)
   {
   mexErrMsgTxt("Only one output argument is allowed.");
   }

if (!mxIsChar(prhs[0]))
   {
   mexErrMsgTxt("Input argument must be a character string.");
   }

if (mxGetM(prhs[0]) != 1)
   {
   mexErrMsgTxt("Input must be a row vector.");
   }

/* Allocate space for the return value */

plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
fd_double = mxGetPr(plhs[0]);
*fd_double = 0.0;

/* Get the filename parameter. Note: filename is a valid string because mxCalloc initializes to 0 */

filename = mxCalloc(mxGetN(prhs[0])+1,sizeof(char));
mxGetString(prhs[0],filename,mxGetN(prhs[0])+1);

if ((fd = open(filename,O_RDWR)) >= 0)
   {
   lseek(fd,0L,0L);
   if (lockf(fd,F_TLOCK,0L) < 0)
      {
      close(fd);
      fd = -1;
      }
   }

*fd_double = (double)fd;
}
