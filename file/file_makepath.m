function fullpath = file_makepath(varargin)

parts = varargin;
fullpath = str_join(parts,filesep);

