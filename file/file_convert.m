function file = file_convert(file,folder,name,ext)
% Converts a file name, replacing folder, name, and/or ext.
% Works for cell file.
% Works for cell folder, name, and/or ext (same size as cell file).
% 
% file
%	original file name
% folder
%	new folder, or []
%	can be a sprintf formatted string, first and only argument ist the orginal folder
% name
%	new name, or []
%	can be a sprintf formatted string, first and only argument ist the orginal name
% ext
%	new ext, or []
%	can be a sprintf formatted string, first and only argument ist the orginal ext
%	extensions are handled _without_ the preceding dot.

if iscell(file)
	
	if ~iscell(folder), folder = repmat({folder},size(file)); end;
	if ~iscell(name), name = repmat({name},size(file)); end;
	if ~iscell(ext), ext = repmat({ext},size(file)); end;
	
	file = cellfun(@file_convert,file,folder,name,ext,'UniformOutput',false);
	
else

	[ofolder oname oext] = fileparts(file);
	if ~isempty(oext) && oext(1)=='.', oext = oext(2:end); end;

	if ischar(folder)
		folder = sprintf(folder,ofolder);
	else
		assert(isempty(folder));
		folder = ofolder;
	end

	if ischar(name)
		name = sprintf(name,oname);
	else
		assert(isempty(name));
		name = oname;
	end;

	if ischar(ext)
		ext = sprintf(ext,oext);
	else
		assert(isempty(ext));
		ext = oext;
	end;
	
	if ~isempty(ext), ext = [ '.' ext ]; end;

	if isempty(folder)
		file = [ name ext ];
	else
		file = [ folder filesep name ext ];
	end;

end;
