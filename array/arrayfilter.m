function Y = arrayfilter(fun,X,method)
% ARRAYFILTER Keep only the elements of a cell or array for which a function returns true.
%
% X = ARRAYFILTER(fun,X,method) filters the cell or array X with logical function fun
%  if method != 'singleton', then fun is considered to work in a vectorial fashion, i.e., Y=fun(X) is such that Y(i)=fun(X(i)).

defaultstr method singleton

if strcmp(method,'singleton'),
    ii = applyfun(fun,X);
else % vectorial function
    ii = feval(fun,X);
end
Y  = X(logical(ii));

