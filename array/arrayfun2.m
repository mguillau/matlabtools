function X = arrayfun2(fun,varargin)
% ARRAYFUN2 Apply a function to each element of an array and always return a cell
%
% X = arrayfun2(fun,varargin) uses arrayfun with non-uniform output enforced.
%
% See also ARRAYFUN

X = arrayfun(fun,varargin{:},'UniformOutput',false);
