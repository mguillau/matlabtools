
% get the root of the toolbox.
matlabtools_root = fileparts(mfilename('fullpath'));
matlabtools_oldroot = getenv('MATLABTOOLS');

if ~strcmp(matlabtools_root,matlabtools_oldroot), 
    setenv('MATLABTOOLS',matlabtools_root);
    
    % add sub-toolboxes to path.
    matlabtools_subdirs = { 'array', 'cache', 'cell', 'datahash', 'default', 'dist', 'eval', 'file', 'image', ...
                            'optim', 'par', 'plot', 'pg', 'rand', 'save', 'string', 'struct', 'util', 'vec' };
    for matlabtools_d=1:length(matlabtools_subdirs),
        addpath([ matlabtools_root filesep matlabtools_subdirs{matlabtools_d} ]);
    end
    
    warning off MATLAB:dispatcher:nameConflict;

    % TODO: ask user to confirm and download 3rd party code.
    % lightspeed, matlab2tikz, DataHash
    % git submodule add git@github.com:nschloe/matlab2tikz.git 3rdparty/matlab2tikz
    matlabtools_subdirs = { 'lightspeed', ['matlab2tikz' filesep 'src'], 'nthelement', 'typecast', 'datahash', 'rgb2lab', 'renderTextFT' };
    for matlabtools_d=1:length(matlabtools_subdirs),
        addpath(file_makepath(matlabtools_root,'3rdparty',matlabtools_subdirs{matlabtools_d}));
    end

    warning on MATLAB:dispatcher:nameConflict;
end

matlabtools_configfile = file_makepath(userdir(),'.matlabtools_config');
matlabtools_subconfigs = { 'par', 'm2t', 'cache', 'pg' };

if exist(matlabtools_configfile,'file'),
    matlabtools_c = json2mat(matlabtools_configfile);
    if all(ismember(matlabtools_subconfigs,fieldnames(matlabtools_c))), % check that all subconfigs are here
        matlabtools_c = structfun2(@(x,y) feval(sprintf('%s_config',x),y),matlabtools_c); % set subconfigs defaults
    else
        warning('Old matlabtools config format. Reinstalling.'); % TODO: lock matlabtools_configfile
        matlabtools_configfile_backup = ['.matlabtools_config.' datestr(now,30) '.bak'];
        if ~isfield(matlabtools_c,'par'), % hack: par used to be the only subconfig
            matlabtools_c_tmp.par = matlabtools_c;
            matlabtools_c = matlabtools_c_tmp;
        end
        matlabtools_c = structfun2(@(x,y) feval(sprintf('%s_config',x),y),matlabtools_c); % set subconfigs defaults
        file_rename(matlabtools_configfile,matlabtools_configfile_backup);
        pg_message('Backup of configuration file done in: %s',matlabtools_configfile_backup);
        clear matlabtools_*;
        run(mfilename('fullpath'));
    end
else
    matlabtools_c = structfun2(@(x,y) feval(sprintf('%s_config',x)),emptystruct(matlabtools_subconfigs{:})); % get default subconfigs
    try % TODO: lock matlabtools_configfile
        matlabtools_fid = fopen(matlabtools_configfile,'wt');
        assert(matlabtools_fid>0);
        fprintf(matlabtools_fid,'%s',mat2json(matlabtools_c,'prettyprinting',1,'indent',2));
        fclose(matlabtools_fid);
        pg_message('Installation successful! Config file is: %s',matlabtools_configfile);
    catch e
        fclose(matlabtools_fid);
        pg_message('Installation failed!');
    end
end

clear matlabtools_*;

