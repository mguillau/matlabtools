function folder = par_state_clusterfolder(new)

persistent state;

if exist('new','var')
	state = new;
end;

folder = state;
