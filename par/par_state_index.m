function index = par_state_index(new)

persistent state;

if exist('new','var')
	state = new;
end;

index = state;
