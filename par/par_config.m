function current = par_config(new)

persistent c;
persistent d;

if isempty(d), % initialize with default settings
	d.folder = file_makepath(userdir,'par'); % folder containing jobs (should be accessible, readable and writable by the nodes of the cluster!)
%	d.machinefolder = '/scratch_net/%s/dkuettel/par/%s'; % location for local folders (submitting machine)
	d.name = ''; % name to be used for folder and job names for easy identification, empty = random name including date
	d.engine = 'sge'; % local | condor | sge | ssh
	d.workers = 50; % number of workers
	d.time = 60*60; % seconds a worker is allowed to run (can also be inf)
	d.memory = 5e9; % number of ram bytes per worker instance (can also be inf)
	d.maxtime = 1*24*60*60; % (approximate) maximum wall time for a job to take, in seconds (not computation time)
	d.bundle = 1; % how many tasks to bundle into a single task (to minimize the overhead of many small tasks)
        [~,d.matlab] = system('which matlab');
 	d.matlab = strtrim(d.matlab); % path to matlab
	d.restart = false; % restart a worker after each job is done (makes sense for longer jobs)
	d.distributeFilesToBenders = false;
	d.distributeTimeout = inf; % timeout in seconds, use inf for no timeout, use a generous timeout if you do, half transfers will leave garbage
	d.retryErrors = false; % keep errored jobs in the queue and continue to retry
	d.dbstop = false; % try to restart an job locally if error and stop in keyboard mode
	d.waiting_function = []; % function handle, nor arguments, will be called periodically when waiting for jobs to be completed
	d.keepClusterFolder = false; % keep cluster folder even when finished without errors
	d.ssh_machines = hostname(); % comma-separated list of machines that can be accessed via ssh with password-less authentification
end;

if exist('new','var'), % Replace existing config
    c = new;
end;


if ~isempty(getenv('MATLABTOOLS')) && isempty(c), % Matlabtools is used but the config got lost (eg, clear functions)
    % Set c to the default values
    feval(mfilename,d);
    % Reload the config
    warning('Reloading matlabtools config');
    run(file_makepath(getenv('MATLABTOOLS'),'setup.m'));
    current = feval(mfilename);
    return
end

current = c;
