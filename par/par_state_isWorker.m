function w = par_state_isWorker(new)

persistent state;

if isempty(state)
	state = false;
end;

if exist('new','var')
	state = new;
end;

w = state;
