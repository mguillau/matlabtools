function varargout = par_maparg(outformat,f,shared,arguments,varargin)
% outformat
%	string of 'c' and 'a' for output "packaging"
%	c output arguments are returned as cell of results
%	a output arguments are returned as arrays of results
% f
%	function handle
% shared
%	cell array of arguments that a shared/singleton for f (first arguments)
% arguments
%	arguments as needed by f
%   

%nArguments = length(arguments);
%assert(nArguments>0);
n = numel(arguments);
%assert(all(cellfun(@(a) all(size(arguments{1})==size(a)),arguments)),'all multi-arguments need to be of the same size');

nout = nargout();
assert(length(outformat)==nout);
assert(all(outformat=='c' | outformat=='a'));

jobs = cell(size(arguments));
for i = 1:n
	jobs{i}.f = f;
	jobs{i}.args = arguments{i};
	jobs{i}.nout = nout;
end;

results = par_jobs(jobs,shared,varargin{:});

if nout>0
	varargout = cell(1,nout);
	for i = 1:nout
		varargout{i} = cellfun(@(r) r{i},results,'UniformOutput',outformat(i)=='a');
	end;
end;
