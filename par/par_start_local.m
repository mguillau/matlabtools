function par_start_local(clusterfolder,jobfolder,config)

if ~isempty(config.waiting_function)
	warning('waiting function in local mode will be called only once at the end');
end;

config.sharedfile = sprintf('%s/shared.mat',jobfolder);
config.restart = false;

par_worker(clusterfolder,jobfolder,false,config);

if ~isempty(config.waiting_function)
	config.waiting_function();
end;
