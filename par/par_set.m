function c = par_set(varargin)
% construct a par configuration
% only selected settings
%
% workers
% time_h = time in hours
% memory_g = memory in gigabytes
% maxtime_d = maxtime in days
% bundle
% restart = logical
% name = string

n = length(varargin);
assert(rem(n,2)==0);

c = struct();

stack = dbstack();
if length(stack)>=2
	c.name = stack(2).name;
end;

for i = 1:2:n
	
	name = varargin{i};
	value = varargin{i+1};
	
	switch name
		case 'workers'
			c.workers = value;
		case 'time_h'
			c.time = value * (60*60);
		case 'memory_g'
			c.memory = value * 1e9;
		case 'memory'
			c.memory = value;
		case 'maxtime_h'
			c.maxtime = value * (60*60);
		case 'maxtime_d'
			c.maxtime = value * (24*60*60);
		case 'bundle'
			c.bundle = value;
		case 'restart'
			c.restart = value;
		case 'name'
			c.name = value;
		case 'config'
			c = add_from_config(c,value);
		otherwise
			assert(false,name);
	end;
	
end;


function a = add_from_config(a,b)

fs = fieldnames(b);
for i = 1:length(fs)
	f = fs{i};
	a.(f) = b.(f);
end;
