
jobfolder = getenv('PAR_JOBFOLDER');
assert(~isempty(jobfolder));

currentfolder = getenv('PAR_CWD');
assert(~isempty(currentfolder));
cd(currentfolder);

par_path = getenv('PAR_PATH');
assert(~isempty(par_path));
addpath(par_path);

pid_file = getenv('PAR_MATLAB_PID_FILE');
if ~isempty(pid_file), % only if the environment variable says where to put it
    fid = fopen(pid_file,'wt');
    fprintf(fid,'%d\n',getpid);
    fclose(fid);
end

if exist('cache_config','file')==2 % only if cache_* is included
	cache_config(load_struct({'%s/cache_config.mat',jobfolder}));
end;

config = load_var({'%s/config.mat',jobfolder});
config.sharedfile = getenv('PAR_SHAREDFILE');

par_worker(jobfolder,jobfolder,true,config);

% TODO only for sge
sge_task_id = getenv('SGE_TASK_ID');
if ~isempty(sge_task_id)
	vmpeak = system_vmpeak();
	save_vars({'%s/sge/info_%s.mat',jobfolder,sge_task_id},'vmpeak',vmpeak);
end;

