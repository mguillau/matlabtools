function par_worker(clusterfolder,jobfolder,doTryCatch,config)

if ~exist('doTryCatch','var'), doTryCatch = true; end;

assert(strcmp(clusterfolder,jobfolder)); % TODO for now, no nesting yet

% set new isWorker state
old_isWorker = par_state_isWorker();
old_isWorker_cleanUp = onCleanup(@() par_state_isWorker(old_isWorker));
par_state_isWorker(true);

% set state
par_state_clusterfolder(clusterfolder);

% current data
c_folder = '';
c_info = [];
c_shared = [];

info = par_loadInfo(jobfolder);
done_cache = [];

done = false;
while ~done
	
	[done found folder index lock done_cache] = getNext(clusterfolder,jobfolder,info,done_cache);
	
	if found
		
		if ~strcmp(c_folder,folder)
			c_folder = folder;
			c_info = par_loadInfo(c_folder);
% 			c_shared = load_var(c_info.shared);
			c_shared = load_var(config.sharedfile);
		end;
		
		par_state_folder(folder);
		par_state_index(index);
		
		pg_begin(sprintfn('computing job %[index]d'));
		j = load_struct(c_info.jobs{index}.in);
		r = cell(1,j.nout);
		if doTryCatch
			try
				if j.nout==0
					j.f(c_shared{:},j.args{:});
				else
					[r{:}] = j.f(c_shared{:},j.args{:});
				end;
			catch e
				save_var({'%s/errors/%d.mat',jobfolder,index},e);
			end;
		else
			if j.nout==0
				j.f(c_shared{:},j.args{:});
			else
				[r{:}] = j.f(c_shared{:},j.args{:});
			end;
		end;
		asave_var(c_info.jobs{index}.out,r);
		clear j r lock;
		pg_end('saved');
		
		if config.restart
			pg_message('exiting worker, because config.restart==true');
			break;
		end;
		
	else
		
		pg_message('exiting worker, nothing found');
		if ~done, pg_message('(but still not all done)'); end;
		break;
			
	end;
	
	% alternatively, only exit, when all is done (keep on searching)
% 	elseif ~done
% 		
% 		assert(isempty(lock));
% 		pause(5+randi(5)); % wait until recheck
% 		
% 	end;
	
end;

% set old isWorker state
clear old_isWorker_cleanUp;


function [done found folder index lock done_cache] = getNext(clusterfolder,jobfolder,info,done_cache)

assert(strcmp(clusterfolder,jobfolder)); % TODO for now, no nesting yet

pg_begin('searching queue','%d/%d');

if isempty(done_cache)
	done_cache = false(info.n,1);
end;

allDone = true;

found = false;
for i = 1:info.n
	pg_update(i,info.n);
	if done_cache(i), continue, end;
	if file_exist(info.jobs{i}.out)
		done_cache(i) = true;
	else
		allDone = false;
		lock = file_slock(info.jobs{i}.lock,'job');
		if ~isempty(lock)
			done = false;
			found = true;
			folder = jobfolder;
			index = i;
			break;
		end;
	end;
end;

if ~found
	done = allDone;
	found = false;
	folder = [];
	index = [];
	lock = [];
end;

if found
	pg_end('found job %d',index);
else
	if done
		pg_end('all done');
	else
		pg_end('found nothing (not all done)',found);
	end;
end;
