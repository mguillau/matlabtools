function keepClusterfolder = par_start_ssh(clusterfolder,jobfolder,config)

if exist('cache_config','file')==2 % only if cache_* is included
	asave_struct({'%s/cache_config.mat',jobfolder},cache_config());
end;

[jobids nTasks] = submitJob(jobfolder,config);
del = onCleanup(@() removeJob(jobids));
keepClusterfolder = waitForJob(jobfolder,jobids,config.workers,nTasks,config);

function removeJob(ids)
for i=1:length(ids),
    if ids(i).remote_matlab_pid>0,
        system(sprintf('ssh -n %s kill -9 %d',ids(i).machine,ids(i).remote_matlab_pid));
    end
    if exist(sprintf('/proc/%d',ids(i).ssh_pid),'file'),
        system(sprintf('kill %d',ids(i).ssh_pid));
    end
end


function [jobids nTasks] = submitJob(jobfolder,config)

%pg_message('restart = %1d',config.restart);
config.restart=false;
pg_message('dbstop = %1d',config.dbstop);

% TODO: filter machines based on memory/cpu ressources
machines = str_split(config.ssh_machines,',');
pg_message('using machines =');
pg_message(' %s',machines{:});
nWorkers = length(machines);

% running time of one worker, in seconds
assert(config.time~=inf);
rt = config.time;

% memory usage of one worker, in bytes
assert(config.memory~=inf);
vmem = config.memory;

%pg_message('time = %.2f hours',rt/60/60);
pg_message('memory = %.1f GB',vmem/1e9);

nTasks = ceil( (config.maxtime/config.time) * nWorkers );
if nTasks>75000
	warning('nTasks>75000, clamping to 75000, current sge setting doesnt allow more.');
	nTasks = 75000;
end;

sshfolder = sprintf('%s/ssh',jobfolder);
file_mkdir(sshfolder);

%if config.distributeFilesToBenders
%	sharedfile = sge_distributeFile(sprintf('%s/shared.mat',jobfolder),config.distributeTimeout);
%	if isnan(sharedfile)
%		pg_message('distributeFile failed, using local scratch instead (might be slow)');
%		sharedfile = sprintf('%s/shared.mat',jobfolder);
%		distributedFile = nan;
%	else
%		distributedFile = sharedfile;
%	end;
%else
	sharedfile = sprintf('%s/shared.mat',jobfolder);
	distributedFile = nan;
%end;

if isempty(config.name)
	qsub_N = str_head(file_getName(jobfolder),2);
else
	qsub_N = sprintf('%s_%s',str_head(file_getName(jobfolder),2),config.name);
end;

par_dir = getenv('MATLABTOOLS');
assert(~isempty(par_dir));
par_ssh_path = [ par_dir '/par/par_ssh' ];
par_matlab_path = [ par_dir '/par/par_matlab' ];
par_worker_path = [ par_dir '/par/par_start_condor_worker.m' ];
%par_vsize_path = [ par_dir '/par/par_vsize.pl' ];

%cmd = sprintf( ...
%		'qsub -N par_%s -t %d-%d -tc %d -l h_rt=%d -l h_vmem=%d -v PAR_MATLAB_BIN=%s -v PAR_JOBFOLDER=%s -v PAR_SHAREDFILE=%s  -v PAR_WORKER_PATH=%s -v PAR_PATH=%s -cwd -o %s -e %s -terse -b y %s' , ...
%		qsub_N,1,nTasks,nWorkers,rt,vmem,config.matlab,jobfolder,sharedfile,par_worker_path,path,sgefolder,sgefolder,par_matlab_path ...
%	);

machine_count = struct();
jobids = struct();
for i=1:length(machines),
    machine = machines{i};
    if ~isfield(machine_count,machine),
        machine_count.(machine) = 0;
    end
    this_machine_worker = machine_count.(machine) + 1;
    machine_count.(machine) = this_machine_worker;
    outfile = sprintf('%s/par_%s.output.%s.%d',sshfolder,qsub_N,machine,this_machine_worker);
    errfile = sprintf('%s/par_%s.error.%s.%d',sshfolder,qsub_N,machine,this_machine_worker);
    pidfile = sprintf('%s/par_%s.pid.%s.%d',sshfolder,qsub_N,machine,this_machine_worker);
    cmd = sprintf( ...
        '/bin/bash %s %s %s %s %s ''PAR_CWD=%s PAR_MATLAB_BIN=%s PAR_MATLAB_PID_FILE=%s PAR_JOBFOLDER=%s PAR_SHAREDFILE=%s PAR_WORKER_PATH=%s PAR_PATH=%s''' , ...
        par_ssh_path, machine, par_matlab_path, outfile, errfile, pwd, config.matlab, pidfile, jobfolder, sharedfile, par_worker_path, path_user()   ...
    );

    %fprintf([ cmd '\n' ]);
    [status,result] = system(cmd);
    assert(status==0,'ssh error: %s',result);

    jobids(i).ssh_pid = sscanf(result,'%d');
    assert(jobids(i).ssh_pid>0,'failed to parse pid for ssh process: %s',result);

    jobids(i).machine = machine;
    jobids(i).worker = this_machine_worker;
    jobids(i).pidfile = pidfile;
    jobids(i).remote_matlab_pid = -1;
    jobids(i).vmpeak = -1;
end

function keepClusterfolder = waitForJob(folder,jobids,nWorkers,nTasks,config)

info = par_loadInfo(folder);

done_ssh = false;
[status,result] = system(sprintf('mkdirhier %s',folder));
assert(status==0,result);

if config.retryErrors
	pg_message('WARNING: retryErrors==true, might loop forever');
end;

done_jobs = false(info.n,1);

pg_message('par_ssh starting (%d workers)',length(jobids));
% pg_begin('sge','%d/%d done (%dR %dI %dH of %d/%d workers@cluster %d, %d errors)');
% pg_begin({'sge %d',jobid},'%d/%d, %s/%d (%de)');
pg_begin('','%d/%d, %s/%d (%de) (wf %s)');

while ~done_ssh && ~all(done_jobs)
	
	% check progress
    for i = 1:info.n
        if ~done_jobs(i)
            done_jobs(i) = file_exist(info.jobs{i}.out);
        end;
    end;
	
	% adapt number of running workers (option -tc), if less tasks than workers
%	if sum(~done_jobs)<nWorkers && ~all(done_jobs)
%		nWorkers = sum(~done_jobs);
		%[status,result] = system(sprintf('qalter -tc %d %d',nWorkers,jobid));
		%assert(status==0,result);
%	end;

	% check worker errors
	errors = file_list(sprintf('%s/errors',folder),'^[0-9]*\.mat$');
	nerrors = length(errors);
	if ~config.retryErrors && ~isempty(errors)
		error = load_var({'%s/errors/%s',folder,errors{1}});
		fprintf('\n\nERROR IN: %s\n\n',errors{1});
		%[status,result] = system(sprintf('qdel %d',jobid));
                removeJob(jobids);
		if config.dbstop
			fprintf('attempting to start locally and reproduce the error\n');
			index = str2num(file_getName(errors{1}));
			j = load_struct(info.jobs{index}.in);
			r = cell(1,j.nout);
			shared = load_var(info.shared);
			if j.nout==0
				keyboard; % continue with debugging, check errors for info
				j.f(shared{:},j.args{:});
			else
				keyboard; % continue with debugging, check errors for info
				[r{:}] = j.f(shared{:},j.args{:});
			end;
		else
			rethrow(error);
		end;
	end;
    
	% check ssh tasks.
        result = {};
        for i=1:length(jobids),
            if ~exist(sprintf('/proc/%d/stat',jobids(i).ssh_pid),'file'), % the ssh process is dead
                %warning('ssh #%d (%d) is dead',i,jobids(i).ssh_pid);
                break;
            end
            state = 'r'; % init phase: ssh is alive
% TODO: a way to read jobid.vmpeak
            if exist(jobids(i).pidfile), % TODO: find a faster thing. NFS too slow to synchronise
%                state = 's'; % matlab job has started, the pidfile has been created by the remote worker
                if jobids(i).remote_matlab_pid==-1, % if remotepid not set yet
                   remotepid_tmp = dlmread(jobids(i).pidfile); % read it from the file
                   if isnumeric(remotepid_tmp) && remotepid_tmp>0, % it's valid, save it
                       jobids(i).remote_matlab_pid = remotepid_tmp;
                       %warning('job %d has pid %s:%d',i,jobids(i).machine,jobids(i).remotepid);
                   end
                end
%                if jobids(i).remote_matlab_pid>0, % valid remote pid, get remote stats
%                   state = 'r';
%                   [status,res] = system(sprintf('ssh %s cat /proc/%d/stat',jobids(i).machine,jobids(i).remote_matlab_pid));
%                   res = textscan(res,'%d %s %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d');%trtrim(res);
%                   if status==0 && length(res{23})==1,
%                       %state = res{3}{1}; % read state from stat (but 'S' is not very explicit...)
%                       jobids(i).vmpeak = max(jobids(i).vmpeak,res{23}(1));
%                   end
%                end
            end
            result{end+1} = state;
        end
        
	if isempty(result)
		states = 'na';
		done_ssh = true;
	else
		[statenames,~,states] = unique(result);
		statecounts = hist(states,1:length(statenames));
        if nerrors>0,
            statenames{end+1} = 'e';
            statecounts(end+1) = nerrors;
        end
		states = cellfun(@(c,s) sprintf('%d%s',c,s),mat2cell(statecounts,1,ones(1,length(statecounts))),statenames,'UniformOutput',false);
		states = cell2mat(states);
	end;
	
	if isempty(config.waiting_function)
		wf_status = '';
	else
		wf_status = config.waiting_function();
	end;
	
	pg_update(sum(done_jobs),info.n,states,nWorkers,nerrors,wf_status);

	% don't update too often
	pause(10);
        system('sync');
	
end;
pause(5);
system('sync');

% check progress
for i = 1:info.n
	if ~done_jobs(i)
		done_jobs(i) = file_exist(info.jobs{i}.out);
	end;
end;

pg_end('%d/%d done',sum(done_jobs),info.n);

if ~all(done_jobs)
	warning('par_start_sge:incomplete','not all jobs have been completed');
	keepClusterfolder = true;
else
	keepClusterfolder = false;
end;

vmpeak = -1;
ninfos = 0;
for i = 1:length(jobids),
	if jobids(i).vmpeak>0,
		vmpeak = max(vmpeak,jobids(i).vmpeak);
		ninfos = ninfos+1;
	end;
end;
pg_message('max VmPeak = %s (%d measurements)',str_frombytes(vmpeak),ninfos);

% if nErrors>0
% 	nErrors = dir(sprintfn('%[folder]s/condor/worker*.err'));
% 	fprintf('\n\n\n---- errors ----\n\n\n');
% 	for i = 1:length(nErrors)
% 		if nErrors(i).bytes>0
% 			fprintf('worker %d\n',i-1);
% 			dumpErrorMessage(sprintf('%s/condor/%s',folder,nErrors(i).name));
% 		end;
% 	end;
% 	nErrors = sum([nErrors.bytes]>0);
% 	warning('there have been errors');
% 	keepClusterfolder = true;
% 	% TODO output, or move to special directory?
% else
% 	keepClusterfolder = false;
% end;


% function dumpErrorMessage(file)
% 
% f = fopen(file);
% l = fgetl(f);
% while ischar(l)
% 	fprintf('%s\n',l);
% 	l = fgetl(f);
% end;
% fclose(f);
