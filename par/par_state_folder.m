function folder = par_state_folder(new)

persistent state;

if exist('new','var')
	state = new;
end;

folder = state;
