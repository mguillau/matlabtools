function dfile = sge_distributeFile(file,timeout)
% timeout is in seconds, inf or none = no timeout
% dfile==nan if it failed because of timeout
% otherwise, path to distributed file (local to every bender)

pg = pg_start('sge_dsitributeFile');
pg_message('file %s',str_shortenPath(file,60));

if ~exist('timeout','var')
	timeout = inf;
end;

available = findAvailableBenders(43211);
assert(length(available)>0,'no benders available!');

binary = '/scratch_net/googolplex/stephaga/build-debian60amd64-release/cpp/distributed_cache/distribute_file';

benders = arrayfun(@(i) sprintf('bender%02d:43211,',i),available,'UniformOutput',false);
benders = reshape(benders,1,[]);
benders = cell2mat(benders);
benders = benders(1:end-1);

% system(sprintf('LD_LIBRARY_PATH= %s -file_to_replicate=%s -replicate_on=%s',binary,file,benders));

if timeout==inf
	[status,result] = system(sprintf('LD_LIBRARY_PATH= %s -file_to_replicate=%s -replicate_on=%s',binary,file,benders));
	assert(status==0,result);
else
	[status,result] = system(sprintf('LD_LIBRARY_PATH= timeout %d %s -file_to_replicate=%s -replicate_on=%s',timeout,binary,file,benders));
	if status==124
		pg_message('distributeFile failed after a timeout of %ds',timeout);
		dfile = nan;
		return;
	else
		assert(status==0,result);
	end;
end;

dfile = result(length('File SHA1 is 3'):length('File SHA1 is 354A1B6BA262FEFCB2DC51F1857016538DC39993'));
pg_message('hash %s',dfile);

dfile = sprintf('/scratch-second/stephaga/distributed_cache/%s',dfile);

pg.done();


function available = findAvailableBenders(port)

available = [];
noReply = {};
noService = {};
n = 52;

for i = 1:n

	[status,result] = system(sprintf('GLOG_logtostderr=0 /home/awesome/lib/rpclight/bin/rpcclient bender%02d %d',i,port));
	if status==0
		if str_islike(result,'.*distributed_cache.CacheNodeService.*')
			available(end+1) = i;
		else
			noService{end+1} = i;
% 			pg_message('bender%02d not available',i);
		end;
	else
		noReply{end+1} = i;
	end;

end;

pg_message('%d/%d benders available',length(available),n);

if length(available)<n
	pg_message('(no reply %s)',sprintf('%d ',noReply{:}));
	pg_message('(no service %s)',sprintf('%d ',noService{:}));
end;
