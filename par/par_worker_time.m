function [n,t,w] = par_worker_time()
persistent start_time
persistent worker_time
secondsPerDay = 24*3600;

if isempty(start_time) || isempty(worker_time),
    start_time = getenv('PAR_MATLAB_START_TIME');
    if isempty(start_time)
        start_time = -1;
    else
        start_time = str2double(start_time)/secondsPerDay;
    end
    jobfolder = getenv('PAR_JOBFOLDER');
    if isempty(jobfolder),
        worker_time = -1;
    else
        config = load_var({'%s/config.mat',jobfolder});
        worker_time = config.time/secondsPerDay;
    end
end
[~,n] = system('date +%s');
n=str2double(n)/(24*60*60);
t=start_time;
w=worker_time;
