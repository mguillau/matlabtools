function varargout = par_calls(calls,nWorkers)
% varargin
%	one or more { function, args{:} }
% varargout
%	concatenated output of the functions in varargin (only one output per call)
% nWorkers (optional)
%	number of workers, defaults to number of calls

n = numel(calls);
assert(nargout()==n);

if ~exist('nWorkers','var'), nWorkers = n; end;

jobs = cell(1,n);
for i = 1:n
	jobs{i}.f = calls{i}{1};
	jobs{i}.args = calls{i}(2:end);
	jobs{i}.nout = 1;
end;

results = par_jobs(jobs,{},nWorkers);
varargout = cellfun(@(o) o{1},results,'UniformOutput',false);
