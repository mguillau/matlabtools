function jobfolder = par_createJobfolder(superfolder,index)

info = load_struct({'%s/info.mat',superfolder});
jobfolder = info.jobs{index}.folder;
assert(~file_exist(jobfolder));
file_mkdir(jobfolder);
