#!/usr/bin/perl
# The goal is to monitor the matlab process for state (has the job finished? how much memory is it using? etc) faster than using nfs files
#

die 'error'; # In development : not active yet!

use strict;
use warnings;
use IO::Socket::INET;
use FileHandle;

# TODO: listener has status, remotepid, kill (to kill worker and itself)
# TODO: worker sends remote_pid to listener

my $stdout = shift;
my $stderr = shift;
pipe(READ,WRITE);
WRITE->autoflush();

#$SIG{TERM} = sub { $continue = 0 };

sub listener {
  # auto-flush on socket
  $| = 1;
 
  # creating a listening socket
  my $socket = new IO::Socket::INET (
    LocalHost => '0.0.0.0', # localhost
    LocalPort => '0', # use random port
    Proto => 'tcp',
    Listen => 100,
    Reuse => 1
  );
  die "cannot create socket $!\n" unless $socket;
  my $port = $socket->sockport();
  print WRITE "port=$port\n";
 
  my $status = 's';
 
  while(1)
  {
    # waiting for a new client connection
    my $client_socket = $socket->accept();
 
    # get information about a newly connected client
    my $client_address = $client_socket->peerhost();
    my $client_port = $client_socket->peerport();
    #print "connection from $client_address:$client_port\n";
 
    # read up to 1024 characters from the connected client
    my $data = "";
    $client_socket->recv($data, 1024);
    #print WRITE "received data: $data\n";

    my $d = $data;
    chomp($d);
    # TODO: make a switch. options are: setpid=%d, status, vmem, kill

    # write response data to the connected client
    my $file = "/proc/" . $d . "/stat";
    #print "file: ",$file,"\n";
    open(FID,"<",$file);
    $data = <FID>;
    close(FID);
    #print "data: ",$data;
    $client_socket->send($data);
 
    # notify client that response has been sent
    shutdown($client_socket, 1);
  }
 
  $socket->close();
}

sub launch_listener {
   # Pipe for child->parent communication
   my $pid = fork ();
   if (!defined $pid || $pid < 0) {
     die "fork failed: $!";
   } 
   if ($pid) { # parent
     my $port;
     PR:
     while(<READ>) {
       if (/port=/) {
         $port = $_;
         $port =~ s/port=//;
         last PR;
       } else {
         print;
       }
     };
     return $port;
   } else { # child
     listener();
     #print WRITE "listener ended\n";
     exit;
   }
}

sub worker {
     my $continue = 10;
     while ($continue) {
         print "tic.. $continue\n";
         $continue = $continue-1;
         system('sleep 2');
     }
}

sub launch_worker {
   my $stdout = shift;
   my $stderr = shift;
   my $pid = fork ();
   if (!defined $pid || $pid < 0) {
     die "fork failed: $!";
   } 
   if ($pid) { # parent
     return 0;
   } else { # child
     # capture output of worker
     open (STDIN, "</dev/null");
     open (STDOUT, ">$stdout");
     open (STDERR, ">$stderr");
     worker;
     print "worker ended\n";
     exit;
   }
}

sub daemonize {
   print "daemonizing\n";
   use POSIX;
   POSIX::setsid or die "setsid: $!";
   my $pid = fork ();
   if ($pid < 0) {
      die "fork: $!";
   } elsif ($pid) {
      exit 0;
   }
   chdir "/";
   umask 0;
   foreach (0 .. (POSIX::sysconf (&POSIX::_SC_OPEN_MAX) || 1024))
      { POSIX::close $_ }
   open (STDIN, "</dev/null");
   open (STDOUT, ">/dev/null");
   open (STDERR, ">&STDOUT");
}


my $port=launch_listener;
print "listener launched on port $port\n";
my $remotepid = launch_worker($stdout,$stderr,$port);
print "worker launched, using pid $remotepid\n";
print "parent waits\n";
#waitpid();
# daemonize;

