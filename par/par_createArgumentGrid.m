function P = par_create_argument_grid(varargin)
% Creates a grid of arguments for par_map

G = create_argument_grid(varargin{:});
nargs = numel(varargin);
P = cell(1,nargs);
for j=1:nargs
    P{j} = cellfun2(@(x) x{j},G);
end

end
