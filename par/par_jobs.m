function results = par_jobs(jobs,shared,config)
% jobs
%	cell array of job structs (can have any shape)
%	members:
%		.f = function handle
%		.args = cell array of arguments (passed after shared arguments)
%		.nout = number return arguments
% shared (optional, default {})
%	cell aray of shared arguments
%	will be the first arguments to the job functions
% config (optional, default empty struct)
%	struct with only the fields to be overwritten from the default
% results
%	cell array of cell arrays of results
%	(will have the same shape as jobs, results per job will be row cell vectors)
%
% Notes:
%	There won't necessarily be a new matlab instance per job.
%	Be careful when using functions with (global) side effects.

% arguments
if ~exist('jobs','var'), error('par_jobs:error','argument jobs is missing'); end;
if ~exist('shared','var'), shared = {}; end;
if ~exist('config','var'), config = struct(); end;

runAsLoop = false;
if length(jobs)==1
	pg_message('par: only one job, calling directly');
	runAsLoop = true;
elseif par_state_isWorker()
	% TODO currently not allowing nested calls
	pg_message('par: running inside a worker, executing as a loop');
	runAsLoop = true;
end;

if runAsLoop
	results = cell(length(jobs),1);
	pg = pg_start('job','%d/%d');
	for i = 1:length(jobs)
		pg.update(i,length(jobs));
		results{i} = cell(1,jobs{i}.nout);
		if jobs{i}.nout==0
			jobs{i}.f(shared{:},jobs{i}.args{:});
		else
			[results{i}{:}] = jobs{i}.f(shared{:},jobs{i}.args{:});
		end;
	end;
	pg.done('%d',length(jobs));
	return;
end;

config = configFromDefaults(par_config(),config);
nWorkers = config.workers;
nBundle = config.bundle;

pg_begin({'par_jobs %s',config.name});

assert(rem(nWorkers,1)==0);

% bundle
if nBundle>1
	results = bundleJobs(jobs,shared,nWorkers,nBundle,config);
	return;
end;

% get cluster folder
if par_state_isWorker(), clusterfolder = par_state_clusterfolder();
else clusterfolder = par_createClusterfolder(config); end;

% lock cluster
clusterlock = par_lockCluster(clusterfolder); %#ok<NASGU>

% get job folder
if par_state_isWorker(), jobfolder = par_createJobfolder(par_state_folder(),par_state_index());
else jobfolder = clusterfolder; end;

% fill job folder
queueJob(clusterfolder,jobfolder,jobs,shared,config);

% unlock cluster
clear clusterlock;

% compute
if par_state_isWorker(), par_worker(clusterfolder,jobfolder);
else
	if length(jobs)==1
		warning('par_jobs:singleJob','not more than 1 job, computing locally');
		config.engine = 'local';
	end;
	switch config.engine
		case 'condor'
			keepClusterfolder = par_start_condor(clusterfolder,jobfolder,nWorkers);
		case 'sge'
			keepClusterfolder = par_start_sge(clusterfolder,jobfolder,config);
                case 'ssh'
			keepClusterfolder = par_start_ssh(clusterfolder,jobfolder,config);
		case 'local'
			par_start_local(clusterfolder,jobfolder,config);
			keepClusterfolder = false;
		otherwise
			assert(false,'unknown engine %s',config.engine);
	end;
end;

% collect results
results = collectResults(jobfolder);

% clean up
if keepClusterfolder || config.keepClusterFolder
	warning('clusterfolder %s is still there to debug',clusterfolder);
else
	cleanUp(clusterfolder,jobfolder);
end;

pg_end();


function queueJob(clusterfolder,jobfolder,jobs,shared,config)

assert(file_exist(jobfolder));

info.clusterfolder = clusterfolder;
info.jobfolder = jobfolder;
info.n = numel(jobs);
info.nShape = size(jobs);

info.shared = sprintfn('%[jobfolder]s/shared.mat');
save_var(info.shared,shared);
pg_message('shared.mat is %s',sizeOfVar('shared','h'));
pg_message('folder %s',file_getName(jobfolder));

pg_begin('queueing','%d/%d');
info.jobs = cell(info.n,1);
for i = 1:info.n
	pg_update(i,info.n);
	info.jobs{i}.in = sprintfn('%[jobfolder]s/job%[i]03d_in.mat');
	assert(isfield(jobs{i},'f'));
	assert(isfield(jobs{i},'args'));
	assert(isfield(jobs{i},'nout'));
	save_struct(info.jobs{i}.in,jobs{i});
	info.jobs{i}.lock = sprintfn('%[jobfolder]s/job%[i]03d_lock');
	file_touch(info.jobs{i}.lock);
	info.jobs{i}.folder = sprintfn('%[jobfolder]s/job%[i]03d_queue');
	info.jobs{i}.out = sprintfn('%[jobfolder]s/job%[i]03d_out.mat');
end;
pg_end('%d',info.n);

file_mkdir({'%s/errors',jobfolder});

save_struct({'%s/info.mat',jobfolder},info);

save_var({'%s/config.mat',jobfolder},config);


function results = collectResults(folder)

info = par_loadInfo(folder);

results = cell(info.nShape);
for i = 1:info.n
	results{i} = load_var(info.jobs{i}.out);
end;


function cleanUp(cluster,folder)

assert(all(cluster==folder(1:length(cluster)))); % folder is subfolder of cluster to be sure
file_delete(folder);


function results = bundleJobs(jobs,shared,nWorkers,nBundle,c)

n = numel(jobs);
new_n = ceil(n/nBundle);
pg_message('bundling %d jobs down to %dx%d jobs',n,new_n,nBundle);
new_jobs = cell(new_n,1);
for i = 1:new_n
	i1 = (i-1)*nBundle+1;
	i2 = min(i*nBundle,n);
	new_jobs{i}.jobs = jobs(i1:i2);
        new_jobs{i}.ids = i1:i2;
end;

c.bundle = 1;
results = par_map('c',@computeBundledJobs,{shared},{new_jobs},c);

results = cat(1,results{:});
results = reshape(results,size(jobs));


function results = computeBundledJobs(shared,jobs)
ids=jobs.ids;
jobs=jobs.jobs;
n = length(jobs);
results = cell(n,1);
for i = 1:n
    pg_message('computing bundled job %d',ids(i));
    results{i} = cell(1,jobs{i}.nout);
    if jobs{i}.nout>0,
        [results{i}{:}] = jobs{i}.f(shared{:},jobs{i}.args{:});
    else
        jobs{i}.f(shared{:},jobs{i}.args{:});
    end
end;
