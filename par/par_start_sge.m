function keepClusterfolder = par_start_sge(clusterfolder,jobfolder,config)

if exist('cache_config','file')==2 % only if cache_* is included
	asave_struct({'%s/cache_config.mat',jobfolder},cache_config());
end;

[jobid distributedFile nTasks] = submitJob(jobfolder,config);
del = onCleanup(@() removeJob(jobid,distributedFile));
keepClusterfolder = waitForJob(jobfolder,jobid,config.workers,nTasks,config);

function removeJob(id,sharedfile)

[s,r] = system(sprintf('qdel %d',id));
if ~isnan(sharedfile)
	sge_undistributeFile(sharedfile);
end;


function [jobid distributedFile nTasks] = submitJob(jobfolder,config)

nWorkers = config.workers;

pg_message('restart = %1d',config.restart);
pg_message('dbstop = %1d',config.dbstop);

% running time of one worker, in seconds
assert(config.time~=inf);
rt = config.time;

% memory usage of one worker, in bytes
assert(config.memory~=inf);
vmem = config.memory;

pg_message('time = %.2f hours',rt/60/60);
pg_message('memory = %.1f GB',vmem/1e9);

nTasks = ceil( (config.maxtime/config.time) * nWorkers );
if nTasks>75000
	warning('nTasks>75000, clamping to 75000, current sge setting doesnt allow more.');
	nTasks = 75000;
end;

sgefolder = sprintf('%s/sge',jobfolder);
file_mkdir(sgefolder);

if config.distributeFilesToBenders
	sharedfile = sge_distributeFile(sprintf('%s/shared.mat',jobfolder),config.distributeTimeout);
	if isnan(sharedfile)
		pg_message('distributeFile failed, using local scratch instead (might be slow)');
		sharedfile = sprintf('%s/shared.mat',jobfolder);
		distributedFile = nan;
	else
		distributedFile = sharedfile;
	end;
else
	sharedfile = sprintf('%s/shared.mat',jobfolder);
	distributedFile = nan;
end;

if isempty(config.name)
	qsub_N = str_head(file_getName(jobfolder),2);
else
	qsub_N = sprintf('%s_%s',str_head(file_getName(jobfolder),2),config.name);
end;

par_dir = getenv('MATLABTOOLS');
assert(~isempty(par_dir));
par_matlab_path = [ par_dir '/par/par_matlab' ];
par_worker_path = [ par_dir '/par/par_start_condor_worker.m' ];

cmd = sprintf( ...
		'qsub -N par_%s -t %d-%d -tc %d -l h_rt=%d -l h_vmem=%d -v PAR_CWD=%s -v PAR_MATLAB_BIN=%s -v PAR_JOBFOLDER=%s -v PAR_SHAREDFILE=%s  -v PAR_WORKER_PATH=%s -v PAR_PATH=%s -cwd -o %s -e %s -terse -b y %s' , ...
		qsub_N,1,nTasks,nWorkers,rt,vmem,pwd,config.matlab,jobfolder,sharedfile,par_worker_path,path_user(),sgefolder,sgefolder,par_matlab_path ...
	);
%fprintf([ cmd '\n' ]);
[status,result] = system(cmd);
assert(status==0,'qsub error: %s',result);

jobid = sscanf(result,'%d');
assert(~isempty(jobid),'could not parse qsub output: %s',result);


function keepClusterfolder = waitForJob(folder,jobid,nWorkers,nTasks,config)

info = par_loadInfo(folder);

done_sge = false;
[status,result] = system(sprintf('mkdirhier %s',folder));
assert(status==0,result);

if config.retryErrors
	pg_message('WARNING: retryErrors==true, might loop forever');
end;

done_jobs = false(info.n,1);

pg_message('sge id %d',jobid);
% pg_begin('sge','%d/%d done (%dR %dI %dH of %d/%d workers@cluster %d, %d errors)');
% pg_begin({'sge %d',jobid},'%d/%d, %s/%d (%de)');
pg_begin('','%d/%d, %s/%d (%de) (wf %s)');

while ~done_sge && ~all(done_jobs)
	
	% check progress
    for i = 1:info.n
        if ~done_jobs(i)
            done_jobs(i) = file_exist(info.jobs{i}.out);
        end;
    end;
	
	% adapt number of running workers (option -tc), if less tasks than workers
	if sum(~done_jobs)<nWorkers && ~all(done_jobs)
		nWorkers = sum(~done_jobs);
		[status,result] = system(sprintf('qalter -tc %d %d',nWorkers,jobid));
		assert(status==0,result);
	end;

	% check worker errors
	errors = file_list(sprintf('%s/errors',folder),'^[0-9]*\.mat$');
	nerrors = length(errors);
	if ~config.retryErrors && ~isempty(errors)
		error = load_var({'%s/errors/%s',folder,errors{1}});
		fprintf('\n\nERROR IN: %s\n\n',errors{1});
		[status,result] = system(sprintf('qdel %d',jobid));
		if config.dbstop
			fprintf('attempting to start locally and reproduce the error\n');
			index = str2num(file_getName(errors{1}));
			j = load_struct(info.jobs{index}.in);
			r = cell(1,j.nout);
			shared = load_var(info.shared);
			if j.nout==0
				keyboard; % continue with debugging, check errors for info
				j.f(shared{:},j.args{:});
			else
				keyboard; % continue with debugging, check errors for info
				[r{:}] = j.f(shared{:},j.args{:});
			end;
		else
			rethrow(error);
		end;
	end;
    
	% check sge tasks
	[status,result] = system(sprintf('qstat -g d | awk ''/^[[:blank:]]*%d/ { print $5 }''',jobid));
	assert(status==0,'qstat error: %s',result);
	result = regexp(result,'\n','split');
	result = result(1:end-1);
	if isempty(result)
		states = 'na';
		done_sge = true;
	else
		[statenames,~,states] = unique(result);
		statecounts = hist(states,1:length(statenames));
        if nerrors>0,
            statenames{end+1} = 'e';
            statecounts(end+1) = nerrors;
        end
		states = cellfun(@(c,s) sprintf('%d%s',c,s),mat2cell(statecounts,1,ones(1,length(statecounts))),statenames,'UniformOutput',false);
		states = cell2mat(states);
	end;
	
	if isempty(config.waiting_function)
		wf_status = '';
	else
		wf_status = config.waiting_function();
	end;
	
	pg_update(sum(done_jobs),info.n,states,nWorkers,nerrors,wf_status);

	% don't update too often
	pause(10);
	
end;

% check progress
for i = 1:info.n
	if ~done_jobs(i)
		done_jobs(i) = file_exist(info.jobs{i}.out);
	end;
end;

pg_end('%d/%d done',sum(done_jobs),info.n);

if ~all(done_jobs)
	warning('par_start_sge:incomplete','not all jobs have been completed');
	keepClusterfolder = true;
else
	keepClusterfolder = false;
end;

vmpeak = -1;
ninfos = 0;
for i = 1:nTasks
	f = sprintf('%s/sge/info_%d.mat',folder,i);
	if file_exists(f)
		p = load_vars(f,'vmpeak');
		vmpeak = max(vmpeak,p);
		ninfos = ninfos+1;
	end;
end;
pg_message('max VmPeak = %s (%d measurements)',str_frombytes(vmpeak),ninfos);

% if nErrors>0
% 	nErrors = dir(sprintfn('%[folder]s/condor/worker*.err'));
% 	fprintf('\n\n\n---- errors ----\n\n\n');
% 	for i = 1:length(nErrors)
% 		if nErrors(i).bytes>0
% 			fprintf('worker %d\n',i-1);
% 			dumpErrorMessage(sprintf('%s/condor/%s',folder,nErrors(i).name));
% 		end;
% 	end;
% 	nErrors = sum([nErrors.bytes]>0);
% 	warning('there have been errors');
% 	keepClusterfolder = true;
% 	% TODO output, or move to special directory?
% else
% 	keepClusterfolder = false;
% end;


% function dumpErrorMessage(file)
% 
% f = fopen(file);
% l = fgetl(f);
% while ischar(l)
% 	fprintf('%s\n',l);
% 	l = fgetl(f);
% end;
% fclose(f);
