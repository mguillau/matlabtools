function folder = par_createClusterfolder(config)

parfolder = config.folder;
% file_mkdir(parfolder);
assert(file_exists(parfolder),'par folder %s does not exist',parfolder);

% earlier, I used a dedicated (local) folder per submitting machine
% machinefolder = getMachinefolder(parfolder,config);
% lock = getClusterslock(machinefolder); %#ok<NASGU>
% folder = createClusterfolder(config,machinefolder);
% clear lock;

lock = getClusterslock(parfolder); %#ok<NASGU>
folder = createClusterfolder(config,parfolder);
clear lock;

function lock = getClusterslock(folder) %#ok<INUSD>

lockfile = sprintfn('%[folder]s/clusters.lock');
if ~file_exist(lockfile)
	file_touch(lockfile);
end;

lock = file_swait(lockfile,'clusters');


function folder = createClusterfolder(config,machinefolder)

% find available folder
% timestamp = datestr(now(),'yyyy-mm-dd_HH-MM-SS');
timestamp = datestr(now(),'dd-HH.MM.SS');
folder = [];
while isempty(folder) || file_exist(folder)
	if isempty(config.name)
		folder = sprintf('%s/%s_%s',machinefolder,rndprefix(),timestamp);
	else
		folder = sprintf('%s/%s_%s_%s',machinefolder,rndprefix(),config.name,timestamp);
	end;
end;

file_mkdir(folder);

lockfile = sprintfn('%[folder]s/cluster.lock');
file_touch(lockfile);


function p = rndprefix()

p = [ char(randi([double('a'),double('z')])) char(randi([double('a'),double('z')])) ];


% function folder = getMachinefolder(parfolder,config)
% 
% assert(false,'now using one fixed folder, on the biwinas cache');
% 
% hostname = system_hostname();
% folder = sprintfn('%[parfolder]s/%[hostname]s');
% 
% if ~file_exist(folder)
% 	
% 	% create machine local folder
% 	lfolder = sprintf(config.machinefolder,hostname,hostname);
% 	file_mkdir(lfolder);
% 	
% 	% create symbolic link
% 	file_slink(folder,lfolder);
% 	
% 	pg_message('created par folder %s in %s',hostname,lfolder);
% 	
% end;
