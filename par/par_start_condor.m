function keepClusterfolder = par_start_condor(clusterfolder,jobfolder,nWorkers)

[condorfolder logfile] = writeConfiguration(jobfolder,nWorkers);
cluster = submitCluster(condorfolder);
keepClusterfolder = waitForCluster(jobfolder,logfile,cluster,nWorkers);


function [folder logfile] = writeConfiguration(jobfolder,nWorkers)
% jobfolder
%	par job folder
% nWorkers
%	number of condor workers to start
% folder
%	folder with condor start configuration

folder = sprintfn('%[jobfolder]s/condor');
assert(~file_exist(folder));
file_mkdir(folder);

condorfile = sprintfn('%[folder]s/cluster.condor');
logfile = sprintfn('%[folder]s/cluster.log');

f = fopen(condorfile,'w');
fprintf(f,'universe = vanilla\n');
fprintf(f,'getenv = true\n');
fprintf(f,'requirements = (Arch=="x86_64") && (Memory>=1000) && (kflops>=1000000)\n');

% fprintf(f,'requirements = (Arch=="x86_64") && (Memory>=1000) && (kflops>=1000000) && regexp("wilco.+",Machine)==False && regexp("stampy.+",Machine)==False\n');
% if now()>datenum('18-april-2011 00:00:00')
% 	warning('temporarily removed wilco and stampy from condor');
% end;

fprintf(f,'rank = kflops + regexp("bender.+",Machine)*10000000\n'); % higher is better
fprintf(f,'executable = /home/dkuettel/bin/dmatlab\n');
fprintf(f,'notification = never\n');
fprintf(f,'environment = PAR_JOBFOLDER=%s\n',jobfolder);
fprintf(f,'kill_sig = SIGINT\n'); % TODO nicht evictionsave, see SIGUSR1
fprintf(f,'arguments = -nodesktop -nosplash -nodisplay -singleCompThread v=\n');
fprintf(f,'log = %s\n',logfile);
fprintf(f,'output = %s/worker$(Process).out\n',folder);
fprintf(f,'error = %s/worker$(Process).err\n',folder);
fprintf(f,'input = %s\n',which('par_start_condor_worker'));
fprintf(f,'initialdir = %s\n',pwd());
fprintf(f,'priority = 0\n'); % higher is better
% fprintf(f,'nice_user = True\n'); 

% if now()>datenum('16-april-2011 00:00:00')
% 	warning('nice_user=true because of BMVC deadline');
% end;

fprintf(f,'queue %d\n',nWorkers);
fclose(f);

if exist('cache_config','file')==2 % only if cache_* is included
	asave_struct({'%s/cache_config.mat',jobfolder},cache_config());
end;


function cluster = submitCluster(folder) %#ok<INUSD>

[status,result] = system(sprintfn('condor_submit %[folder]s/cluster.condor'));
assert(status==0,'condor_submit error: %s',result);

cluster = sscanf(result,'Submitting job(s)%[.]\nLogging submit event(s)%[.]\n%d job(s) submitted to cluster %d.');
assert(~isempty(cluster),'could not understand condor_submit output: %s',result);

cluster = cluster(end);


function keepClusterfolder = waitForCluster(folder,logfile,cluster,nWorkers)

info = par_loadInfo(folder);

done_condor = false;
done_jobs = false(info.n,1);

pg_begin('condor','%d/%d done (%dR %dI %dH of %d/%d workers@cluster %d, %d errors)');

while ~done_condor && ~all(done_jobs)
	
	% wait for condor
	[done_condor,~] = system(sprintf('condor_wait -wait %d %s',10,logfile));
	done_condor = done_condor==0;
	
	% check progress
	for i = 1:info.n
		if ~done_jobs(i)
			done_jobs(i) = file_exist(info.jobs{i}.out);
		end;
	end;
	
	% check condor
	[~,status] = system(sprintf('condor_q %d | awk ''/^ *%d/ {print $6;}''',cluster,cluster));
	
	% check worker errors
	nErrors = dir(sprintfn('%[folder]s/condor/worker*.err'));
	nErrors = sum([nErrors.bytes]>0);
	
	% update
	pg_update( ...
			sum(done_jobs),info.n, ...
			sum(status=='R'),sum(status=='I'),sum(status=='H'), ...
			sum(status=='R')+sum(status=='I')+sum(status=='H'),nWorkers, ...
			cluster,nErrors ...
		);
	
	% don't update too often
	pause(10);
	
end;

pg_end('%d/%d done',sum(done_jobs),info.n);

if ~done_condor
	[~,~] = system(sprintfn('condor_rm %[cluster]d'));
	done_condor = true;
end;

assert(done_condor);

if nErrors>0
	nErrors = dir(sprintfn('%[folder]s/condor/worker*.err'));
	fprintf('\n\n\n---- errors ----\n\n\n');
	for i = 1:length(nErrors)
		if nErrors(i).bytes>0
			fprintf('worker %d\n',i-1);
			dumpErrorMessage(sprintf('%s/condor/%s',folder,nErrors(i).name));
		end;
	end;
	nErrors = sum([nErrors.bytes]>0);
	warning('there have been errors');
	keepClusterfolder = true;
	% TODO output, or move to special directory?
else
	keepClusterfolder = false;
end;

if ~all(done_jobs)
	warning('not all jobs have been completed');
end;


function dumpErrorMessage(file)

f = fopen(file);
l = fgetl(f);
while ischar(l)
	fprintf('%s\n',l);
	l = fgetl(f);
end;
fclose(f);
