function lock = par_lockCluster(folder)

lock = file_swait({'%s/cluster.lock',folder},{'locking cluster %s',file_getName(folder)});
