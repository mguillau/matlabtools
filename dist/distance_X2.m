function d = distance_X2(from,to)
% Computes the X2 distance from from to each to.
% Returns the distances in a row vector.
% Chooses the best implementation based on size/sparsity of inputs.
%
% from
%	from feature
%	one row
% to
%	to features
%	one feature per row

% TODO expecting only non negativ entries? since those are bins (counts can't be negative)
% TODO these asserts are heavy for sparse matrices (?)
% assert(all(from(:)>=0));
% assert(all(to(:)>=0));
% TODO also, put them in individual functions, not here

if issparse(to)
	d = distance_X2_sparse_mex(to,full(from));
else
	d = distance_X2_full(from,to);
end;
