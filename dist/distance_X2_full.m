function d = distance_X2_full(from,to)
% See distance_X2.m
% Implementation using repmat, works for full and sparse.
% It is not memory efficient.

% TODO mex memory in-place version?

assert(size(from,1)==1);
assert(size(from,2)==size(to,2));

if size(to,1)==0
	d = [];
else
	
	n = size(to,1);
	
	from = repmat(from,n,1);
	a = (to-from).^2;
	b = (to+from);
	b(b==0) = 1;
	d = sum( a./b , 2 );
	
end;
