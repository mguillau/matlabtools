#ifndef __DISTANCE_UTIL

// utilities to generate c literals
#define __DISTANCE_UTIL
#define XSTR(s) STR(s)
#define STR(s) #s
#define CAT(x,y) x ## y
#define XCAT(x,y) CAT(x,y)

// 3 distances
#define L1 1
#define L2 2
#define X2 3
#define L2SQ 4

// data structure used for threads. depends only on data type
#define DISTSTRUCT_S(data) XCAT(XCAT(distance_argstruct_,data),_s)
#define DISTSTRUCT_T(data) XCAT(XCAT(distance_argstruct_,data),_t)

// build function names for the different data and distances
#define DISTFUN_THREAD(data,dist) XCAT(XCAT(distance,dist),XCAT(data,_thread))
#define DISTFUN_IMPL(data,dist) XCAT(XCAT(distance,dist),XCAT(data,_implementation))

#define min(x,y) ((x)>(y)?(y):(x))
#define max(x,y) ((x)<(y)?(y):(x))
#define EPSILON 1e-15

#endif



