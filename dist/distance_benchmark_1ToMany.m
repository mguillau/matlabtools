function distance_benchmark()

% last run with N=100
% 	L1 repmat full: 155.2328 seconds
% 	L1 repmat sparse: 35.525300 seconds (4.37x faster)
% 	L1 mex: 0.935328 seconds (165.97x faster)
% 	L2 repmat full: 178.9918 seconds
% 	L2 repmat sparse: 45.303604 seconds (3.95x faster)
% 	L2 mex: 0.895426 seconds (199.90x faster)
% 	X2 repmat full: 387.7455 seconds
% 	X2 mex: 1.679044 seconds (230.93x faster)

N = 10; % number of trials
n = 45000;
d = 1728;
densities = [ 0.001, 0.0259, 0.1, 0.2, 0.5, 1 ];

for k=1:length(densities),
fprintf('data density = %f\n',densities(k));

features = sprand(n,d,densities(k));
Ffeatures = full(features);

% distance L1
[t,h] = time('L1 repmat full',-1,-1,N,@distance_L1_full,{Ffeatures(1,:),Ffeatures});
time('L1 bsxfun full',t,h,N,@distance_L1_full_bsxfun,{Ffeatures(1,:),Ffeatures});
time('L1 repmat sparse',t,h,N,@distance_L1_full,{features(1,:),features});
time('L1 bsxfun sparse',t,h,N,@distance_L1_full_bsxfun,{features(1,:),features});
time('L1 full mex',t,h,N,@distance_L1_full_mex,{Ffeatures,Ffeatures(1,:)});
time('L1 sparse mex',t,h,N,@distance_L1_sparse_mex,{features,Ffeatures(1,:)});

%% distance L2
[t,h] = time('L2 repmat full',-1,-1,N,@distance_L2_full,{Ffeatures(1,:),Ffeatures});
time('L2 bsxfun full',t,h,N,@distance_L2_full_bsxfun,{Ffeatures(1,:),Ffeatures});
time('L2 repmat sparse',t,h,N,@distance_L2_full,{features(1,:),features});
time('L2 bsxfun sparse',t,h,N,@distance_L2_full_bsxfun,{features(1,:),features});
time('L2 full mex',t,h,N,@distance_L2_full_mex,{Ffeatures,Ffeatures(1,:)});
time('L2 sparse mex',t,h,N,@distance_L2_sparse_mex,{features,Ffeatures(1,:)});

%% distance X2
%t = time('X2 repmat full',-1,N,@distance_X2_full,{full(features(1,:)),full(features)});
%time('X2 mex',t,N,@distance_X2_sparse_mex,{features,full(features(2,:))});
%
end

function [t,h] = time(name,bt,bh,N,f,args)

fprintf('%s:',name);

tic();
for i = 1:N
	d = f(args{:});
end;
t = toc();
h = DataHash(full(d));

if bt==-1, fprintf(' %.4f seconds\n',t);
else fprintf(' %f seconds (%.2fx faster)\n',t,bt/t); end;
if ischar(bh)&&~strcmp(h,bh),
  if size(args{1})==1,
     D = distance_L1_full(args{:});
  else
     D = distance_L1_full(args{2},args{1});
  end
  warning('incorrect output! rel.norm=%e',norm(d-D)/norm(D));
end

