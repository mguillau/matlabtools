function distance_L1_sparse_mex_test()

mex -largeArrayDims -outdir ../utilities ../utilities/distance_L1_sparse_mex.cpp

s = zeros(7,3);
s(2,1) = 1.3;
s(5,1) = 1;
s(3,2) = 1.1;
s(2,3) = 2.5;
s(5,3) = 1;
s(6,3) = 1;

x1 = distance_L1_sparse_mex(sparse(s),[10 -10 10]);
x2 = distance_L1_full([10 -10 10],s);

[ x1 x2 ]
