function d = distance_L2_full(from,to)
% See distance_L1.m
% Implementation using bsxfun, works for full and sparse.

% TODO mex memory in-place version?

assert(size(from,1)==1);
assert(size(from,2)==size(to,2));

if size(to,1)==0
	d = [];
else
	n = size(to,1);
	d = sqrt( sum( bsxfun(@minus,to,from).^2 , 2 ) );
end;
