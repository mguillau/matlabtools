function d = distance_L1(from,to)
% Computes the L1 distance from from to each to.
% Returns the distances in a row vector.
% Chooses the best implementation based on size/sparsity of inputs.
%
% from
%	from feature
%	one row
% to
%	to features
%	one feature per row

if issparse(to)
	d = distance_L1_sparse_mex(to,full(from));
else
	d = distance_L1_full(from,to);
end;
