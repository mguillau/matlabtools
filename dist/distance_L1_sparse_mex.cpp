#include "mex.h"
#include <cmath>

//matlab input: features (sparse), value (full)
//matlab output: L1 distances (full)
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	
	//check call
	if(nrhs!=2) mexErrMsgTxt("Exactly 2 input parameters expected.");
	if(!mxIsSparse(prhs[0])) mexErrMsgTxt("First input parameter needs to be sparse.");
	if(mxIsSparse(prhs[1])) mexErrMsgTxt("Second input parameter needs to be full.");
	if(nlhs>1) mexErrMsgTxt("Only one output.");
	
	//get features
	mwSize fm = mxGetM(prhs[0]);
    mwSize fn  = mxGetN(prhs[0]);
// 	mwSize fnzmax = mxGetNzmax(prhs[0]);
	double* fv  = mxGetPr(prhs[0]);
	mwIndex* fr = mxGetIr(prhs[0]);
	mwIndex* fc = mxGetJc(prhs[0]);
	
	//get value
	mwSize vm = mxGetM(prhs[1]);
	mwSize vn = mxGetN(prhs[1]);
	double* vv = mxGetPr(prhs[1]);
	
	//check dimensions
	if(fn!=vn) mexErrMsgTxt("Features and value need to have the same column size.");
	if(vm!=1) mexErrMsgTxt("Value needs to have one row.");
	
	//allocate output
	plhs[0] = mxCreateDoubleMatrix(fm,1,mxREAL);
	double* d = mxGetPr(plhs[0]);
	
	/* compute */
	
	//init distances (as if features==0)
	double d_init = 0;
	for(mwSize i=0; i<vn; i++) d_init += fabs(vv[i]); // TODO should be positive anyway
	for(mwSize i=0; i<fm; i++) d[i] = d_init;
	
	//traverse sparse (compressed column storage)
	for(mwSize j=0; j<fn; j++) { // for each column
		double vj = vv[j];
		double vja = fabs(vj);
		for(mwIndex ii=fc[j]; ii<fc[j+1]; ii++) { // sparse entries
			int i = fr[ii]; // row
			double diff = fabs(vj-fv[ii]) - vja;
			d[i] += diff;
		}
	}
	
}
