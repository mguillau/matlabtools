function d = distance_L1_full(from,to)
% See distance_L1.m
% Implementation using repmat, works for full and sparse.
% It is not memory efficient.

% TODO mex memory in-place version?

assert(size(from,1)==1);
assert(size(from,2)==size(to,2));

if size(to,1)==0
	d = [];
else
	
	n = size(to,1);
	d = sum( abs(to-repmat(from,n,1)) , 2 );
	
end;
