function distance_benchmark()

% last run with N=100
% 	L1 repmat full: 155.2328 seconds
% 	L1 repmat sparse: 35.525300 seconds (4.37x faster)
% 	L1 mex: 0.935328 seconds (165.97x faster)
% 	L2 repmat full: 178.9918 seconds
% 	L2 repmat sparse: 45.303604 seconds (3.95x faster)
% 	L2 mex: 0.895426 seconds (199.90x faster)
% 	X2 repmat full: 387.7455 seconds
% 	X2 mex: 1.679044 seconds (230.93x faster)

N = 1; % number of trials
n1 = 1000;
n2 = 1000;
d = 5000; %1728;
densities = 1; %[ 0.001, 0.0259, 0.1, 0.2, 0.5, 1 ];

for k=1:length(densities),
fprintf('data density = %f\n',densities(k));

features1 = sprand(n1,d,densities(k));
Ffeatures1 = full(features1);
Sfeatures1 = single(Ffeatures1);
features2 = sprand(n2,d,densities(k));
Ffeatures2 = full(features2);
Sfeatures2 = single(Ffeatures2);

% distance L1
[t,h] = time('L1 repmat full',-1,-1,N,1,@distance_L1_full,{Ffeatures1,Ffeatures2});
time('L1 bsxfun full',t,h,N,1,@distance_L1_full_bsxfun,{Ffeatures1,Ffeatures2});
time('L1 repmat sparse',t,h,N,1,@distance_L1_full,{features1,features2});
time('L1 bsxfun sparse',t,h,N,1,@distance_L1_full_bsxfun,{features1,features2});
time('L1 full mex',t,h,N,0,@distance_L1_full_mex,{Ffeatures1,Ffeatures2});
time('L1 full mex (single)',t,h,N,0,@distance_L1_full_mex,{Sfeatures1,Sfeatures2});
time('L1 sparse mex',t,h,N,2,@distance_L1_sparse_mex,{features1,Ffeatures2});

% distance L2
[t,h] = time('L2 repmat full',-1,-1,N,1,@distance_L2_full,{Ffeatures1,Ffeatures2});
time('L2 bsxfun full',t,h,N,1,@distance_L2_full_bsxfun,{Ffeatures1,Ffeatures2});
time('L2 repmat sparse',t,h,N,1,@distance_L2_full,{features1,features2});
time('L2 bsxfun sparse',t,h,N,1,@distance_L2_full_bsxfun,{features1,features2});
time('L2 full mex',t,h,N,0,@distance_L2_full_mex,{Ffeatures1,Ffeatures2});
time('L2 full mex (single)',t,h,N,0,@distance_L2_full_mex,{Sfeatures1,Sfeatures2});
time('L2 sparse mex',t,h,N,2,@distance_L2_sparse_mex,{features1,Ffeatures2});

%% distance X2
%t = time('X2 repmat full',-1,N,@distance_X2_full,{full(features(1,:)),full(features)});
%time('X2 mex',t,N,@distance_X2_sparse_mex,{features,full(features(2,:))});
%
end
end

function [t,h] = time(name,bt,bh,N,iter_arg,f,args)
switch name(1:2),
    case 'L1',
	ref=@distance_L1_full_mex;
    case 'L2',
	ref=@distance_L2_full_mex;
    case 'X2',
	ref=@distance_X2_full_mex;
end
fprintf('%s:',name);
if iter_arg>0, % in case we have to iterate, pre-allocate output memory
    d = zeros(size(args{1},1),size(args{2},1));
end

tic();
for i = 1:N
        if iter_arg==0,
		d = f(args{:});
	else
		for j=1:size(args{iter_arg},1),
                        if iter_arg==1,
				d(j,:) = f( args{1}(j,:), args{2} );
			else
				d(:,j) = f( args{1}, args{2}(j,:) );
			end
		end
	end
end;
t = toc();
h = DataHash(full(double(d)));

if bt==-1, fprintf(' %.4f seconds\n',t);
else fprintf(' %f seconds (%.2fx faster)\n',t,bt/t); end;
if ischar(bh) && ~strcmp(h,bh),
  D = ref(double(full(args{1})),double(full(args{2})));
  warning('incorrect output! rel.norm=%e',norm(d-D)/norm(D));
end
end

