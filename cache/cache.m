function varargout = cache(fun,args,config)

assert(iscell(args),'invalid argument list');

default config struct();
config = configFromDefaults(cache_config(),config);

[fullfile,infofile] = cache_make_filename(fun,args,config);

if file_exist(fullfile)
    
    cache_print(config,'cache exists');
    varargout = load_var(fullfile);
    
else

    cache_print(config,'cache does not exist');
    folder = fileparts(fullfile);
    file_mkdir(folder);
    doLock = config.lock_folder || config.lock_file;

    if config.lock_folder,
        lockfile = [ config.folder '.lock' ];
    else if config.lock_file,
        lockfile = [ fullfile '.lock' ];
    end; end
    
    if doLock,
        cache_print(config,'locking %s',lockfile);
        lock = file_swait(lockfile,sprintf('waiting for locked %s',lockfile));
        if file_exist(fullfile), % if there was a lock, maybe the file now exists
            varargout = load_var(fullfile);
            clear lock;
            varargout = varargout(1:nargout);
            return;
        end
    end

    fun_nargout = nargout(fun);
    if fun_nargout>0, % there is a defined number of output
        cache_print(config,'function has %d output args',fun_nargout);
        varargout = cell(1,nargout(fun));
    else % there is a variable number of outputs, use the current output request
        cache_print(config,'function has variable output args, using %d',max(1,nargout));
        varargout = cell(1,max(1,nargout));
    end
    cache_print(config,'computing');
    [varargout{:}] = feval(fun,args{:});

    if config.safe_save,
        cache_print(config,'safe-saving result in %s',fullfile);
        asave_var(fullfile,varargout);
    else
        cache_print(config,'saving result in %s',fullfile);
        save_var(fullfile,varargout);
    end

    % TODO: also print information in infofile (like, a human-readable version of (fun,args))

    if doLock,
        clear lock;
    end

end

varargout = varargout(1:nargout);

end

function cache_print(config,message,varargin)
if config.verbose, fprintf([message '\n'],varargin{:}); end
end

