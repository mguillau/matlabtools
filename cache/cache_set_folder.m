function cache_set_folder(folder)

% remove trailing '/'
while folder(end)=='/',
    folder=folder(1:end-1);
end

c=cache_config();
c.folder=folder;
cache_config(c);

