function cache_wipe(config)

default config struct();
config = configFromDefaults(cache_config(),config);

if isempty(config.name),
    warning('About to erase all the content of %s',config.folder);
    str = input('Are you sure? [yes/no] ','s');
    if strcmpi(str,'yes'),
        cached_files = dir2(config.folder,['^[^.]']);
    else
        return;
    end
else
    cached_files = dir2(config.folder,['^' config.name]);
end

for f=1:length(cached_files),
    progress_bar(f,length(cached_files));
    fullfile = file_makepath(config.folder,cached_files{f});
    file_delete(fullfile);
end 
