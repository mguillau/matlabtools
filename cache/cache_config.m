function current = cache_config(new)

persistent c;
persistent d;

if isempty(d), % initialize with default settings: check the help of matlab2tikz
	d.folder = file_makepath(userdir,'cache'); % location for data storage
        d.name = ''; % name of the cache (default = function name)
        d.lock_folder = false; % use a folder-wide locking mechanism (reduces disk-load, prevents concurrent writing of the same files)
        d.lock_file = true; % use a file locking mechanism (prevents concurrent writing of the same file)
        d.hash_args = false; % the cache filename depends on the input arguments
        d.hash_hierarchy = 0; % use a hierarchy of that levels to limit the number of files in a single directory (only when hash_args=true). one level is 2 characters of the hash, ie sets a maximum of 256 files in a directory at that level.
        d.safe_save = false; % check the saved data (tries to reload the data, and re-saves it under failure)
	d.verbose = false;
end;

if exist('new','var'), % Replace existing config
    c = new;
end;

if ~isempty(getenv('MATLABTOOLS')) && isempty(c), % Matlabtools is used but the config got lost (eg, clear functions)
    % Set c to the default values
    feval(mfilename,d);
    % Reload the config
    warning('Reloading matlabtools config');
    run(file_makepath(getenv('MATLABTOOLS'),'setup.m'));
    current = feval(mfilename);
    return
end

current = c;
