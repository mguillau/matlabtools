function cache_set_lock(mode)
% CACHE_SET_LOCK Set lock level for cache
%
% input: 
%  'file' for file-level locking
%  'folder' for folder-level locking
%  otherwise do not lock.
%

c=cache_config();
c.lock_file = false;
c.lock_folder = false;
switch mode,
    case 'file',
        c.lock_file = true;
    case 'folder',
        c.lock_folder = true;
end
cache_config(c);

