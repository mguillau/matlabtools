function varargout = cache_by_name(name,fun,varargin)

varargout = cell(1,max(1,nargout));
[varargout{:}] = cache(fun, varargin, struct('name',name));

