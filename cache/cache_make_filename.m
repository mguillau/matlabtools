function [filename,info_filename] = cache_make_filename(fun,args,config)
% CACHE_MAKE_FILENAME Create the filename for caching data

default config struct();
config = configFromDefaults(cache_config(),config);

[func.handle,func.name] = get_function_info(fun);

% Use the function name if no config.name is provided
if isempty(config.name),
    cache_print(config,'empty config.name, using function name');
    config.name = func.name;
end

% Add the function handle to the list of arguments to hash
args_to_hash = [ {func.handle} args ];

% Do we use the hash of the arguments do derive the filename?
if config.hash_args,
    hash = DataHash(args_to_hash);
    cache_print(config,'hash for arguments is %s',hash);
    % Do we use the hash hierarchy?
    if config.hash_hierarchy>0,
        hash = str_split(hash,[2*ones(1,config.hash_hierarchy) 32]); % split the hash in 42-character blocks
        filename = file_makepath(config.folder,config.name,hash{1:end-1},hash{end});
    else
        filename = file_makepath(config.folder,config.name,hash);
    end
else
    hash = '';
    filename = file_makepath(config.folder,config.name);
end

info_filename = [ filename '.txt' ];
filename = [ filename '.mat' ];
cache_print(config,'cache filename is %s',filename);

end

function cache_print(config,message,varargin)
if config.verbose, fprintf([message '\n'],varargin{:}); end
end

function [fun_handle,fun_name] = get_function_info(fun)
if ischar(fun),
    fun_name = fun;
    fun_handle = str2func(fun_name);
else
    fun_handle = fun;
    fun_name = func2str(fun);
    if fun_name(1)=='@', % The function is an anonymous function.
        fun_name = '_anonymous_function';
    end
end
end

