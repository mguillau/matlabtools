function bool = cache_check(fun,args,varargin)
% CACHE_CHECK Check whether the data is cached already
%

filename = cache_make_filename(fun,args,varargin{:});
bool = file_exist(filename);

