function cache_set_hash(mode)
% CACHE_SET_HASH Set argument hashing mode for cache
%
% input: 
%  n<0 - no hashing
%  0   - flat hashing
%  n>0 - hierarchical hashing
%

assert(isnumeric(mode),'expected a number');

c=cache_config();
c.hash_args = mode>=0;
c.hash_hierarchy = max(0,mode);
cache_config(c);

