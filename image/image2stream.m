function stream = image2stream(I,ext),
% IMAGE2STREAM Transform an image in a binary stream.

     tmp = [ tempname() '.' ext ];
     imwrite(I,tmp);
     stream.bytes = readstream(tmp);
     stream.ext = ext;
     delete(tmp);
end
