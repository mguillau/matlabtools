function imc = gray2jet(im,range,varargin)
% GRAY2JET Colorize a grayscale image
%
% imc = gray2jet(im,range,varargin)
%
% Transforms grayscale image im into a colorful range image (JET colormap)
%
% im = input grayscale image
% range = values to use as min and max of output range (default [ min(im(:)) max(im(:)) ])
% options = options of jet2

assert(ndims(im)<3);

default m 64;
default range [min(im(:)) max(im(:))];
if range(2)==range(1),
    range = range+[-1 1];
end

% Load the color map from jet()
cmap = jet2(m);
nmap = size(cmap,1)-1;
imsize = size(im);

% Warp the image to [1-N] where N is the number of colors in the color map
im = max(0,im-range(1));
im = im./(range(2)-range(1));
im = round(im*nmap)+1;
im = im(:);

% Assign colors
imc = cmap(im,:);

% Reshape to original image size
imc = reshape(imc,imsize(1),imsize(2),3);
end
