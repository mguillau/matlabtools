function LABXY = rgb2labxy(img)
H = size(img,1);
W = size(img,2);

img=im2double(img);
% if (max(img(:)) > 1.0)
%   img = img/255;
% end
B = img(:,:,3);
G = img(:,:,2);
R = img(:,:,1);

% Set a threshold
T = 0.008856;

RGB = [R(:) G(:) B(:)];%reshape(R,1,s); reshape(G,1,s); reshape(B,1,s)];

% RGB to XYZ
MAT = [0.412453 0.357580 0.180423;
       0.212671 0.715160 0.072169;
       0.019334 0.119193 0.950227];
XYZ = MAT*RGB';

X = XYZ(1,:) / 0.950456;
Y = XYZ(2,:);
Z = XYZ(3,:) / 1.088754;

XT = X > T;
YT = Y > T;
ZT = Z > T;

%%fX = XT .* X.^(1/3) + (~XT) .* (7.787 .* X + 16/116);
fX(XT) = X(XT).^(1/3);
fX(~XT) = (7.787 .* X(~XT) + 16/116);

% Compute L
Y3 = Y.^(1/3);
%%fY = YT .* Y3 + (~YT) .* (7.787 .* Y + 16/116);
fY(YT) = Y3(YT);
fY(~YT) = (7.787 .* Y(~YT) + 16/116);

%L  = YT .* (116 * Y3 - 16.0) + (~YT) .* (903.3 * Y);
L(YT) = (116 * Y3(YT) - 16.0);
L(~YT)  = (903.3 * Y(~YT));

%fZ = ZT .* Z.^(1/3) + (~ZT) .* (7.787 .* Z + 16/116);
fZ(ZT) = Z(ZT).^(1/3);
fZ(~ZT) = (7.787 .* Z(~ZT) + 16/116);

% Compute a and b
A = 500 * (fX - fY);
B = 200 * (fY - fZ);

[Y X] = meshgrid(1:W,1:H);
LABXY = [ L(:) A(:) B(:) X(:) Y(:) ];

