function I = stream2image(stream,ext)
% STREAM2IMAGE Convert a stream back to an image

if ~exist('ext','var'),
    ext = stream.ext;
    stream = stream.bytes;
end

tmp = [tempname() ext];
writestream(tmp,stream);
try
  I = imread(tmp);
  delete(tmp);
catch e
  delete(tmp);
  rethrow(e);
end

