function rg = rgb2normrgbxy(img)

H = size(img,1);
W = size(img,2);

img=im2double(img);
B = img(:,:,3);
G = img(:,:,2);
R = img(:,:,1);

S = 1./(B+G+R);

[Y,X] = meshgrid(1:W,1:H);
rg = [ R(:).*S(:) G(:).*S(:) B(:)./S(:) X(:) Y(:) ];
