function streambytes = readstream(file),
% READSTREAM Read a stream from file

     fid = fopen(file,'rb');
     streambytes = fread(fid,inf,'*uint8');
     fclose(fid);
end
