function imshow2(imgs,titls,fix)
% Plot an array of images with titles in a compact way.
% 1-d arrays are reformated to square 2-d arrays.
% 2-d arrays are kept untouched.
% images and titles sizes must coincide

s = size(imgs);
assert(length(s)<=2);

default titls repmat({''},s);
assert(length(imgs)==length(titls) || isempty(titls));

b = any(s==1) && ~exist('fix','var'); % input is 1d
if b, % input is 1-d: re-arrange in 2d
    N  = length(imgs(:));
    nr = ceil(sqrt(N));
    nc = ceil(N/nr);
    u  = 0;
else % the input is in 2-d: keep arrangement
    nr = s(1);
    nc = s(2);
    N = nr*nc;
end    
    
for i=1:nr,
    for j=1:nc
        if b,
            u = u+1;
        else
            u = (j-1)*nr+i;
        end
        if u<=N,
            subplot2d([nr nc i j]);
            if ~isempty(titls) && ~isempty(titls{u}),
                imgs{u} = imdrawtitle(imgs{u},titls{u});
            end
            imshow(imgs{u});
        else
            break
        end
    end
end
