function im = imdrawboxes(im,boxes,col,thick)
% Overlay bounding-box box=[xmin ymin xmax ymax]
% on image 'im', using color 'col' and thickness 'thick'.
%
% bb must be integer (pixel values) and its lines are cropped to fit im
%
% col should be a 3x1 vector, each element in [0,1]
%
% if thick(i) = offset given
% -> offsets BB to the list, generating different thicknesses
% >0 offsets -> outward, <0 offsets -> inward
%

default col [1 0 0]
default thick 0

if size(boxes,2)==4, % no score with boxes
    for b=1:size(boxes,1),
        im = imdrawbox(im,boxes(b,:),col,thick);
    end
else
    [~,ii] = sort(boxes(:,5));
    topcol = col;
    for b=1:size(boxes,1),
        col = topcol*boxes(ii(b),5);
        im = imdrawbox(im,boxes(ii(b),:),col,thick);
    end
end
