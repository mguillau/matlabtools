function image = imdrawtitle(image,title,varargin)
% image = imdrawtitle(image,title)
%
% Render a title on the input image
%
% image = the input image
% title = the title string
%
% options, default value:
% 'text_opts'                   options for str2image
% 'title_fraction',0.125        height of the title as fraction of image size
% 'title_top',5                 vertical placement of the title in pixels
% 'overlay_opts'                options for imoverlay
%

read_varargin;
default text_opts.fontsize 72;
defaultstr text_opts.h_align center;
default title_fraction 0.075;
default title_top 5;
default overlay struct();

% render title in image and resize
imageSize  = size(image);
titleImage = str2image(title,text_opts);
titleSize  = size(titleImage);
scale = imageSize(1)/titleSize(1)*title_fraction;
titleImage = imresize(titleImage,scale);

% find position for title
hdis = (size(image,2)-size(titleImage,2))/2;
overlay_opts.image2_origin = [title_top hdis];

% overlay title on image
image = imoverlay(image,titleImage,overlay_opts);
