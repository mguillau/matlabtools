function rg = rgb2rg(img)
% RGB2RG Convert a RGB image to RG

img=im2double(img);
B = img(:,:,3);
G = img(:,:,2);
R = img(:,:,1);

S = 1./(B+G+R);

rg = [ R(:).*S(:) G(:).*S(:) ];
