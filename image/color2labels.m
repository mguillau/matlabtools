function mask = rgbToMask(img)
% Assigns unique ids to each pixels for uniqe (rgb) values in img.
% Values will be from 1 to max unique pixels.
% No ids are skipped.

height = size(img,1);
width = size(img,2);
assert(size(img,3)==3);

r = img(:,:,1); g = img(:,:,2); b = img(:,:,3);
mask = [ r(:) g(:) b(:) ];
[~,~,mask] = unique(mask,'rows');
mask = reshape(mask,[height width]);
