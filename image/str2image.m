function image = str2image(text,varargin)
% STR2IMAGE Render text as an image
%
% image = str2image(text,...)
%
% Make an image from text.
% text = string to render
%
% options, default value:
% 'fontsize', 20;          font size to use
% 'fg_color', [0 0 0];     color triplet for foreground
% 'bg_color', [1 1 1];     color triplet for background
% 'font', 'LinLibertine_Italic';       cf  list_fonts() and 3rdparty/renderTextFT/fonts/
% 'h_align', 'left';         text alignment: 'left', 'center' or 'right';

read_varargin;
default fontsize 20;
default fg_color [0 0 0];
default bg_color [1 1 1];
defaultstr font 'LinLibertine_Italic';
defaultstr h_align 'left';

assert(~isempty(getenv('MATLABTOOLS')),'Matlabtools must properly setup to run this function');
font_file = file_makepath(getenv('MATLABTOOLS'),'3rdparty','renderTextFT','fonts',[font '.ttf']);
assert(exist(font_file,'file')==2,'Invalid font');

if any(bg_color<0) || any(fg_color<0) || any(bg_color>1) || any(fg_color>1),
    error('All color components must be in [0-1]');
end

textToRender = str_split(text,sprintf('\n')); % split lines
image = zeros(0,0,3);
for l=1:length(textToRender),
    % create image for each line
    text = textToRender{l};
    renderText = [ double([' ' text ' ']) 0 ];
    image2     = renderTextFT(renderText,round(255*bg_color),round(255*fg_color),font_file,round(fontsize));
    % compute offset to previous lines
    offset_top = size(image,1);
    switch h_align,
        case 'left',
            offset_left = 0;
        case 'center',
            offset_left = (size(image,2)-size(image2,2))/2;
        case 'right',
            offset_left = size(image,2)-size(image2,2);
        otherwise
            error('unknown alignement %s',h_align);
    end
    % concatenate with the same background color
    image = imoverlay(image,image2,'image2_origin',[offset_top offset_left],'bg_color',bg_color);
end
