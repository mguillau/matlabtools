function imslideshow( imgs, varargin )
% Display a set of images in a slideshow.

read_varargin
default images_per_page 36
s = size(imgs);
assert(length(s)<=2);
default titles repmat({''},s)
assert(length(imgs)==length(titles) || isempty(titles));

if nnz(size(imgs)>1)>1,
    for i=1:size(imgs,3),
        imshow(imgs(:,:,i),titles(:,:,i));
        pause;
    end
else
    imgs = imgs(:);
    Ntotal = length(imgs);
    Npages = ceil(Ntotal/images_per_page);
    pg_begin('slideshow','page %d of %d (images %d to %d of %d)');
    for i=1:Npages,
        b = (i-1)*images_per_page+1;
        e = min(Ntotal,i*images_per_page);
        clf;
        imshow2(imgs(b:e),titles(b:e));
        pg_update(i,Npages,b,e,Ntotal);
        if i<Npages,
            pause;
        end
    end
    pg_end
end

end

