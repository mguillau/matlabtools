function im = imdrawbox(im,box,col,thick)
% Overlay bounding-box box=[xmin ymin xmax ymax]
% on image 'im', using color 'col' and thickness 'thick'.
%
% bb must be integer (pixel values) and its lines are cropped to fit im
%
% col should be a 3x1 vector, each element in [0,1]
%
% if thick(i) = offset given
% -> offsets BB to the list, generating different thicknesses
% >0 offsets -> outward, <0 offsets -> inward
%

% process arguments
default thick 0;
default col [1,0,0];
if size(col,1)==1,
    col = repmat(col,size(box,1),1);
end
box = double(box);
box(:,1:4) = round(box(:,1:4));

if size(box,2)>4,
    box = sortrows(box,5);
end

for b=1:size(box,1),
    bb = [ box(b,1) box(b,2) box(b,3)-box(b,1) box(b,4)-box(b,2) ];
    if size(box,2)>4,
        sc = box(b,5);
        col(b,:) = sc*col(b,:);
    end
    % draw all layers of bb
    for t = -floor(thick/2):ceil(thick/2)
        bbt = [bb(1)-t bb(2)-t bb(3)+2*t bb(4)+2*t];
        im = PaintBB1(im, bbt, col(b,:));
    end
end

end

function im = PaintBB1(im, bb, col)
% Paint bounding-box bb = [xmin ymin width height]
% on image im, using color col.
%
% bb must be integer (pixel values) and its lines are cropped to fit im
%
% col should be a 3x1 vector, each element in [0,1]
%
% this is a function copied from vitto_matlab

% from [0,1] to [1 255]

if max(im(:))>1
    col = round(col*255);
end

% image width and height
iw = size(im, 2);
ih = size(im, 1);

% top-left -> top-right
l = CropLine([bb(1) bb(2) bb(1)+bb(3)-1 bb(2)], [iw ih]);  % l = [xmin ymin xmax ymax]
if not(isempty(l))
  im(l(2):l(4), l(1):l(3), 1) = col(1);
  im(l(2):l(4), l(1):l(3), 2) = col(2);
  im(l(2):l(4), l(1):l(3), 3) = col(3);
end

% bottom-left -> bottom-right
l = CropLine([bb(1) bb(2)+bb(4)-1 bb(1)+bb(3)-1 bb(2)+bb(4)-1], [iw ih]);  % l = [xmin ymin xmax ymax]
if not(isempty(l))
  im(l(2):l(4), l(1):l(3), 1) = col(1);
  im(l(2):l(4), l(1):l(3), 2) = col(2);
  im(l(2):l(4), l(1):l(3), 3) = col(3);
end

% top-left -> bottom-left
l = CropLine([bb(1) bb(2) bb(1) bb(2)+bb(4)-1], [iw ih]);  % l = [xmin ymin xmax ymax]
if not(isempty(l))
  im(l(2):l(4), l(1):l(3), 1) = col(1);
  im(l(2):l(4), l(1):l(3), 2) = col(2);
  im(l(2):l(4), l(1):l(3), 3) = col(3);
end

% top-right -> bottom-right
l = CropLine([bb(1)+bb(3)-1 bb(2) bb(1)+bb(3)-1 bb(2)+bb(4)-1], [iw ih]);  % l = [xmin ymin xmax ymax]
if not(isempty(l))
  im(l(2):l(4), l(1):l(3), 1) = col(1);
  im(l(2):l(4), l(1):l(3), 2) = col(2);
  im(l(2):l(4), l(1):l(3), 3) = col(3);
end
end

function lo = CropLine(l, wh)

% Crops horizontal or vertical line l = [xmin ymin xmax ymax]
% onto image of width wh(1) and height wh(2)
%
% If l is entirely out of image
% -> lo = []
%

% horizontal or vertical ?
if l(2) == l(4)
  % horizontal
  xov = [max([l(1) 1]) min([l(3) wh(1)])];
  if l(2) < 1 || l(2) > wh(2) || xov(1) > xov(2)
    lo = [];
    return;
  end
  lo = [xov(1) l(2) xov(2) l(4)];
elseif l(1) == l(3)
  % vertical
  yov = [max([l(2) 1]) min([l(4) wh(2)])];
  if l(1) < 1 || l(1) > wh(1) || yov(1) > yov(2)
    lo = [];
    return;
  end
  lo = [l(1) yov(1) l(3) yov(2)];
else
  error([mfilename ' works only for horizontal or vertical lines']);
end
end
