function writestream(file,streambytes),
% WRITESTREAM Write a stream to a file or file identifier.

isfile = ~isnumeric(file);

if isfile,
    file=fopen(file,'wb');
end
fwrite(file,streambytes);
if isfile,
    fclose(fid);
end

