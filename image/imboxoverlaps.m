function o = imboxoverlaps(a, b)
% IMBOXOVERLAPS Compute the symmetric intersection over union between image windows
%
% o = imboxoverlaps(a,b) returns the intersection over union between the
% bounding boxes in a and the ones in b.
%
% a  a Nx4 matrix where each row specifies a bounding box
% b  a Mx4 matrix where each row specifies a bounding box
%
% o  a NxM matrix with results

o = zeros(size(a,1),size(b,1));
for c=1:size(b,1),
    o(:,c) = imboxoverlap(a, b(c,:));
end
