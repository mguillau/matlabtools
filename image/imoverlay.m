function image = imoverlay(image1,image2,varargin)
% image = imoverlay(image1,image2,options)
%
% overlay image2 over image 1
%
% options, default values:
% 'bg_color', 1;            color to fill background
% 'image2_origin', [0 0];   position of top-left pixel of image2 relative to top-left pixel of image1

% TODO: transparency?

read_varargin;
default bg_color 1;
default image2_origin [0 0];

% normalize input
pos1 = [0 0];
pos2 = image2_origin;
pos2 = round(pos2); 
image1 = im2double(image1);
image2 = im2double(image2);

ii = pos2<0;
pos1(ii) = abs(pos2(ii));
pos2(ii) = 0;

ims1 = size(image1);
ims1(1:2) = ims1(1:2) + pos1;
ims2 = size(image2);
ims2(1:2) = ims2(1:2) + pos2;

if numel(ims1)~=numel(ims2),
    if numel(ims1)<3,
        ims1(3)=1;
    else
        ims2(3)=1;
    end
end

targetSize = max(ims1,ims2);
if length(bg_color) == 3, %#ok<NODEF>
    targetSize(3) = 3;
end

if length(targetSize)==3 && targetSize(3) == 3;
    image1 = gray2rgb(image1);
    image2 = gray2rgb(image2);
    if numel(bg_color)==1,
        bg_color = bg_color*[1 1 1];
    end
    image = repmat(reshape(bg_color(:),[1 1 3]),targetSize(1:2));
else
    image = bg_color*ones(targetSize);
end

image(pos1(1)+(1:size(image1,1)),pos1(2)+(1:size(image1,2)),:) = image1;
image(pos2(1)+(1:size(image2,1)),pos2(2)+(1:size(image2,2)),:) = image2;
