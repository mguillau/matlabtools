function J = imautocrop(I)
% IMAUTOCROP Automatically remove constant rows and columns at the border of an image
%
% J = imautocrop(I)
%
% Removes uniform rows and columns from the image I

channels = size(I,3);
J = cell(1,channels);
[M N] = size(I(:,:,1));
V = false(N,1);
U = false(M,1);

for k=1:channels,
Ik = I(:,:,k);
for m=1:M,
    U(m) = U(m)||~is_constant(Ik(m,:));
end
for n=1:N,
    V(n) = V(m)||~is_constant(Ik(:,n));
end
end

u1 = find(U,1,'first');
u2 = find(U,1,'last');
U(u1:u2) = true;
v1 = find(V,1,'first');
v2 = find(V,1,'last');
V(v1:v2) = true;

I = I(U,V,:);
