function fonts = list_fonts()

assert(~isempty(getenv('MATLABTOOLS')),'Matlabtools must properly setup to run this function');

font_dir = file_makepath(getenv('MATLABTOOLS'),'3rdparty','renderTextFT','fonts');
fonts = dir2(font_dir,'^.*\.ttf',@(x) regexprep(x,'\.ttf',''));

fprintf('To add/remove fonts, use .ttf files in:\n  %s/\n',font_dir);
fprintf('Available fonts:\n');
fprintf('  ->  ''%s''\n',fonts{:});

