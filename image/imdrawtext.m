function img2 = imdrawtext(img,text_string,pos,varargin)
% IMDRAWTEXT Render text on the input image
%
% image = imdrawtext(image,text,pos,...)
%
% Render a text on the input image
%
% image = the input image
% text = the text string
% pos = [top,left] positions of the text
%
% options, default value:
% text_opts         options for str2image()
% overlay_opts      options for imoverlay()

read_varargin;
default text_opts struct();
default overlay_opts struct();

if exist('pos','var'),
    overlay_opts.image2_origin = pos;
end

% render title in image and resize
textImage = str2image(text_string,text_opts);

% overlay title on image
img2 = imoverlay(img,textImage,overlay_opts);
