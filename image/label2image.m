function img2 = label2image(labels,varargin)
% LABEL2IMAGE Create image from image labelling
%
% options:
%  'render': rendering option, among 'randomcolor', 'thinborder', 'border', 'thickborder'.
%  'img': provide an image. additional rendering options become available: 'blend', 'averagecolor'. 
%  'mask': provide a binary mask for the image. impacts only '*border', only when 'img' is provided.

read_varargin
default img [];
default mask [];

noImage = isempty(img);

if noImage,
    defaultstr render randomcolor;
else
    assert(all(size(img(:,:,1))==size(labels)));
    defaultstr render blend;
end


labels = double(labels);
if min(labels(:))==0,
    labels = labels+1;
end

if noImage,
    img = ones(size(labels,1),size(labels,2),3);
    switch render,
        case 'randomcolor',
            img2 = im2double(label2color(labels));
        case 'thinborder',
            img2 = makeBorderImage(img,labels,1);
        case 'border',
            img2 = makeBorderImage(img,labels,3);
        case 'thickborder',
            img2 = makeBorderImage(img,labels,5);
        otherwise
            error('invalid render %s. try with image option.',render);
    end
else
    img = im2double(img);
    switch render,
        case 'novel',
            img_tmp = im2double(label2colorNovel(labels));
            img  = repmat(rgb2gray(img),[1 1 3]); % convert the original image in grayscale
            img2 = imlincomb(0.5,img,0.5,img_tmp);    % combine with label colors        
        case 'blend', % give a random color to each superpixel and overlay with original image
            img_tmp = im2double(label2color(labels));
            img  = repmat(rgb2gray(img),[1 1 3]); % convert the original image in grayscale
            img2 = imlincomb(0.5,img,0.5,img_tmp);    % combine with label colors
        case 'randomcolor',
            img2 = im2double(label2color(labels));
        case 'averagecolor', % raplce a superpixel with its average color
            img2 = makeAVGimage(img,labels);
        case 'thinborder',
            img = imagemask(img,labels,mask);
            img2 = makeBorderImage(img,labels,1);
        case 'border',
            img = imagemask(img,labels,mask);
            img2 = makeBorderImage(img,labels,3);
        case 'thickborder',
            img = imagemask(img,labels,mask);
            img2 = makeBorderImage(img,labels,5);
        otherwise
            error('invalid render %s, choose from "novel","blend","randomcolor","averagecolor","thinborder","border","thickborder"',render);
    end
end    
end

function img = imagemask(img,sp,mask)
if ~isempty(mask),
    msk = mask(sp);
    hsv = reshape(rgb2hsv(img),[],3);
    img = reshape(img,[],3);
    img(msk,1) = hsv(msk,1);
    img(msk,2) = hsv(msk,2);
    img(msk,3) = hsv(msk,3);
    img = reshape(img,size(msk,1),size(msk,2),3);
end
end

function img = label2color(labels)
N = max(labels(:));
%rng(0);
rp = randperm(N);
img = gray2jet(rp(labels),[0 N]);
end

function img = label2colorNovel(labels)
[ai,~,ui]=unique(labels);
N = max(ui);
ui = reshape(ui,size(labels));
%rng(0);
rp = 1:N;
idx = ai==max(labels(:));
%rp=1:N;
rp(idx) = 2*N;
img = gray2jet(rp(ui),[0 2*N]);
end

function supimg = makeAVGimage(img,labels)

H = size(img,1);
W = size(img,2);
np = H*W;

[L,A,B] = rgb2lab(img);
LAB = [ L(:) A(:) B(:) ];
C = max(labels(:));
Mlabels = sparse(labels,1:np,1,C,np);
nl = sum(Mlabels,2); % number of pixels for each label
Zc = bsxfun(@rdivide,Mlabels*LAB,nl); % take mean.

supimg = (Zc(:,1:3)'*Mlabels)';
supimg = lab2rgb(reshape(supimg,H,W,3));
end

function I2 = makeBorderImage(I,labels,thin)
borderMap = conv2(labels,[-1 -1 -1; -1 8 -1; -1 -1 -1],'same')~=0;
borderMap(1,:) = 0;
borderMap(end,:) = 0;
borderMap(:,1) = 0;
borderMap(:,end) = 0;
borderMap = bwmorph(borderMap,'thin',Inf);
switch thin,
    case 3,
        borderMap = bwmorph(borderMap,'dilate',1);
    case 5,
        borderMap = bwmorph(borderMap,'dilate',3);
end
I = im2double(I);
I2 = I(:,:,1);
I2(borderMap(:)) = 1;
if ndims(I)>1,
    I3 = I(:,:,2);
    I3(borderMap(:)) = 0;
    I4 = I(:,:,3);
    I4(borderMap(:)) = 0;
    I2 = cat(3,I2,I3,I4);
end
end
