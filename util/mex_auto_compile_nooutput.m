% Script to automatically compile and call mex-files.
%
% Should be called after the following is defined for myfunction.c :
%  function varargout = myfunction(varargin)
%    ext_source = '.c'; % extension of c/c++ code
%    funcName = mfilename;
%    fileName = mfilename('fullpath');
%    options = {}; % options for mex compilation
%

% Get caller full path
d=dbstack;
assert(length(d)>=2,'This script should be called by an m-file');
funcName = d(2).name;
fullpath = which(d(2).name);
assert( strcmp(fullpath(end-1:end), '.m'),'This script should be called by an m-file');
fileName = fullpath(1:end-2);

ext_mex = mexext;
sourceName = [ fileName ext_source ];
mexName = [ fileName '.' ext_mex ];
pg_message('compiling %s',mexName);
mex(sourceName,options{:},'-output',mexName);
feval(funcName,varargin{:});
