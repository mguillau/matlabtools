function b = has_caller(funs,level)
% HAS_CALLER Checks if the function has been called by a specific function
%
% has_caller(funstr) returns true if the current function has been directly
% called by funstr, a function provided by its name
%
% has_caller({funstr1,funstr2}) returns true if any has_caller(funstri) is
% true
%
% has_caller(funstr,level) looks up 'level' steps in the calling sequence.
% has_caller(funstr) is equivalent to has_caller(funstr,1).
%
% has_caller(funstr,0) returns true if funstr is anywhere in the calling
% sequence of the current function
%

default level 1

if ischar(funs),
    funs={funs};
end
callerstack = dbstack;
if length(callerstack)<max(3,2+level), % no level-caller to caller of has_caller...
    b=false;
    return
end
callerstack = callerstack(3:end); % remove call to has_caller and to caller of has_caller

if level>0,
    b = ismember(callerstack(level).name,funs);
else
    b = any(arrayfun(@(x) ismember(x.name,funs),callerstack));
end
