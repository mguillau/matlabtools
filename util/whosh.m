function whosh(varargin)
% accepts the same arguments as whos

% settings
usageWidth = 10;

if isempty(varargin)
	varargin = '';
else
	varargin = sprintf('''%s'',',varargin{:});
	varargin = varargin(1:end-1);
end;
w = evalin('caller',sprintf('whos(%s)',varargin));

n = length(w);

totalUsage = sum([w.bytes]);

table = cell(n+2,6);
table(1,:) = { 'Name' 'Size' 'Class' 'Memory' 'Usage' 'Attributes' };

for i = 1:n
	table{1+i,1} = w(i).name;
	table{1+i,2} = sprintf('%s%d',sprintf('%dx',w(i).size(1:end-1)),w(i).size(end));
	table{1+i,3} = w(i).class;
	table{1+i,4} = sizeString(w(i).bytes);
	table{1+i,5} = usageString(w(i).bytes,totalUsage,usageWidth);
	table{1+i,6} = attributeString(w(i));
end; clear i;

table{end,1} = sprintf('%d vars',n);
table{end,4} = sizeString(sum([w.bytes]));

widths = cellfun(@length,table);
widths = max(widths,[],1);

% fprintf('\n');
for i = 1:n+2
	for j = 1:size(table,2)
		fprintf('  %s%s',table{i,j},repmat(' ',1,widths(j)-length(table{i,j})));
	end;
	fprintf('\n');
	% if i==1, fprintf('\n'); end;
	% if i==n+1, fprintf('\n'); end;
end; clear i j;
fprintf('\n');


function s = sizeString(b)

if b>1e9
	s = sprintf('%.1fGb',b/1e9);
elseif b>1e6
	s = sprintf('%.1fMb',b/1e6);
elseif b>1e3
	s = sprintf('%.1fKb',b/1e3);
else
	s = sprintf('%db',b);
end;


function s = attributeString(w)

a = {};
if w.global, a{end+1} = 'global'; end;
if w.sparse, a{end+1} = 'sparse'; end;
if w.complex, a{end+1} = 'complex'; end;
if w.persistent, a{end+1} = 'persisent'; end;

if isempty(a)
	s = '';
else
	s = sprintf('%s%s',sprintf('%s,',a{1:end-1}),a{end});
end;


function s = usageString(usage,total,width)

r = usage/total;
n1 = round(r*width);
n2 = width-n1;
s = sprintf('%3d%% [%s%s]',round(100*r),repmat('-',1,n1),repmat(' ',1,n2));
