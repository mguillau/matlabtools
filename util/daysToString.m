function s = daysToString(dt,format)
% assuming dt is in days (as from differences between now() calls)

defaultstr format '%.1f%s';
dts = { 'd' 'h' 'm' 's' 'ms' [char(181) 's'] 'ns' };
dtt = [ 24 60 60 1000 1000 1000 1 ];

for sc=1:length(dts),
    if abs(dt)>=1, break; end
    dt=dt*dtt(sc);
end

s = sprintf(format,dt,dts{sc});
