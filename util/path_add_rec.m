function path_add_rec(pth)
% PATH_ADD_REC add directory and sub-directories to PATH. uses genpath but ignores VCS directories.

assert(ischar(pth));
assert(exist(pth,'file')==7);

% use genpath to get all subdirectories.
allpaths = str_split(genpath(pth),':');
% remove .svn and .git subdirectories
allpaths = arrayfilter(@(x) ~str_match(x,'/.svn') && ~str_match(x,'/.git'),allpaths);

addpath(allpaths{:});

