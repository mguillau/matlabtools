function s = sizeOfVar(name,unit)
% name = name string of var in callers workspace
% unit = b (default) | kb | mb | gb | human (or h)

assert(ischar(name));
defaultstr unit b

w = evalin('caller',sprintf('whos(''%s'')',name));
s = w.bytes;

if strcmp(unit,'h') || strcmp(unit,'human')

	if s<1e3
		s = sprintf('%db',s);
	elseif s<1e6
		s = sprintf('%.3fkb',s/1e3);
	elseif s<1e9
		s = sprintf('%.3fmb',s/1e6);
	else
		s = sprintf('%.3fgb',s/1e9);
	end;

else
	
	switch unit
		case 'b'
			u = 1;
		case 'kb'
			u = 1e3;
		case 'mb'
			u = 1e6;
		case 'gb'
			u = 1e9;
		otherwise
			assert(false,unit);
	end;

	s = s / u;
	
end;
