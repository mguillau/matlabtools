function p = path_user()
% returns subset of path that exclude system directories

syspath = which('mex');
syspath = regexprep(syspath,'^(.*)/toolbox/.*$','$1');
p = path();
p = str_split(p,pathsep);
p = arrayfilter(@(x) ~str_startswith(x,syspath),p);
p = str_join(p,pathsep);
