function warning(message,varargin)
% Prints a short warning message, accepting fprintf-like inputs.
% If the message is an MException, then prints a short report

if strcmp(class(message),'MException'),
    err = message;
    stack = err.stack;
    message = sprintf('[MException.message] %s',err.message);
    level = 1;
else
    stack = dbstack();
    level = 2;
end

%if length(stack)>=level,
%    caller = stack(level).name;
%    line   = sprintf(':%d',stack(level).line);
%else
%    message = sprintf(['[toplevel] ', message],varargin{:});
%end

warning(message);

