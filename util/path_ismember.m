function [b,pth] = path_ismember(varargin)
% PATH_ISMEMBER Check the presence of directories in Matlab's search path, resolving symbolic links.
%
% b = path_ismember(path1) returns b=true iff path1 is already in Matlab's search path.
% [b,realpath1] = path_ismember(path1) also returns the real path of the input (e.g. useful to use in addpath)
%
% b = path_ismember(path1,path2,...,pathN) returns a logical vector of length N where b(I) = path_ismember(pathI).
% [b,realpath1] = path_ismember(path1,path2,...,pathN) also returns the cell array containing all the real paths.

pths = path();
pths = str_split(pths,pathsep);
pth = cellfun2(@file_realpath,varargin);
b = ismember(pth,pths);
if length(pth)==1,
  pth = pth{1};
end
