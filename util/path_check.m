function path_check(folder,inPath)
% folder = folder to check
% inPath = is this folder already in the path (default false)

arg_assert folder char;
arg_assert inPath logical default false;

% TODO there are more files to check, not only *.m
mfiles = dir(sprintf('%s%s*.m',folder,filesep));
mfiles = { mfiles.name };
n = length(mfiles);

for i = 1:n %#ok<FORPF>
	
	refs = which('-all',mfiles{i});
	
	if inPath && length(refs)>1
		fprintf('name clash: %d references for %s\n',length(refs),mfiles{i});
	end;
	
	if ~inPath && ~isempty(refs)
		fprintf('name clash: already %d references for %s\n',length(refs),mfiles{i});
	end;
	
end;
