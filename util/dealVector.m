function varargout = dealVector(v)

n = nargout();
varargout = cell(1,n);
assert(length(v)==n);

for i = 1:n
	varargout{i} = v(i);
end;
