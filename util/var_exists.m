function ans = var_exists(varargin)
% ans = the variables in varargin (strings) exist?

n = length(varargin);
ans = false(1,n);

for i = 1:n
	ans(i) = evalin('caller',sprintf('exist(''%s'',''var'')',varargin{i}));
end;
