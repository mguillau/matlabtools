function path_add(varargin)
% Adds and checks folders to the path.
% arguments: [base:folder] folder ...
% alternatively: { folder ... }
% if first folder starts with 'base:' it is prepended to all following folders

if length(varargin)==1 && iscell(varargin{1})
	folders = varargin{1};
else
	folders = varargin;
end;

if isempty(folders)
	return;
end;

if str_startswith(folders{1},'base:')
	base = folders{1}(length('base:')+1:end);
        if base(end) ~= filesep,
            base(end+1) = filesep;
        end
	folders(1) = [];
else
	base = '';
end;

for i = 1:length(folders)
	f = sprintf('%s%s',base,folders{i});
        f = file_realpath(f);
	path_check(f,false);
	addpath(f);
end;
