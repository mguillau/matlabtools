function userdirpath = userdir()

if ispc,
    userdirpath = getenv('USERPROFILE'); 
else
    assert(isunix);
    userdirpath = getenv('HOME'); 
end       

