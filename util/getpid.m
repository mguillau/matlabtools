function varargout = getpid(varargin)
% pid = getpid()
%
% Returns the pid of the matlab process.

ext_source = '.c';
options = { '-largeArrayDims' };

mex_auto_compile;
