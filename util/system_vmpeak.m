function s = system_vmpeak()

assert(isunix);

[status,result] = system(sprintf('cat /proc/%d/status | grep VmPeak',getpid()));
assert(status==0,result);

tokens = regexp(result,'VmPeak:\s*([0-9]*) (.?B)','tokens');
tokens = tokens{1};
assert(length(tokens)==2);

s = str2num(tokens{1});

switch tokens{2}
	case 'B'
	case 'kB'
		s = 1e3*s;
	case 'mB'
		s = 1e6*s;
	case 'gB'
		s = 1e9*s;
	otherwise
		assert(false,tokens{2});
end;

