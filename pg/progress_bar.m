function progress_bar(total,current,varargin)
% PROGRESS_BAR Plots a progress bar
%
% progress_bar(total,current) plots a progress bar at step 'current' out of 'total' steps.
% e.g:
% >> progress_bar(60,10);
% ========/ 16.67%
% 
% It is most useful in loops:
% >> total=60; for i=1:total, pause(0.2); progress_bar(total,i); end
%
% Optional arguments are:
% - linewidth (integer): the width of the progress bar (excluding percentage, default: 50)
% - donechar (character): the character used to indicate progress (default: '=') 
% - chars (cell array of characters): sequence of characters showing iterations (default: { '\\' '|' '/' '-' })
% - pctformat (string): fprintf-like formatting of the percentage (default: ' %3.2f%%')
% - updateToc (time in sec): frequency of update

persistent oldlength
persistent oldtic
default linelength 50
defaultstr donechar =
default chars { '\' '|' '/' '-' }
defaultstr pctformat ' %3.2f%%'
default updateToc 0.2

% initialize oldlength and oldtoc
if isempty(oldlength) || current==1,
   oldlength = 0;
end
if isempty(oldtic),
    oldtic = tic();
    thisToc = 2*updateToc;
else
    thisToc = toc(oldtic);
end
if thisToc < updateToc && current~=total,
    return;
end
oldtic=tic();

% delete previous output
fprintf(repmat('\b',1,oldlength));

% set the spinning head
if current==total,
    lastchar = '';
else
    lastchar = chars{mod(current,length(chars))+1};
end

% compute the length of the progress bar
newlength=floor(current/total*linelength);

% print the progress bar, the spinning head and the completion
fwrite(1,repmat(donechar,1,newlength));
fwrite(1,lastchar);
pct = sprintf(pctformat,100*current/total);
fwrite(1,pct);

% save the length of the output
oldlength=newlength+length(lastchar)+length(pct);

% when finished, clear oldlength
if current==total,
    oldlength=[];
    fprintf('\n');
end

end
