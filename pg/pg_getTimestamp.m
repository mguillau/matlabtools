function s = pg_getTimestamp(config)

if config.timestamp
    
    switch config.dateformat
        case 31
            s = sprintf('%s',datestr(now(),config.dateformat));
        case 'cputime'
            s = sprintf('%d',round(cputime));
        otherwise
            s = sprintf('%s',datestr(now(),config.dateformat));
%             error('unknown timestamp %s',config.timestamp);
    end;
    
else
    s = '';
end;

if config.par_time,
    [t1,t0,w] = par_worker_time();
    if t0>=0 && w>=0,
        t = daysToString(t1-t0,'%.0f%s');
        ws = daysToString(w,'%.0f%s');
        s2 = sprintf('[%s/%s]',t,ws);
        if isempty(s),
            s = s2;
        else
            s = [ s ' ' s2 ];
        end
    end
end
