function pg_message(message,varargin)

config = pg_config();
state = pg_state();

indentation = sprintf('%s',repmat('  ',1,length(state.stack)));

timestamp = pg_getTimestamp(config);

if ~state.newLine, fprintf('\n'); end;

fprintf('%s%s ',timestamp,indentation);
fprintf(message,varargin{:});
fprintf('\n');

state.lastOutput = '';
state.newLine = true;
	
pg_state(state);
