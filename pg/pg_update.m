function pg_update(varargin)
% Currently, if there are >=2 arguments, the first two are assumed to be count and total for eta computation.
% Eta semantics: A call with (count/total) means, count-1 have been done, count is about to be done.

% doeta = true; % TODO maybe setting, if not always applicable
if length(varargin)<2
	doeta = false;
else
	doeta = true;
end;

if doeta
	done = varargin{1};
	total = varargin{2};
% 	varargin = varargin(3:end);
end;

config = pg_config();
state = pg_state();

if (now()-state.stack(end).lastUpdate)*24*60*60 >= config.minDelay

	if doeta
		
		if ~isfield(state.stack(end),'eta_length') || isempty(state.stack(end).eta_length)
			state.stack(end).eta_length = 10;
			state.stack(end).eta_last_time = state.stack(end).starttime;
			state.stack(end).eta_last_done = 0;
			state.stack(end).eta_duration = zeros(state.stack(end).eta_length,1);
			state.stack(end).eta_count = zeros(state.stack(end).eta_length,1);
			state.stack(end).eta_next = 1;
		end;

		assert(max(0,done-1)>=state.stack(end).eta_last_done);
		
		if done>state.stack(end).eta_last_done-1
			state.stack(end).eta_duration(state.stack(end).eta_next) = now()-state.stack(end).eta_last_time;
			state.stack(end).eta_count(state.stack(end).eta_next) = (done-1)-state.stack(end).eta_last_done;
			state.stack(end).eta_next = rem(state.stack(end).eta_next,state.stack(end).eta_length)+1;
			state.stack(end).eta_last_time = now();
			state.stack(end).eta_last_done = done-1;
		end;
		
		if any(state.stack(end).eta_count>0)
			eta = (total-(done-1)) * sum(state.stack(end).eta_duration(state.stack(end).eta_count>0)) / sum(state.stack(end).eta_count(state.stack(end).eta_count>0));
		else
			eta = nan();
		end;
	else
		eta = nan();
	end;
	
	indentation = sprintf('%s',repmat('  ',1,length(state.stack)-1));

	timestamp = pg_getTimestamp(config);

	if state.newLine
% 		out = sprintf('%s%s ... %s (%s eta %s)',...
% 				timestamp,indentation, ...
% 				sprintf(state.stack(end).updatemessage,varargin{:}), ...
% 				daysToString(now()-state.stack(end).starttime), ...
% 				daysToString(eta) ...
% 			);
		out = sprintf('%s%s ... %s (%s)',...
				timestamp,indentation, ...
				sprintf(state.stack(end).updatemessage,varargin{:}), ...
				daysToString(now()-state.stack(end).starttime) ...
			);
	else
		fprintf(repmat('\b',1,length(state.lastOutput)));
		fprintf(repmat(' ',1,length(state.lastOutput)));
		fprintf(repmat('\b',1,length(state.lastOutput)));
% 		out = sprintf('%s%s %s ... %s (%s eta %s)',...
% 				timestamp,indentation, ...
% 				state.stack(end).message, ...
% 				sprintf(state.stack(end).updatemessage,varargin{:}), ...
% 				daysToString(now()-state.stack(end).starttime), ...
% 				daysToString(eta) ...
% 			);
		out = sprintf('%s%s %s ... %s (%s)',...
				timestamp,indentation, ...
				state.stack(end).message, ...
				sprintf(state.stack(end).updatemessage,varargin{:}), ...
				daysToString(now()-state.stack(end).starttime) ...
			);
	end;

	fprintf('%s',out);
	
	state.stack(end).lastUpdate = now();
	state.lastOutput = out;
	state.newLine = false;
	
end;

pg_state(state);
