function pg_begin(message,updatemessage)
% message
%	string or cell of sprintf arguments
% updatemessage [optional]

if iscell(message)
	message = sprintf(message{:});
end;

if ~exist('updatemessage','var')
	updatemessage = '';
end;

config = pg_config();
state = pg_state();

if ~state.newLine, fprintf('\n'); end;

indentation = sprintf('%s',repmat('  ',1,length(state.stack)));

timestamp = pg_getTimestamp(config);

out = sprintf('%s%s %s ... ',timestamp,indentation,message);
fprintf('%s',out);

state.stack(end+1).starttime = now();
state.stack(end).message = message;
state.stack(end).updatemessage = updatemessage;
state.stack(end).lastUpdate = -inf; % could also use now(), -inf=never
state.lastOutput = out;
state.newLine = false;

pg_state(state);

% TODO beta
% trying to prevent forgetting pg_end, especially after debug exits
% if evalin('caller','exist(''pg_automatic_clear'',''var'')')
% 	assert(evalin('caller','isa(pg_automatic_clear,''onCleanup2'')'));
% 	evalin('caller','pg_automatic_clear(end+1) = onCleanup2(@pg_end);');
% else
% 	evalin('caller','pg_automatic_clear = onCleanup2(@pg_end);');
% end;
