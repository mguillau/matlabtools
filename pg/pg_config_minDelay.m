function config = pg_config_minDelay(d)

config = pg_config();
config.minDelay = d;
pg_config(config);
