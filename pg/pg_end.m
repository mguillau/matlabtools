function pg_end(message,varargin)

if ~exist('message','var')
	message = '';
end;

if ~isempty(varargin)
	message = sprintf(message,varargin{:});
end;

config = pg_config();
state = pg_state();

indentation = sprintf('%s',repmat('  ',1,length(state.stack)-1));

timestamp = pg_getTimestamp(config);

if state.newLine
	fprintf('%s%s ... %s (%s)\n',timestamp,indentation,message,daysToString(now()-state.stack(end).starttime));
else
	fprintf(repmat('\b',1,length(state.lastOutput)));
	fprintf(repmat(' ',1,length(state.lastOutput)));
	fprintf(repmat('\b',1,length(state.lastOutput)));
	fprintf('%s%s %s ... %s (%s)\n',timestamp,indentation,state.stack(end).message,message,daysToString(now()-state.stack(end).starttime));
end;

state.stack = state.stack(1:end-1);
state.lastOutput = '';
state.newLine = true;

pg_state(state);

% TODO beta
% trying to prevent forgetting pg_end, especially after debug exits
% if evalin('caller','exist(''pg_automatic_clear'',''var'')')
% 	evalin('caller','pg_automatic_clear(end).enabled = false;');
% 	evalin('caller','pg_automatic_clear(end) = [];');
% end;
