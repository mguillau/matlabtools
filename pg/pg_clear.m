function pg_clear()

state = pg_state();
state.stack = [];
state.lastOutput = '';
state.newLine = true;
pg_state(state);
