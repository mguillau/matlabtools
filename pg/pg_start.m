classdef pg_start < handle
	
	properties
		isDone = false;
	end
	
	methods
		
		function this = pg_start(varargin)
			pg_begin(varargin{:});
		end
		
		function update(this,varargin)
			pg_update(varargin{:});
		end
		
		function done(this,varargin)
			assert(~this.isDone);
			pg_end(varargin{:});
			this.isDone = true;
		end
		
		function delete(this)
			if ~this.isDone
				this.done();
			end;
		end
		
	end
	
end
