function current = pg_config(new)

persistent config;

if isempty(config)
	config.minDelay = 5; % in seconds
	config.timestamp = true;
	config.dateformat = 'dd-HH:MM:SS'; % 31, 'cputime'
	config.par_time = true;     
end;

if exist('new','var')==1
	config = new;
end;

current = config;
