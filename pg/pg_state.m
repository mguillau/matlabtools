function current = pg_state(new)

persistent state;

if isempty(state)
	state.stack = [];
	state.lastOutput = '';
	state.newLine = true;
end;

if exist('new','var')==1
	state = new;
end;

current = state;
